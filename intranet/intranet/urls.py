"""intranet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.views.generic.base import RedirectView

urlpatterns = [
    url(r'^admin/?', admin.site.urls),
    url(r'^login/', include('apps.login.urls', namespace='login'),),
    url(r'^main/', include('apps.main.urls', namespace='main'),),
    url(r'^api/v1/', include('apps.api.urls', namespace='apiv1'),),
    url(r'^mail/', include('apps.mail.urls', namespace='mail'),),
    url(r'^invoice/', include('apps.invoice.urls', namespace='invoice'),),
    url(r'^productos/', include('apps.productos.urls', namespace='productos'),),
    url(r'^catalogos/', include('apps.catalogos.urls', namespace='catalogos'),),
    url(r'^grants/', include('apps.grants.urls', namespace='grants'),),
    url(r'^$', RedirectView.as_view(url='login/', permanent=False)),
    # url(r'^api-auth/', include('rest_framework.urls'))
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
