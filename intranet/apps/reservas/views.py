from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.views import View
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import JsonResponse
# from apps.metas import (
#     models as mdlmetas
# )
# from apps.catalogos import (
#     models as mdlcat,
#     serializers as srlzcat,
# )
# from apps.reservas import (
#     models as mdl,
#     serializers as srlz,
# )
from django.db.models import Value, Case, When, CharField, Q, F
from apps.connections.config import (
    MYSQLConnection,
)
from apps.common.utils import Validators
import json

from django.utils.decorators import method_decorator as md
from django.contrib.auth.decorators import (
    login_required,
    permission_required as perm,
)
from django.core.exceptions import PermissionDenied
# from apps.reservas.utils import getScheduleList

DEFAULT_FORBIDDEN_PAGE = '/login/forbidden'


# @md(login_required, name='dispatch')
# class ViewReservasSalas(View):
#     template_name = 'reservas/reservas_salas.html'
#     template_header = 'SKBerge | Salas de Reservación'
#     template_title = 'Salas de Reservación'
#     defaultResponse = {
#         "ok": 1,
#         "result": 'Process has been processed successfuly.',
#         "status": 200,
#     }

#     def dispatch(self, *args, **kwargs):
#         return super(ViewReservasSalas, self).dispatch(*args, **kwargs)

#     @md(perm('catalogos.add_sala', login_url=DEFAULT_FORBIDDEN_PAGE,),)
#     def get(self, request):
#         wresponse = self.defaultResponse.copy()
#         header = self.template_header
#         title = self.template_title
#         return render(request, self.template_name, locals())

#     @md(perm('catalogos.add_sala', raise_exception=True),)
#     def post(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             if not request.is_ajax():
#                 wresponse['status'] = 400
#                 raise ValueError(str('Bad Request'))
#             if wdata.get("wtype"):
#                 wtype = wdata.get("wtype")
#                 if wtype == "get_reservas_sala":
#                     return self.getSalasSearch(request)
#                 if wtype == "save_information":
#                     return self.saveInformation(request)
#                 if wtype == "update_reserva_sala":
#                     return self.updateSalas(request)
#                 if wtype == "remove_reserva_sala":
#                     return self.removeSala(request)
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if type(e) is PermissionDenied:
#                 wresponse['status'], wresponse[
#                     'result'] = 403, 'Permission Denied'
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @md(perm('catalogos.add_sala', raise_exception=True),)
#     def getSalasSearch(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             # connection = MYSQLConnection()
#             wdata = json.loads(request.body.decode("utf8"))
#             wfilters = {}
#             wsala = wdata.get('sala') or ''
#             wplanta = wdata.get('planta') or ''
#             westado = wdata.get('estado') or ''

#             if wsala != '':
#                 wfilters['descripcion__icontains'] = wsala
#             if westado != '':
#                 wfilters['estado'] = mdlmetas.Estados.objects.get(
#                     id=int(westado))
#             if wplanta != '':
#                 wfilters['planta'] = mdlcat.Planta.objects.get(
#                     id=int(wplanta))

#             wqueryset = mdlcat.Sala.objects.filter(**wfilters) \
#                 .order_by("descripcion").distinct()
#             # print(str(wqueryset.query))
#             woutcome = srlzcat.SalaSerializer(
#                 wqueryset,
#                 many=True,
#             ).data
#             wresponse['result'] = woutcome
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @md(perm('catalogos.add_sala', raise_exception=True),)
#     def saveInformation(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             print(wdata)
#             wdescription = wdata.get("descripcion") or ''
#             if len(wdescription) == 0:
#                 raise ValueError('Error, descripcion no puede ser null.')
#             westado = wdata.get("estado")
#             wplanta = wdata.get("planta")
#             wcapacidad = wdata.get("capacidad") or 1
#             wfecha_vigencia = wdata.get("fecha_vigencia") or None
#             if not wfecha_vigencia is None:
#                 wfecha_vigencia, wstatus = Validators.isDate(
#                     wfecha_vigencia, "%Y-%m-%d")
#                 if not wstatus:
#                     raise ValueError('Error, Fecha Vigencia invalida.')

#             objEstado = mdlmetas.Estados.objects.get(id=int(westado))
#             objPlanta = mdlcat.Planta.objects.get(id=int(wplanta))
#             wparameters = {
#                 "descripcion": wdescription.strip().upper(),
#                 "planta": objPlanta,
#                 "estado": objEstado,
#                 "limite_participantes": wcapacidad,
#                 "fecha_vigencia": wfecha_vigencia,
#             }
#             objSala = mdlcat.Sala(**wparameters)
#             objSala.save()
#             wresponse['result'] = {
#                 "id": objSala.pk,
#                 "message": "Sala de Reserva procesada satisfactoriamente.",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @md(perm('catalogos.change_sala', raise_exception=True),)
#     def updateSalas(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             widsala = wdata.get("id") or 0
#             objSala = mdlcat.Sala.objects.get(id=int(widsala))
#             wdescription = wdata.get("descripcion") or ''
#             wcapacidad = wdata.get("capacidad") or 1
#             if objSala == None:
#                 raise ValueError('Error, sala especificada no existe.')
#             if len(wdescription) == 0:
#                 raise ValueError('Error, descripcion no puede ser null.')
#             wfecha_vigencia = wdata.get("fecha_vigencia") or None
#             if not wfecha_vigencia is None:
#                 wfecha_vigencia, wstatus = Validators.isDate(
#                     wfecha_vigencia, "%Y-%m-%d")
#                 if not wstatus:
#                     raise ValueError('Error, fecha vigencia invalida.')

#             widestado = wdata.get("estado") or 0
#             widplanta = wdata.get("planta") or 0
#             objEstado = mdlmetas.Estados.objects.get(id=int(widestado))
#             objPlanta = mdlcat.Planta.objects.get(id=int(widplanta))

#             objSala.estado = objEstado
#             objSala.planta = objPlanta
#             objSala.descripcion = wdescription.strip().upper()
#             objSala.limite_participantes = wcapacidad
#             objSala.fecha_vigencia = wfecha_vigencia
#             objSala.save()
#             wresponse['result'] = {
#                 "id": objSala.pk,
#                 "message": "Cambios fueron actualizados satisfactoriamente",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @md(perm('catalogos.delete_sala', raise_exception=True),)
#     def removeSala(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             widsala = wdata.get("item").get("id") or 0
#             objSala = mdlcat.Sala.objects.get(id=int(widsala))
#             if objSala == None:
#                 raise ValueError('Error, Sala especificada no existe.')
#             objSala.delete()
#             wresponse['result'] = {
#                 "message": "Sala fue eliminada satisfactoriamente.",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))


# @md(login_required, name='dispatch')
# class ViewReservasFeriados(View):
#     template_name = 'reservas/reservas_feriados.html'
#     template_header = 'SKBerge | Registro de Feriados'
#     template_title = 'Registro de Feriados'
#     defaultResponse = {
#         "ok": 1,
#         "result": 'Process has been processed successfuly.',
#         "status": 200,
#     }

#     def dispatch(self, *args, **kwargs):
#         return super(ViewReservasFeriados, self).dispatch(*args, **kwargs)

#     @md(perm('reservas.add_feriados', login_url=DEFAULT_FORBIDDEN_PAGE,),)
#     def get(self, request):
#         wresponse = self.defaultResponse.copy()
#         header = self.template_header
#         title = self.template_title
#         return render(request, self.template_name, locals())

#     @md(perm('reservas.add_feriados', raise_exception=True),)
#     def post(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             if not request.is_ajax():
#                 wresponse['status'] = 400
#                 raise ValueError(str('Bad Request'))
#             if wdata.get("wtype"):
#                 wtype = wdata.get("wtype")
#                 if wtype == "get_reservas_feriados":
#                     return self.getFeriadosSearch(request)
#                 elif wtype == "save_information":
#                     return self.saveInformation(request)
#                 elif wtype == "remove_reserva_feriado":
#                     return self.removeFeriado(request)
#                 elif wtype == "update_reserva_feriado":
#                     return self.updateFeriado(request)
#                 else:
#                     pass
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if type(e) is PermissionDenied:
#                 wresponse['status'], wresponse[
#                     'result'] = 403, 'Permission Denied'
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     def getFeriadosSearch(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             # connection = MYSQLConnection()
#             wdata = json.loads(request.body.decode("utf8"))
#             wferiado = wdata.get('fecha') or ''
#             wfilters = Q(id__isnull=False)
#             if wferiado != '':
#                 # if date comes in the format value, change it by the default
#                 wfecha, wstatus = Validators.isDate(wferiado, "%d/%m/%Y")
#                 wferiado = wfecha.strftime(
#                     "%Y-%m-%d") if wstatus else wferiado
#                 # query the value for both fields descripcion and fecha
#                 wfilters = Q(descripcion__icontains=wferiado) | \
#                     Q(fecha__icontains=wferiado)

#             wqueryset = mdl.Feriados.objects.filter(wfilters) \
#                 .order_by("-fecha").distinct()
#             # print(str(wqueryset.query))
#             woutcome = srlz.FeriadosSerializer(
#                 wqueryset,
#                 many=True,
#             ).data
#             wresponse['result'] = woutcome
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @md(perm('reservas.add_feriados', raise_exception=True),)
#     def saveInformation(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             wdescription = wdata.get("descripcion") or ''
#             if len(wdescription) == 0:
#                 raise ValueError('Error, descripcion no puede ser null.')
#             westado = wdata.get("estado")
#             wfecha = wdata.get("fecha") or None
#             if not wfecha is None:
#                 wfecha, wstatus = Validators.isDate(wfecha, "%Y-%m-%d")
#                 if not wstatus:
#                     raise ValueError('Error, Fecha invalida.')

#             objEstado = mdlmetas.Estados.objects.get(id=int(westado))
#             wparameters = {
#                 "descripcion": wdescription.strip().upper(),
#                 "estado": objEstado,
#                 "fecha": wfecha,
#             }
#             objFeriado = mdl.Feriados(**wparameters)
#             objFeriado.save()
#             wresponse['result'] = {
#                 "id": objFeriado.pk,
#                 "message": "Sala de Reserva procesada satisfactoriamente.",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @md(perm('reservas.change_feriados', raise_exception=True),)
#     def updateFeriado(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             widferiado = wdata.get("id") or 0
#             objFeriado = mdl.Feriados.objects.get(id=widferiado)
#             wdescription = wdata.get("descripcion") or ''
#             if len(wdescription) == 0:
#                 raise ValueError('Error, descripcion no puede ser null.')
#             wfecha = wdata.get("fecha") or None
#             if not wfecha is None:
#                 wfecha, wstatus = Validators.isDate(
#                     wfecha, "%Y-%m-%d")
#                 if not wstatus:
#                     raise ValueError('Error, fecha invalida.')

#             widestado = wdata.get("estado") or 0
#             objEstado = mdlmetas.Estados.objects.get(id=int(widestado))

#             objFeriado.estado = objEstado
#             objFeriado.descripcion = wdescription.strip().upper()
#             objFeriado.fecha = wfecha
#             objFeriado.save()
#             wresponse['result'] = {
#                 "id": objFeriado.pk,
#                 "message": "Cambios fueron actualizados satisfactoriamente",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @md(perm('reservas.delete_feriados', raise_exception=True),)
#     def removeFeriado(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             widferiado = wdata.get("item").get("id") or 0
#             objFeriado = mdl.Feriados.objects.filter(id=int(widferiado))
#             if not objFeriado.exists():
#                 raise ValueError('Error, Sala especificada no existe.')
#             objFeriado.get().delete()
#             wresponse['result'] = {
#                 "message": "Feriado fue eliminado satisfactoriamente.",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))


# @md(login_required, name='dispatch')
# class ViewReservasCalendario(View):
#     template_name = 'reservas/reservas_calendario.html'
#     template_header = 'SKBerge | Calendario de Reservaciones'
#     template_title = 'Calendario de Reservaciones'
#     defaultResponse = {
#         "ok": 1,
#         "result": 'Process has been processed successfuly.',
#         "status": 200,
#     }

#     def dispatch(self, *args, **kwargs):
#         return super(ViewReservasCalendario, self).dispatch(*args, **kwargs)

#     @md(perm('reservas.add_reservas', login_url=DEFAULT_FORBIDDEN_PAGE,),)
#     def get(self, request):
#         wresponse = self.defaultResponse.copy()
#         header = self.template_header
#         title = self.template_title
#         return render(request, self.template_name, locals())

#     @md(perm('reservas.add_reservas', raise_exception=True),)
#     def post(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             if not request.is_ajax():
#                 wresponse['status'] = 400
#                 raise ValueError(str('Bad Request'))
#             if wdata.get("wtype"):
#                 wtype = wdata.get("wtype")
#                 if wtype == "get_reserva_item":
#                     return self.getReserva(request)
#                 if wtype == "get_reservas":
#                     return self.getReservasSearch(request)
#                 if wtype == "save_information":
#                     return self.saveInformation(request)
#                 if wtype == "update_reserva":
#                     return self.updateReserva(request)
#                 if wtype == "remove_reserva":
#                     return self.removeReserva(request)
#                 if wtype == "get_schedules":
#                     return self.getSchedules(request)

#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if type(e) is PermissionDenied:
#                 wresponse['status'], wresponse[
#                     'result'] = 403, 'Permission Denied'
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @md(perm('reservas.add_reservas', raise_exception=True),)
#     def getReserva(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             # connection = MYSQLConnection()
#             wdata = json.loads(request.body.decode("utf8"))
#             wreserva = wdata.get('id') or ''
#             wqueryset = mdl.Reservas.objects.filter(id=int(wreserva)) \
#                 .order_by("-fhregistro").distinct()
#             if not wqueryset.exists():
#                 raise ValueError('Error, reserva invalido.')
#             woutcome = srlz.ReservasSerializer(
#                 wqueryset,
#                 many=True,
#             ).data
#             wqueryset_det = mdl.ReservasParticipantes.objects.filter(
#                 reserva=wqueryset.get())
#             wdetails = srlz.ReservasParticipantesSerializer(
#                 wqueryset_det,
#                 many=True,
#             ).data
#             for item in wdetails:
#                 item["check"] = False
#                 item["visible"] = True
#                 item["empleado"]['fullname'] = '{} {}'.format(
#                     item.get("empleado").get("nombres"),
#                     item.get("empleado").get("apellidos"),
#                 )
#             woutcome[0]['participantes'] = wdetails
#             wresponse['result'] = woutcome
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @md(perm('reservas.add_reservas', raise_exception=True),)
#     def getReservasSearch(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             # connection = MYSQLConnection()
#             wdata = json.loads(request.body.decode("utf8"))
#             Qfilters = Q(id__isnull=False)
#             wfecha_desde = wdata.get('fecha_desde') or None
#             wfecha_hasta = wdata.get('fecha_hasta') or None
#             if None not in[wfecha_desde, wfecha_hasta]:
#                 wfecha_desde, wstatus = Validators.isDate(
#                     wfecha_desde, "%Y-%m-%d")
#                 if not wstatus:
#                     raise ValueError('Error, fecha desde invalido.')
#                 wfecha_hasta, wstatus = Validators.isDate(
#                     wfecha_hasta, "%Y-%m-%d")
#                 if not wstatus:
#                     raise ValueError('Error, fecha hasta invalido.')
#                 Qfilters.add(
#                     Q(fecha_evento__range=[wfecha_desde, wfecha_hasta]),
#                     Qfilters.connector
#                 )

#             wempleado = wdata.get('empleado') or ''
#             if wempleado != '':
#                 Qfilters.add(
#                     Q(empleado__nombres__icontains=wempleado) |
#                     Q(empleado__apellidos__icontains=wempleado),
#                     Qfilters.connector
#                 )

#             wqueryset = mdl.Reservas.objects.filter(Qfilters) \
#                 .order_by("-fhregistro").distinct()
#             # print(str(wqueryset.query))
#             woutcome = srlz.ReservasSerializer(
#                 wqueryset,
#                 many=True,
#             ).data
#             wresponse['result'] = woutcome
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     def validatingLayer(self, request, **kwargs):
#         wresponse = self.defaultResponse.copy()
#         status = True
#         error = None
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             wsolicitante = wdata.get("solicitante") or 0
#             wfecha = wdata.get("fecha") or None
#             westado = wdata.get("estado")
#             wsala = wdata.get("sala")
#             wplanta = wdata.get("planta")
#             wtime_from = '{} {}'.format(wfecha, wdata.get("time_from"))
#             wtime_to = '{} {}'.format(wfecha, wdata.get("time_to"))

#             if kwargs.get("process"):
#                 if kwargs.get("process") == "update":
#                     widreserva = wdata.get("id")
#                     wobjReserva = mdl.Reservas.objects.filter(id=widreserva)
#                     if not wobjReserva.exists():
#                         raise ValueError('Error, reserva invalido.')

#             objSolicitante = mdlmetas.Empleados.objects \
#                 .filter(id=int(wsolicitante))
#             if not objSolicitante.exists():
#                 raise ValueError('Error, solicitante invalido.')

#             if wfecha is None:
#                 raise ValueError('Error, fecha invalido.')
#             wfecha, wstatus = Validators.isDate(wfecha, "%Y-%m-%d")
#             if not wstatus:
#                 raise ValueError('Error, fecha invalido.')

#             wtime_format = "%Y-%m-%d %H:%M"
#             if wtime_from is None:
#                 raise ValueError('Error, horario inicio invalido.')
#             wtime_from, wstatus = Validators.isDate(wtime_from, wtime_format)
#             if not wstatus:
#                 raise ValueError('Error, horario inicio invalido.')

#             if wtime_to is None:
#                 raise ValueError('Error, horario final invalido.')
#             wtime_to, wstatus = Validators.isDate(wtime_to, wtime_format)
#             if not wstatus:
#                 raise ValueError('Error, horario final invalido.')

#             objSala = mdlcat.Sala.objects.filter(id=int(wsala))
#             if not objSala.exists():
#                 raise ValueError('Error, sala invalido.')
#             objPlanta = mdlcat.Planta.objects.filter(id=int(wplanta))
#             if not objPlanta.exists():
#                 raise ValueError('Error, planta invalido.')
#             objEstado = mdlmetas.Estados.objects.filter(id=int(westado))
#             if not objEstado.exists():
#                 raise ValueError('Error, estado invalido.')
#         except Exception as e:
#             status = False
#             error = str(e)
#         return [status, error]

#     def saveDetails(self, request, **kwargs):
#         status = True
#         error = None
#         try:
#             objReserva = kwargs.get("reserva")
#             wdetails = kwargs.get("participantes")
#             if objReserva.pk:
#                 if len(wdetails) > 0:
#                     wparticipantes = []
#                     mdl.ReservasParticipantes.objects.filter(
#                         reserva=objReserva).delete()
#                     for item in wdetails:
#                         objEmpleado = mdlmetas.Empleados.objects.get(
#                             id=int(item.get("empleado").get("id")))
#                         wkeys = {
#                             "reserva": objReserva,
#                             "estado": mdlmetas.Estados.objects.get(id=1),
#                             "empleado": objEmpleado,
#                         }
#                         obj = mdl.ReservasParticipantes(**wkeys)
#                         wparticipantes.append(obj)
#                     mdl.ReservasParticipantes.objects.bulk_create(
#                         wparticipantes)
#         except Exception as e:
#             error = str(e)
#             status = False
#         return [error, status]

#     @md(perm('reservas.add_reservas', raise_exception=True),)
#     def saveInformation(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             waddition = {'process': 'new'}
#             wstatus, werror = self.validatingLayer(request, **waddition)
#             if werror:
#                 raise ValueError(str(werror))

#             wsolicitante = wdata.get("solicitante")
#             wfecha = wdata.get("fecha") or None
#             wfecha, wstatus = Validators.isDate(wfecha, "%Y-%m-%d")
#             westado = wdata.get("estado")
#             wsala = wdata.get("sala")
#             wplanta = wdata.get("planta")
#             wtime_format = "%Y-%m-%d %H:%M"
#             wtime_from = '{} {}'.format(
#                 wdata.get("fecha"), wdata.get("time_from"))
#             wtime_from, wstatus = Validators.isDate(wtime_from, wtime_format)
#             wtime_to = '{} {}'.format(wdata.get("fecha"), wdata.get("time_to"))
#             wtime_to, wstatus = Validators.isDate(wtime_to, wtime_format)
#             wdetails = wdata.get("participantes") or list()
#             wparameters = {
#                 "empleado": mdlmetas.Empleados.objects
#                 .get(id=int(wsolicitante)),
#                 "fecha_evento": wfecha,
#                 "sala": mdlcat.Sala.objects.get(id=int(wsala)),
#                 "planta": mdlcat.Planta.objects.get(id=int(wplanta)),
#                 "estado": mdlmetas.Estados.objects.get(id=int(westado)),
#                 "hora_inicio": wtime_from,
#                 "hora_final": wtime_to,
#             }
#             objReserva = mdl.Reservas(**wparameters)
#             objReserva.save()
#             error, status = self.saveDetails(request, **{
#                 "reserva": objReserva,
#                 "participantes": wdetails,
#             })
#             if error:
#                 raise ValueError(str(error))
#             wresponse['result'] = {
#                 "id": objReserva.pk,
#                 "message": "Reserva procesada satisfactoriamente.",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @md(perm('reservas.delete_reservas', raise_exception=True),)
#     def removeReserva(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             widreserva = wdata.get("item").get("id") or 0
#             objReserva = mdl.Reservas.objects.filter(id=int(widreserva))
#             if not objReserva.exists():
#                 raise ValueError('Error, reserva especificada no existe.')
#             objReserva.get().delete()
#             wresponse['result'] = {
#                 "message": "Reserva fue eliminado satisfactoriamente.",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @md(perm('reservas.change_reservas', raise_exception=True),)
#     def updateReserva(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             waddition = {'process': 'update'}
#             wstatus, werror = self.validatingLayer(request, **waddition)
#             if werror:
#                 raise ValueError(str(werror))

#             wtime_format = "%Y-%m-%d %H:%M"
#             wsolicitante = wdata.get("solicitante")
#             westado = wdata.get("estado")
#             wsala = wdata.get("sala")
#             wplanta = wdata.get("planta")
#             wfecha = wdata.get("fecha")
#             wfecha, wstatus = Validators.isDate(wfecha, "%Y-%m-%d")
#             wtime_from = '{} {}'.format(
#                 wdata.get("fecha"), wdata.get("time_from"))
#             wtime_from, wstatus = Validators.isDate(wtime_from, wtime_format)
#             wtime_to = '{} {}'.format(wdata.get("fecha"), wdata.get("time_to"))
#             wtime_to, wstatus = Validators.isDate(wtime_to, wtime_format)
#             wdetails = wdata.get("participantes") or list()

#             widreserva = wdata.get("id") or 0
#             objReserva = mdl.Reservas.objects.get(id=widreserva)
#             objReserva.fecha_evento = wfecha
#             objReserva.hora_inicio = wtime_from
#             objReserva.hora_final = wtime_to
#             objReserva.empleado = mdlmetas.Empleados.objects.get(
#                 id=int(wsolicitante))
#             objReserva.sala = mdlcat.Sala.objects.get(id=int(wsala))
#             objReserva.planta = mdlcat.Planta.objects.get(id=int(wplanta))
#             objReserva.estado = mdlmetas.Estados.objects.get(id=int(westado))
#             objReserva.save()
#             error, status = self.saveDetails(request, **{
#                 "reserva": objReserva,
#                 "participantes": wdetails,
#             })
#             if error:
#                 raise ValueError(str(error))
#             wresponse['result'] = {
#                 "id": objReserva.pk,
#                 "message": "Cambios fueron actualizados satisfactoriamente",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     def getSchedules(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             print(wdata)
#             wschedules = getScheduleList()
#             wresponse["result"] = {
#                 "schedules": wschedules,
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))
