#! /usr/bin/env python
# -*- coding:utf-8 -*-

from rest_framework import serializers
from apps.reservas import models as m
from apps.metas import serializers as srlmetas
from apps.catalogos import serializers as srlcat


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class FeriadosSerializer(DynamicFieldsModelSerializer):
    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.Feriados
        fields = '__all__'


class ReservasSerializer(DynamicFieldsModelSerializer):
    empleado = srlmetas.EmpleadosSerializer(
        fields=['id', 'ndoc', 'nombres', 'apellidos'], read_only=True,)
    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)
    sala = srlcat.SalaSerializer(
        fields=['id', 'descripcion'], read_only=True,)
    planta = srlcat.PlantaSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.Reservas
        fields = '__all__'


class ReservasParticipantesSerializer(DynamicFieldsModelSerializer):
    empleado = srlmetas.EmpleadosSerializer(
        fields=['id', 'ndoc', 'nombres', 'apellidos'], read_only=True,)
    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)
    reserva = ReservasSerializer(fields=['id', ], read_only=True,)

    class Meta:
        model = m.ReservasParticipantes
        fields = '__all__'
