from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from apps.reservas import views as v

app_name = 'module_reservas'

# urlpatterns = [
#     url(r'^reservas_salas/$', v.ViewReservasSalas.as_view(), name='reservas_salas'),
#     url(r'^reservas_feriados/$', v.ViewReservasFeriados.as_view(),
#         name='reservas_feriados'),
#     url(r'^reservas_calendario/$',
#         v.ViewReservasCalendario.as_view(), name='reservas_calendario'),
# ]
