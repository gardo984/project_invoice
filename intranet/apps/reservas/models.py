from django.db import models
from apps.common.models import CommonStructure
from apps.connections.config import MYSQLConnection
from datetime import datetime
from decimal import Decimal

# Create your models here.

DEFAULT_STATUS = 1


class Reservas(CommonStructure):
    planta = models.ForeignKey('catalogos.Planta', on_delete=models.CASCADE, )
    sala = models.ForeignKey('catalogos.Sala', on_delete=models.CASCADE, )
    empleado = models.ForeignKey('metas.Empleados', on_delete=models.CASCADE,)
    fecha_evento = models.DateField(
        null=False, blank=False, default=datetime.now,)
    hora_inicio = models.TimeField(
        default=datetime.now, auto_now=False, auto_now_add=False,)
    hora_final = models.TimeField(
        default=None, auto_now=False, auto_now_add=False,)
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS,)

    class Meta:
        verbose_name = "Reservas"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        unique_together = [
            'sala', 'fecha_evento', 'hora_inicio',
        ]
        indexes = [
            models.Index(fields=['fecha_evento'],),
            models.Index(fields=['hora_inicio'],),
            models.Index(fields=['hora_final'],),
        ]

    def __str__(self):
        return str(self.pk)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class ReservasParticipantes(CommonStructure):
    reserva = models.ForeignKey(
        'reservas.Reservas', on_delete=models.CASCADE, )
    empleado = models.ForeignKey('metas.Empleados', on_delete=models.CASCADE,)
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS,)

    class Meta:
        verbose_name = "Reservas Participantes"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        unique_together = [
            'reserva', 'empleado',
        ]
        indexes = [
            models.Index(fields=['fhregistro'],),
        ]

    def __str__(self):
        return str(self.pk)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Feriados(CommonStructure):
    fecha = models.DateField(null=False, blank=False, default=None,)
    descripcion = models.CharField(max_length=100, null=False, blank=False,
                                   default=None,)
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS,)

    class Meta:
        verbose_name = "Feriados"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['fecha'],),
            models.Index(fields=['fhregistro'],),
        ]

    def __str__(self):
        return str(self.fecha)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)
