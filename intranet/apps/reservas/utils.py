#! /usr/bin/env python
# -*- encoding:utf8 -*-

from datetime import datetime, timedelta
import re


def getScheduleList():
    MINUTES_INTERVAL = 30
    TIME_FORMAT = '%H:%M'
    DATE_FORMAT = '%Y-%m-%d'
    WHILE_STATUS = True
    FROM_TIME = 8
    LIMIT_TIME = '23:30'
    wdate = '{} 00:00'.format(datetime.now().strftime(DATE_FORMAT))
    current_time = datetime.strptime(
        wdate, '{} {}'.format(DATE_FORMAT, TIME_FORMAT))
    list_schedule = list()
    while WHILE_STATUS:
        time_from = current_time.strftime(TIME_FORMAT)
        current_time += timedelta(minutes=MINUTES_INTERVAL)
        time_to = current_time.strftime(TIME_FORMAT)
        time_value = '{} - {}'.format(time_from, time_to)
        if int(current_time.strftime("%H")) >= FROM_TIME:
            list_schedule.append(time_value)
        if current_time.strftime(TIME_FORMAT) == LIMIT_TIME:
            WHILE_STATUS = False
    wkeys = ['id', 'time_value']
    wresult = [dict(zip(wkeys, [re.sub(r':|-| ', '', item), item]))
               for item in list_schedule]
    return wresult
