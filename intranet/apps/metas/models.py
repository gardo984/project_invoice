from django.db import models
from apps.common.models import CommonStructure
from apps.connections.config import MYSQLConnection
from datetime import datetime
from decimal import Decimal
# Create your models here.


DEFAULT_STATUS = 1


# class Estados(CommonStructure):
#     descripcion = models.CharField(max_length=200, null=False, blank=False,)
#     estado = models.IntegerField(null=False, default=1, blank=False)

#     class Meta:
#         verbose_name = "Estados"
#         get_latest_by = ["fhregistro", ]
#         ordering = ["fhregistro", ]
#         indexes = [
#             models.Index(fields=['descripcion'],),
#         ]

#     def __str__(self):
#         return self.descripcion

#     def save(self, *args, **kwargs):
#         if not self.id is None:
#             self.fhmodificacion = datetime.now()
#         super().save(*args, **kwargs)


# class Marca(CommonStructure):
#     nombre = models.CharField(max_length=200, null=False, blank=False,)
#     cod_marca = models.CharField(max_length=25, null=False, blank=False,)
#     estado = models.ForeignKey(
#         Estados, on_delete=models.CASCADE, default=DEFAULT_STATUS)

#     class Meta:
#         verbose_name = "Marca"
#         get_latest_by = ["fhregistro", ]
#         ordering = ["fhregistro", ]
#         indexes = [
#             models.Index(fields=['nombre'],),
#             models.Index(fields=['cod_marca'],),
#         ]

#     def __str__(self):
#         return self.nombre

#     def save(self, *args, **kwargs):
#         if not self.id is None:
#             self.fhmodificacion = datetime.now()
#         super().save(*args, **kwargs)


# class Concesionario(CommonStructure):
#     nombre = models.CharField(max_length=200, null=False, blank=False,)
#     ndoc = models.CharField(max_length=25, null=False, blank=False,)
#     estado = models.ForeignKey(
#         Estados, on_delete=models.CASCADE, default=DEFAULT_STATUS)

#     class Meta:
#         verbose_name = "Concesionario"
#         get_latest_by = ["fhregistro", ]
#         ordering = ["fhregistro", ]
#         indexes = [
#             models.Index(fields=['nombre'],),
#             models.Index(fields=['ndoc'],),
#         ]

#     def __str__(self):
#         return self.nombre

#     def save(self, *args, **kwargs):
#         if not self.id is None:
#             self.fhmodificacion = datetime.now()
#         super().save(*args, **kwargs)



# class MarcaJefeConcesionario(CommonStructure):
#     marca = models.ForeignKey(Marca, on_delete=models.CASCADE)
#     concesionario = models.ForeignKey(Concesionario, on_delete=models.CASCADE)
#     jefe = models.ForeignKey(Empleados, on_delete=models.CASCADE)

#     class Meta:
#         verbose_name = "Marca Jefe Concesionario"
#         ordering = ['fhregistro', ]
#         get_latest_by = ['fhregistro', ]
#         indexes = [
#             models.Index(fields=['fhregistro'],),
#             models.Index(fields=['fhmodificacion'],),
#         ]

#     def __str__(self):
#         return str(self.pk)


# class MetaJefe(CommonStructure):
#     marca = models.ForeignKey(Marca, on_delete=models.CASCADE)
#     jefe = models.ForeignKey(Empleados, on_delete=models.CASCADE)
#     anio = models.IntegerField(null=False, default=0)
#     mes = models.IntegerField(null=False, default=0)
#     meta = models.IntegerField(null=False, default=0)

#     class Meta:
#         verbose_name = "Meta Jefe"
#         ordering = ['fhregistro', ]
#         get_latest_by = ['fhregistro', ]
#         indexes = [
#             models.Index(fields=['fhregistro'],),
#             models.Index(fields=['fhmodificacion'],),
#             models.Index(fields=['anio'],),
#             models.Index(fields=['mes'],),
#             models.Index(fields=['meta'],),
#         ]

#     def __str__(self):
#         return str(self.pk)

#     def save(self, *args, **kwargs):
#         if not self.id is None:
#             self.fhmodificacion = datetime.now()
#         super().save(*args, **kwargs)


# class MetaMarca(CommonStructure):
#     marca = models.ForeignKey(Marca, on_delete=models.CASCADE)
#     anio = models.IntegerField(null=False, default=0)
#     mes = models.IntegerField(null=False, default=0)
#     meta = models.IntegerField(null=False, default=0)

#     class Meta:
#         verbose_name = "Meta Marca"
#         ordering = ['fhregistro', ]
#         get_latest_by = ['fhregistro', ]
#         indexes = [
#             models.Index(fields=['fhregistro'],),
#             models.Index(fields=['fhmodificacion'],),
#             models.Index(fields=['anio'],),
#             models.Index(fields=['mes'],),
#             models.Index(fields=['meta'],),
#         ]

#     def __str__(self):
#         return str(self.pk)

#     def save(self, *args, **kwargs):
#         if not self.id is None:
#             self.fhmodificacion = datetime.now()
#         super().save(*args, **kwargs)

