#! /usr/bin/env python
# -*- coding:utf-8 -*-

from rest_framework import serializers
from apps.metas import models as m


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)






# class MarcaSerializer(DynamicFieldsModelSerializer):

#     class Meta:
#         model = m.Marca
#         fields = '__all__'


# class ConcesionarioSerializer(DynamicFieldsModelSerializer):

#     # asignado_status = serializers.SerializerMethodField()
#     asignado = serializers.CharField()

#     class Meta:
#         model = m.Concesionario
#         fields = '__all__'

#     # def get_asignado_status(self,obj):
#     #     return self.asignado or 0


# class MetaJefeSerializer(DynamicFieldsModelSerializer):

#     jefe = EmpleadosSerializer(
#         fields=['id', 'ndoc', 'nombres', 'apellidos', ],
#         read_only=True
#     )
#     marca = MarcaSerializer(
#         fields=['id', 'nombre', 'cod_marca', ],
#         read_only=True
#     )

#     class Meta:
#         model = m.MetaJefe
#         fields = '__all__'
