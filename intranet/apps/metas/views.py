from django.shortcuts import render
from django.views import View
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import JsonResponse
from apps.metas import (
    models as mdl,
    serializers as srlz,
    querys as q
)
from django.db.models import Value, Case, When, CharField
from apps.connections.config import (
    MYSQLConnection,
)
import json

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import (
    login_required,
    permission_required,
)
from django.core.exceptions import PermissionDenied

# Create your views here.

DEFAULT_FORBIDDEN_PAGE = '/login/forbidden'


# @method_decorator(login_required, name='dispatch')
# class ViewMetaAsignaConcesionario(View):
#     template_name = 'metas/meta_asigna_concesionario.html'
#     template_header = 'SKBerge | Asignación Concesionarios'
#     template_title = 'Asignación de Concesionarios'
#     defaultResponse = {
#         "ok": 1,
#         "result": 'Process has been processed successfuly.',
#         "status": 200,
#     }

#     def dispatch(self, *args, **kwargs):
#         return super(ViewMetaAsignaConcesionario, self).dispatch(*args, **kwargs)

#     @method_decorator(permission_required(
#         'metas.add_marcajefeconcesionario', login_url=DEFAULT_FORBIDDEN_PAGE,),)
#     def get(self, request):
#         wresponse = self.defaultResponse.copy()
#         header = self.template_header
#         title = self.template_title
#         return render(request, self.template_name, locals())

#     @method_decorator(permission_required(
#         'metas.add_marcajefeconcesionario', raise_exception=True),)
#     def post(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             if not request.is_ajax():
#                 wresponse['status'] = 400
#                 raise ValueError(str('Bad Request'))
#             if wdata.get("wtype"):
#                 wtype = wdata.get("wtype")
#                 if wtype == "save_information":
#                     return self.saveInformation(request)
#                 if wtype == "get_metas_concesionarios":
#                     return self.getMetasConcesionarios(request)
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if type(e) is PermissionDenied:
#                 wresponse['status'], wresponse[
#                     'result'] = 403, 'Permission Denied'
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @method_decorator(permission_required('metas.add_marcajefeconcesionario', raise_exception=True),)
#     def getMetasConcesionarios(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             connection = MYSQLConnection()
#             wdata = json.loads(request.body.decode("utf8"))
#             wfilters = {}
#             wjefe = wdata.get('jefe') or 'null'
#             wmarca = wdata.get('marca') or 'null'
#             wcmd = q.cmd_listar_concesionarios.format(wmarca, wjefe)
#             werror, wresult = connection.get_query_dict(wcmd)
#             if werror:
#                 raise ValueError(str(werror))
#             wresponse['result'] = wresult
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#             # if wjefe != '':
#             #     wfilters['jefe'] = mdl.Empleados.objects.get(id=int(wjefe))
#             # if wmarca != '':
#             #     wfilters['marca'] = mdl.Marca.objects.get(id=int(wmarca))

#             # wqueryset = mdl.Concesionario.objects.values("id", "nombre", "ndoc") \
#             #     .annotate(asignado=Case(
#             #         When(
#             #             marcajefeconcesionario__id__isnull=False,
#             #             then=Value("1")),
#             #         default=Value("0"),
#             #         output_field=CharField(),)
#             # ).filter(
#             #     marcajefeconcesionario__marca=wfilters.get("marca"),
#             #     marcajefeconcesionario__jefe=wfilters.get("jefe")) \
#             #     .exclude(estado__id=2).order_by("nombre").distinct()
#             # print(str(wqueryset.query))
#             # wfields = [
#             #     "id", "nombre", "ndoc", "asignado"
#             # ]
#             # woutcome = srlz.ConcesionarioSerializer(
#             #     wqueryset,
#             #     many=True,
#             #     fields=wfields,
#             # ).data
#             # wresponse['result'] = woutcome
#             # return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @method_decorator(permission_required('metas.change_marcajefeconcesionario', raise_exception=True),)
#     def saveInformation(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             witems = wdata.get("concesionarios")
#             wmarca = wdata.get("marca")
#             wjefe = wdata.get("jefe")
#             objMarca = mdl.Marca.objects.get(id=int(wmarca))
#             objEmpleado = mdl.Empleados.objects.get(id=int(wjefe))
#             # first remove any existing details
#             mdl.MarcaJefeConcesionario.objects.filter(
#                 marca=objMarca, jefe=objEmpleado
#             ).delete()
#             # then insert the new items
#             for item in witems:
#                 wconcesionario = int(item.get("id") or 0)
#                 objAsignacion = mdl.MarcaJefeConcesionario()
#                 objAsignacion.marca = objMarca
#                 objAsignacion.concesionario = mdl.Concesionario.objects.get(
#                     id=wconcesionario)
#                 objAsignacion.jefe = objEmpleado
#                 objAsignacion.save()

#             wresponse['result'] = {
#                 "id": "",
#                 "message": "Asignación de Concesionarios procesada satisfactoriamente.",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))


# @method_decorator(login_required, name='dispatch')
# class ViewMetaJefe(View):
#     template_name = 'metas/meta_jefe.html'
#     template_header = 'SKBerge | Metas por Jefe'
#     template_title = 'Metas por Jefe'
#     defaultResponse = {
#         "ok": 1,
#         "result": 'Process has been processed successfuly.',
#         "status": 200,
#     }

#     def dispatch(self, *args, **kwargs):
#         return super(ViewMetaJefe, self).dispatch(*args, **kwargs)

#     @method_decorator(permission_required('metas.add_metajefe', login_url=DEFAULT_FORBIDDEN_PAGE,),)
#     def get(self, request):
#         wresponse = self.defaultResponse.copy()
#         header = self.template_header
#         title = self.template_title
#         return render(request, self.template_name, locals())

#     @method_decorator(permission_required('metas.add_metajefe', raise_exception=True),)
#     def post(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             if not request.is_ajax():
#                 wresponse['status'] = 400
#                 raise ValueError(str('Bad Request'))
#             if wdata.get("wtype"):
#                 wtype = wdata.get("wtype")
#                 if wtype == "save_information":
#                     return self.saveInformation(request)
#                 if wtype == "get_metas_jefe":
#                     return self.getMetasJefe(request)
#                 if wtype == "remove_meta_jefe":
#                     return self.removeMeta(request)
#                 if wtype == "update_meta_jefe":
#                     return self.updateMeta(request)
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if type(e) is PermissionDenied:
#                 wresponse['status'], wresponse[
#                     'result'] = 403, 'Permission Denied'
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @method_decorator(permission_required('metas.add_metajefe', raise_exception=True),)
#     def getMetasJefe(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             wfilters = {}
#             wjefe = wdata.get('jefe') or ''
#             wmarca = wdata.get('marca') or ''
#             if wjefe != '':
#                 wfilters['jefe'] = mdl.Empleados.objects.get(id=int(wjefe))
#             if wmarca != '':
#                 wfilters['marca'] = mdl.Marca.objects.get(id=int(wmarca))
#             if not wdata.get('anio') is None:
#                 wfilters['anio'] = int(wdata.get('anio'))
#             if not wdata.get('mes') is None:
#                 wfilters['mes'] = int(wdata.get('mes'))

#             if len(wfilters) > 0:
#                 wqueryset = mdl.MetaJefe.objects.filter(**wfilters)
#             else:
#                 wqueryset = mdl.MetaJefe.objects.all()
#             woutcome = srlz.MetaJefeSerializer(wqueryset, many=True).data
#             wresponse['result'] = woutcome
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @method_decorator(permission_required('metas.add_metajefe', raise_exception=True),)
#     def saveInformation(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             wmarca = mdl.Marca.objects.get(id=int(wdata.get("marca")))
#             wjefe = mdl.Empleados.objects.get(id=int(wdata.get("jefe")))
#             objMeta = mdl.MetaJefe(
#                 marca=wmarca,
#                 jefe=wjefe,
#                 anio=int(wdata.get("anio")),
#                 mes=int(wdata.get("mes")),
#                 meta=int(wdata.get("meta")),
#             )
#             objMeta.save()
#             wresponse['result'] = {
#                 "id": objMeta.pk,
#                 "message": "Meta fue registrada satisfactoriamente",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @method_decorator(permission_required('metas.change_metajefe', raise_exception=True),)
#     def updateMeta(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             widmeta = wdata.get("id") or 0
#             objMeta = mdl.MetaJefe.objects.get(id=int(widmeta))
#             wmarca = mdl.Marca.objects.get(id=int(wdata.get("marca")))
#             wjefe = mdl.Empleados.objects.get(id=int(wdata.get("jefe")))
#             objMeta.marca = wmarca
#             objMeta.jefe = wjefe
#             objMeta.anio = int(wdata.get("anio"))
#             objMeta.mes = int(wdata.get("mes"))
#             objMeta.meta = int(wdata.get("meta"))
#             objMeta.save()
#             wresponse['result'] = {
#                 "id": objMeta.pk,
#                 "message": "Cambios fueron actualizados satisfactoriamente",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @method_decorator(permission_required('metas.delete_metajefe', raise_exception=True),)
#     def removeMeta(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             widmeta = wdata.get("item").get("id") or 0
#             mdl.MetaJefe.objects.get(id=int(widmeta)).delete()
#             wresponse['result'] = {
#                 "message": "Meta fue eliminada satisfactoriamente.",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))


# @method_decorator(login_required, name='dispatch')
# class ViewMetaMarca(View):
#     template_name = 'metas/meta_marca.html'
#     template_header = 'SKBerge | Metas por Marca'
#     template_title = 'Metas por Marca'
#     defaultResponse = {
#         "ok": 1,
#         "result": 'Process has been processed successfuly.',
#         "status": 200,
#     }

#     def dispatch(self, *args, **kwargs):
#         return super(ViewMetaMarca, self).dispatch(*args, **kwargs)

#     @method_decorator(permission_required('metas.add_metamarca', login_url=DEFAULT_FORBIDDEN_PAGE,),)
#     def get(self, request):
#         wresponse = self.defaultResponse.copy()
#         header = self.template_header
#         title = self.template_title
#         return render(request, self.template_name, locals())

#     @method_decorator(permission_required('metas.add_metamarca', raise_exception=True),)
#     def post(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             if not request.is_ajax():
#                 wresponse['status'] = 400
#                 raise ValueError(str('Bad Request'))
#             if wdata.get("wtype"):
#                 wtype = wdata.get("wtype")
#                 if wtype == "save_information":
#                     return self.saveInformation(request)
#                 if wtype == "get_metas_marca":
#                     return self.getMetasMarca(request)
#                 if wtype == "remove_meta_marca":
#                     return self.removeMeta(request)
#                 if wtype == "update_meta_marca":
#                     return self.updateMeta(request)
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if type(e) is PermissionDenied:
#                 wresponse['status'], wresponse[
#                     'result'] = 403, 'Permission Denied'
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @method_decorator(permission_required('metas.change_metamarca', raise_exception=True),)
#     def updateMeta(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             widmeta = wdata.get("id") or 0
#             objMeta = mdl.MetaMarca.objects.get(id=int(widmeta))
#             wmarca = mdl.Marca.objects.get(id=int(wdata.get("marca")))
#             objMeta.marca = wmarca
#             objMeta.anio = int(wdata.get("anio"))
#             objMeta.mes = int(wdata.get("mes"))
#             objMeta.meta = int(wdata.get("meta"))
#             objMeta.save()
#             wresponse['result'] = {
#                 "id": objMeta.pk,
#                 "message": "Cambios fueron actualizados satisfactoriamente",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @method_decorator(permission_required('metas.delete_metamarca', raise_exception=True),)
#     def removeMeta(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             widmeta = wdata.get("item").get("id") or 0
#             mdl.MetaMarca.objects.get(id=int(widmeta)).delete()
#             wresponse['result'] = {
#                 "message": "Meta fue eliminada satisfactoriamente.",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @method_decorator(permission_required('metas.add_metamarca', raise_exception=True),)
#     def getMetasMarca(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             wfilters = {}
#             wmarca = wdata.get('marca') or ''
#             if wmarca != '':
#                 wfilters['marca'] = mdl.Marca.objects.get(id=int(wmarca))
#             if not wdata.get('anio') is None:
#                 wfilters['anio'] = int(wdata.get('anio'))
#             if not wdata.get('mes') is None:
#                 wfilters['mes'] = int(wdata.get('mes'))

#             if len(wfilters) > 0:
#                 wqueryset = mdl.MetaMarca.objects.filter(**wfilters)
#             else:
#                 wqueryset = mdl.MetaMarca.objects.all()
#             woutcome = srlz.MetaJefeSerializer(wqueryset, many=True).data
#             wresponse['result'] = woutcome
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))

#     @method_decorator(permission_required('metas.add_metamarca', raise_exception=True),)
#     def saveInformation(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             wdata = json.loads(request.body.decode("utf8"))
#             wmarca = mdl.Marca.objects.get(id=int(wdata.get("marca")))
#             objMeta = mdl.MetaMarca(
#                 marca=wmarca,
#                 anio=int(wdata.get("anio")),
#                 mes=int(wdata.get("mes")),
#                 meta=int(wdata.get("meta")),
#             )
#             objMeta.save()
#             wresponse['result'] = {
#                 "id": objMeta.pk,
#                 "message": "Meta fue registrada satisfactoriamente",
#             }
#             return JsonResponse(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return JsonResponse(wresponse, status=wresponse.get("status"))
