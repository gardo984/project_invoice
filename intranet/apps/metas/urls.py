from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from apps.metas import views as v

app_name = 'module_metas'

# urlpatterns = [
#     url(r'^meta_jefe/$', v.ViewMetaJefe.as_view(), name='meta_jefe'),
#     url(r'^meta_marca/$', v.ViewMetaMarca.as_view(), name='meta_marca'),
#     url(r'^meta_concesionario/$',
#         v.ViewMetaAsignaConcesionario.as_view(), name='meta_asigna_concesionario'),
# ]
