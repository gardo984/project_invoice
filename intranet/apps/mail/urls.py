from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from apps.mail import views as v

app_name = 'module_mail'

urlpatterns = [
    url(r'^vista/$', v.ViewTemplateMail.as_view(), name='template_mail'),
]
