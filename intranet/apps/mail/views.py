from django.shortcuts import render
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template

from django.conf import settings
from django.shortcuts import render
from django.views import View

# Create your views here.


class SendMailer(object):
    """docstring for ClassName"""
    _from = None
    _to = None
    _cc = None
    _bcc = None
    _subject = 'SKBerge Mailer - '
    _body = None
    _template = None
    _message = None

    def __init__(self, **kwargs):
        # super(ClassName, self).__init__()
        # self.arg = arg
        self._from = kwargs.get('from') or settings.DEFAULT_FROM_EMAIL
        self._to = kwargs.get('to') or list()
        self._cc = kwargs.get('cc') or list()
        self._bcc = kwargs.get('bcc') or list()
        self._subject += kwargs.get(
            'subject') or 'Default subject from mail server'
        self._body = kwargs.get(
            'body') or 'Default Content Body from mail server'
        self._template = kwargs.get('template') or ''
        self._context = kwargs.get('context') or dict({})

    def BuildMessage(self):
        status = True
        error = None
        try:
            if self._template:
                wtemplate = get_template(self._template)
                html_content = wtemplate.render(self._context)
                self._message = EmailMessage(
                    subject=self._subject,
                    body=html_content,
                    from_email=self._from,
                    to=self._to,
                    cc=self._cc,
                    bcc=self._bcc,
                )
                self._message.content_subtype = "html"
            else:
                self._message = EmailMessage(
                    subject=self._subject,
                    body=self._body,
                    from_email=self._from,
                    to=self._to,
                    cc=self._cc,
                    bcc=self._bcc,
                )
        except Exception as e:
            status = False
            error = str(e)
        return [error, status]

    def Send(self):
        status = True
        error = None
        try:
            self._message.send()
        except Exception as e:
            status = False
            error = str(e)
        return [error, status]


class ViewTemplateMail(View):
    template_name = 'mail/mail_template.html'

    def get(self, request):
        return render(request, self.template_name, locals())
