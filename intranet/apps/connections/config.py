
import pymssql
import MySQLdb
from intranet.utils import get_config_parameters


class SQLConnection(object):

    _connection = None
    _cursor = None
    _error = None

    def __init__(self, *args, **kwargs):
        self.connection()

    def connection(self):
        wparam = get_config_parameters()
        try:
            self._connection = pymssql.connect(
                wparam.get('SQL_HOST'),
                wparam.get('SQL_USER'),
                wparam.get('SQL_PASSWORD'),
                wparam.get('SQL_DATABASE'),
                port=wparam.get('SQL_PORT'),
            )
            self._cursor = self._connection.cursor(as_dict=True)
        except Exception as e:
            self._error = str(e)
            print(self._error)

    def get_connection(self):
        return self._connection

    def get_cursor(self):
        return self._cursor

    def get_query(self, sentence):
        error = None
        result = None
        try:
            self._cursor.execute(sentence)
            result = self._cursor.fetchall()
        except Exception as e:
            error = str(e)
        return [error, result]

    def get_query_dict(self, sentence):
        error = None
        result = None
        try:
            self._cursor.execute(sentence)
            wcolumns = [item[0] for item in self._cursor.description]
            wrows = self._cursor.fetchall()
            result = [ dict(zip(wcolumns,item)) for item in wrows ]
        except Exception as e:
            error = str(e)
        return [error, result]
        
    def get_query_with_headers(self, sentence):
        error = None
        result = None
        try:
            self._cursor.execute(sentence)
            wcolumns = [item[0] for item in self._cursor.description]
            wrows = self._cursor.fetchall()
            result = {
                "columns": wcolumns,
                "rows": wrows,
            }
        except Exception as e:
            error = str(e)
        return [error, result]


class MYSQLConnection(object):

    _connection = None
    _cursor = None
    _error = None

    def __init__(self, *args, **kwargs):
        self.connection()

    def connection(self):
        wparam = get_config_parameters()
        try:
            self._connection = MySQLdb.connect(
                wparam.get('MYSQL_HOST'),
                wparam.get('MYSQL_USER'),
                wparam.get('MYSQL_PASSWORD'),
                wparam.get('MYSQL_DATABASE'),
            )
            self._cursor = self._connection.cursor()
        except Exception as e:
            self._error = str(e)
            print(self._error)

    def execute_query(self, sentence):
        status = True
        error = None
        try:
            self._cursor.execute(sentence)
        except Exception as e:
            status = False
            error = str(e)
        return [error, status]

    def execute_insert(self, sentence, values):
        status = True
        error = None
        try:
            self._cursor.executemany(sentence, values)
            self._connection.commit()
        except Exception as e:
            status = False
            error = str(e)
        return [error, status]

    def get_query(self, sentence):
        error = None
        result = None
        try:
            self._cursor.execute(sentence)
            result = self._cursor.fetchall()
        except Exception as e:
            error = str(e)
        return [error, result]

    def get_query_dict(self, sentence):
        error = None
        result = None
        try:
            self._cursor.execute(sentence)
            wcolumns = [item[0] for item in self._cursor.description]
            wrows = self._cursor.fetchall()
            result = [ dict(zip(wcolumns,item)) for item in wrows ]
        except Exception as e:
            error = str(e)
        return [error, result]

    def get_query_with_headers(self, sentence):
        error = None
        result = None
        try:
            self._cursor.execute(sentence)
            wcolumns = [item[0] for item in self._cursor.description]
            wrows = self._cursor.fetchall()
            result = {
                "columns": wcolumns,
                "rows": wrows,
            }
        except Exception as e:
            error = str(e)
        return [error, result]

    def get_connection(self):
        return self._connection

    def get_cursor(self):
        return self._cursor
