from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from apps.invoice import views as v

app_name = 'module_invoice'

urlpatterns = [
    url(r'^$', v.ViewInvoice.as_view(), name='main'),
    url(r'^backoffice/$', v.ViewBackOffice.as_view(), name='backoffice'),
    # url(r'^articles/$', v.ViewArticlesView.as_view(), name='articles'),
]
