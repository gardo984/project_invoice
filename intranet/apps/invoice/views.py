
from django.shortcuts import render, redirect
from django.views import View
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import JsonResponse
from django.db.models import (
    Value, Case, When, CharField, Q,
    F, Count, Sum, Avg,
    Prefetch, IntegerField,
)
from apps.connections.config import (
    MYSQLConnection,
)

import json
from datetime import datetime

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import (
    login_required,
    permission_required,
)
from django.core.exceptions import PermissionDenied
from apps.productos import models as mdlprod
from apps.invoice import (
    models as mdls,
    serializers as srlz,
    forms as frm,
)

from apps.catalogos import (
    models as mdlcat,
)
from apps.invoice import utils as utl

# Create your views here.


DEFAULT_FORBIDDEN_PAGE = '/login/forbidden'


@method_decorator(login_required, name='dispatch')
class ViewInvoice(View):
    template_name = 'invoices/invoice.html'
    template_header = 'Invoice | Orders'
    template_title = 'Orders'
    defaultResponse = {
        "ok": 1,
        "result": 'Process has been processed successfuly.',
        "status": 200,
    }

    def dispatch(self, *args, **kwargs):
        return super(ViewInvoice, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'invoice.display_invoiceparent', login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        wresponse = self.defaultResponse.copy()
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "add_invoice":
                    return self.addInvoice(request)
                if wtype == "get_invoices":
                    return self.getInvoices(request)
                if wtype == "get_invoice_item":
                    return self.getInvoiceItem(request)
                if wtype == "remove_invoice":
                    return self.removeInvoice(request)
                if wtype == "update_invoice":
                    return self.updateInvoice(request)
                if wtype == "get_xls_invoice":
                    return self.getInvoiceReport(request)
                if wtype == "get_xls_report":
                    return self.getInvoicesReport(request)
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('invoice.xlsreport_invoiceparent', raise_exception=True),)
    def getInvoicesReport(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            select_fields = [
                "id", "order", "fhregistro_format",
                "fecha_emision", "cliente_nombre",
                "cliente_phone", "ndetails",
                "fhmodificacion_format", "tipo_invoice",
                "total", "forma_pago", "fecha_entrega",
                "username", "username_modif",
                "forma_pago_descrip", "tipo_descrip",
                "vendedor_nombre",
                "pais_descrip",
                "ciudad_descrip",
                "fecha_entrega_reprogramada",
            ]
            wdata.update({
                "user": request.user, "select_fields": select_fields,
            })
            outcome, err = utl.getXlsInvoiceList(**wdata)
            if err:
                raise ValueError(err)

            srlzdata = srlz.InvoiceParentSerializer(
                outcome, fields=select_fields, many=True,
            ).data
            wreport, werror = utl.get_xls_report(request, srlzdata)
            if werror:
                raise ValueError(str(werror))
            wresponse["result"] = wreport
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('invoice.xlsinvoice_invoiceparent', raise_exception=True),)
    def getInvoiceReport(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            winvoice = wdata.get('uid') or ''
            if winvoice == '':
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            Qfilters = Q(id=int(winvoice))
            wqueryset = mdls.InvoiceParent.objects.filter(Qfilters)
            if not wqueryset.exists:
                wresponse['status'] = 400
                raise ValueError(str('Error, invalid Invoice ID specified.'))
            wsource = srlz.InvoiceParentSerializer(wqueryset.get()).data
            for item in wsource.get("invoicedetails_set"):
                item["article_description"] = item.get(
                    "article").get("descripcion")
            wreport, werror = utl.get_xls_invoice(request, wsource)
            if werror:
                raise ValueError(str(werror))
            wresponse['result'] = wreport
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def getInvoiceItem(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            winvoice = wdata.get('id') or ''
            if winvoice == '':
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            Qfilters = Q(id=int(winvoice))

            qsdetails = mdls.InvoiceDetails.objects.all() \
                .select_related("article__categoria__estado")
            qstipodoc = mdlcat.TipoDocumento.objects.all() \
                .select_related("estado")
            qscliente = mdlcat.Clientes.objects.all() \
                .select_related("estado") \
                .prefetch_related(Prefetch(
                    "tdoc", queryset=qstipodoc,
                ))

            wqueryset = mdls.InvoiceParent.objects.filter(Qfilters) \
                .select_related("estado") \
                .prefetch_related(Prefetch("cliente", queryset=qscliente)) \
                .prefetch_related(Prefetch(
                    "invoicedetails_set", queryset=qsdetails,
                ))

            fields = [
                "id", "cliente", "fhregistro", "fhmodificacion",
                "order", "cliente_phone", "cliente_phone2",
                "cliente_address", "cliente_country", "total",
                "observaciones", "fecha_venta", "fecha_entrega",
                "tipo_invoice", "forma_pago", "estado",
                "nro_tarjeta", "invoicedetails_set",
                "observaciones2", "contesta_celular", "labora",
                "hora_entrega", "vendedor",
                "fecha_etd_courier", "fecha_recepcion",
                "tarjeta_cvv", "tarjeta_vigencia",
                "hora_entrega_hasta",
                "delivery_address",
            ]
            if not wqueryset.exists:
                wresponse['status'] = 400
                raise ValueError(str('Error, invalid Invoice ID specified.'))
            woutcome = srlz.InvoiceParentSerializer(
                wqueryset.get(), fields=fields,
            ).data
            for item in woutcome.get("invoicedetails_set"):
                item["check"], item["visible"] = False, True
            wresponse['result'] = woutcome
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('invoice.display_invoiceparent', raise_exception=True),)
    def getInvoices(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            select_fields = [
                "id", "fhregistro", "fhmodificacion",
                "cliente_ndoc",
                "subtotal", "igv", "total", "cliente",
                "order", "fecha_emision", "fecha_vigencia",
                "username", "estado", "forma_pago",
                "username_modif",
                "ndetails", "tipo_invoice", "sub_tipo_invoice",
            ]
            wdata.update({
                "user": request.user, "select_fields": select_fields,
            })

            outcome, err = utl.getInvoicesList(**wdata)
            if err:
                raise ValueError(err)

            page["total"] = outcome.count()
            srlzdata = srlz.InvoiceParentSerializer(
                outcome[wfrom:wto], fields=select_fields, many=True
            ).data
            wresponse['result'] = {
                "rows": srlzdata,
                "page": page,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('invoice.add_invoiceparent', raise_exception=True),)
    def addInvoice(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wdata["order"] = utl.getNewCorrelative()

            f = frm.MFInvoiceParent(wdata)
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            # parent layer validation
            if not f.is_valid():
                werror = f.get_error_first()
                werr_msg = 'Error, {} : {}.'.format(
                    werror.get("field"), werror.get("message"),
                )
                wresponse['status'] = 422
                raise ValueError(werr_msg)

            # details layer validation
            wdetails = wdata.get("articulos") or []
            result, error = self.validateInvoiceDetails(wdetails)
            if error:
                raise ValueError(str(error))
            #--------------------------
            wparameters = f.clean()

            status, err = utl.validateInvoice(**wparameters)
            if err:
                wresponse["status"] = 422
                raise ValueError(str(err))

            wparameters["username"] = request.user
            objInvoice = mdls.InvoiceParent(**wparameters)
            objInvoice.save()
            error = self.updateInvoiceDetails(objInvoice, result)
            if error:
                raise ValueError(str(error))
            wresponse['result'] = {
                "id": objInvoice.pk,
                "message": "Orden fue registrado satisfactoriamente, Nro. {}".format(objInvoice.order),
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def updateInvoiceDetails(self, invoice, details):
        error = None
        try:
            mdls.InvoiceDetails.objects.filter(invoice=invoice).delete()
            for item in details:
                c = mdls.InvoiceDetails(**item)
                c.invoice = invoice
                c.save()
        except Exception as e:
            error = str(e)
        return error

    def validateInvoiceDetails(self, details):
        data, error,  = [], None
        try:
            for item in details:
                wparameters = item.copy()
                wparameters["article"] = item.get("article").get("id")
                f = frm.MFInvoiceDetails(wparameters)
                if not f.is_valid():
                    werror = f.get_error_first()
                    raise ValueError(werror)
                result = f.clean()
                data.append(result)
        except Exception as e:
            error = str(e)
        return [data, error]

    @method_decorator(permission_required('invoice.change_invoiceparent', raise_exception=True),)
    def updateInvoice(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wid = wdata.get("id") or ''
            if wid == '':
                raise ValueError('Error, id es valor requerido.')

            objInvoice = mdls.InvoiceParent.objects.filter(id=int(wid))
            if not objInvoice.exists():
                wresponse['status'] = 400
                raise ValueError("Error, Invalid Invoice ID specified.")

            # parent layer validation
            f = frm.MFInvoiceParent(wdata)
            if not f.is_valid():
                werror = f.get_error_first()
                werr_msg = 'Error, {} : {}.'.format(
                    werror.get("field"), werror.get("message"),
                )
                wresponse['status'] = 400
                raise ValueError(werr_msg)
            wparameters = f.clean()
            # details layer validation
            wdetails = wdata.get("articulos") or []
            result, error = self.validateInvoiceDetails(wdetails)
            if error:
                raise ValueError(str(error))
            wparameters["fhmodificacion"] = datetime.now()
            wparameters["username_modif"] = request.user
            objInvoice.update(**wparameters)
            error = self.updateInvoiceDetails(objInvoice.get(), result)
            if error:
                raise ValueError(str(error))
            wresponse['result'] = {
                "id": objInvoice.get().id,
                "message": "Datos fueron procesados satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('invoice.delete_invoiceparent', raise_exception=True),)
    def removeInvoice(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widinvoice = wdata.get("item").get("id") or 0
            mdls.InvoiceParent.objects.get(id=int(widinvoice)).delete()
            wresponse['result'] = {
                "message": "Orden fue eliminado satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ViewBackOffice(View):
    template_name = 'invoices/backoffice.html'
    template_header = 'Invoice | Backoffice'
    template_title = 'Backoffice'
    defaultResponse = {
        "ok": 1,
        "result": 'Process has been processed successfuly.',
        "status": 200,
    }

    def dispatch(self, *args, **kwargs):
        return super(ViewBackOffice, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'invoice.display_backoffice', login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        wresponse = self.defaultResponse.copy()
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    @method_decorator(permission_required(
        'invoice.display_backoffice', raise_exception=True,),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "get_list_items":
                    return self.getListItems(request)
                if wtype == "get_item_values":
                    return self.getItemValue(request)
                if wtype == "update_order":
                    return self.updateOrder(request)
                if wtype == "get_xls_report":
                    return self.getBackOfficeReport(request)
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def getListItems(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            wto = 10
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            select_fields = [
                "id", "order", "fhregistro_format",
                "fhmodificacion_format", "tipo_invoice",
                "cliente", "total", "forma_pago", "fecha_entrega",
                "username", "username_modif",
                "sub_tipo_invoice",
                "tipo_seguimiento",
                "vendedor", "vendedor_nombre",
                "tipo_descrip", "forma_pago_descrip",
                "cliente_phone", "fecha_emision",
                "cliente_nombre", "tipo_seguimiento_descrip",
                "sub_tipo_invoice_descrip",
                "fecha_entrega_reprogramada",
            ]
            wdata.update({
                "select_fields": select_fields,
            })
            outcome, err = utl.getListBackOffice(**wdata)
            if err:
                raise ValueError(err)

            srlzdata = srlz.InvoiceParentSerializer(
                outcome[wfrom:wto], fields=select_fields, many=True,
            ).data

            page["total"] = outcome.count()
            wresponse['result'] = {
                "rows": srlzdata,
                "page": page,
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('invoice.change_backoffice', raise_exception=True),)
    def getItemValue(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            winvoice = wdata.get('id') or ''
            if winvoice == '':
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            Qfilters = Q(id=int(winvoice))

            qsdetails = mdls.InvoiceDetails.objects.all() \
                .select_related("article__categoria__estado")
            qstipodoc = mdlcat.TipoDocumento.objects.all() \
                .select_related("estado")
            qscliente = mdlcat.Clientes.objects.all() \
                .select_related("estado") \
                .prefetch_related(Prefetch(
                    "tdoc", queryset=qstipodoc,
                ))

            wqueryset = mdls.InvoiceParent.objects.filter(Qfilters) \
                .select_related("estado") \
                .select_related("sub_tipo_invoice") \
                .prefetch_related(Prefetch("cliente", queryset=qscliente)) \
                .prefetch_related(Prefetch(
                    "invoicedetails_set", queryset=qsdetails,
                ))

            fields = [
                "id", "cliente", "fhregistro", "fhmodificacion",
                "order", "cliente_phone", "cliente_phone2",
                "cliente_address", "cliente_country", "total",
                "observaciones", "fecha_venta", "fecha_entrega",
                "tipo_invoice", "forma_pago", "estado",
                "nro_tarjeta", "invoicedetails_set",
                "observaciones2", "contesta_celular", "labora",
                "hora_entrega", "delivery_address",
                "sub_tipo_invoice", "tipo_seguimiento",
                "vendedor",
                "fecha_recepcion", "fecha_etd_courier",
                "tarjeta_cvv", "tarjeta_vigencia",
                "fecha_entrega_reprogramada",
                "hora_entrega_reprogramada",
                "hora_entrega_hasta",
                "motivo_reprogramacion",
            ]
            if not wqueryset.exists:
                wresponse['status'] = 400
                raise ValueError(str('Error, invalid Order ID specified.'))
            woutcome = srlz.InvoiceParentSerializer(
                wqueryset.get(), fields=fields,
            ).data
            for item in woutcome.get("invoicedetails_set"):
                item["check"], item["visible"] = False, True
            wresponse['result'] = woutcome
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('invoice.change_backoffice', raise_exception=True),)
    def updateOrder(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wid = wdata.get("id") or ''
            if wid == '':
                raise ValueError('Error, id es valor requerido.')

            objorder = mdls.InvoiceParent.objects.filter(id=int(wid))
            if not objorder.exists():
                wresponse['status'] = 400
                raise ValueError("Error, Invalid Order ID specified.")

            # parent layer validation
            f = frm.MFBackOffice(wdata)
            if not f.is_valid():
                werror = f.get_error_first()
                werr_msg = 'Error, {} : {}.'.format(
                    werror.get("field"), werror.get("message"),
                )
                wresponse['status'] = 400
                raise ValueError(werr_msg)
            wparameters = f.clean()

            wparameters["fhmodificacion"] = datetime.now()
            wparameters["username_modif"] = request.user
            objorder.update(**wparameters)
            wresponse['result'] = {
                "id": objorder.get().id,
                "message": "Datos fueron procesados satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('invoice.xlsreport_backoffice', raise_exception=True),)
    def getBackOfficeReport(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            select_fields = [
                "id", "order", "fhregistro_format",
                "fhmodificacion_format", "tipo_invoice",
                "cliente", "total", "forma_pago", "fecha_entrega",
                "username", "username_modif",
                "sub_tipo_invoice",
                "tipo_seguimiento",
                "vendedor", "vendedor_nombre",
                "tipo_descrip", "forma_pago_descrip",
                "cliente_phone", "fecha_emision",
                "cliente_nombre", "tipo_seguimiento_descrip",
                "sub_tipo_invoice_descrip",
                "fecha_entrega_reprogramada",
                "pais_descrip",
                "ciudad_descrip",
            ]
            wdata.update({
                "select_fields": select_fields,
            })
            outcome, err = utl.getListBackOffice(**wdata)
            if err:
                raise ValueError(err)

            srlzdata = srlz.InvoiceParentSerializer(
                outcome, fields=select_fields, many=True,
            ).data
            wreport, werror = utl.get_xls_backoffice(request, srlzdata)
            if werror:
                raise ValueError(str(werror))
            wresponse['result'] = wreport
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))
