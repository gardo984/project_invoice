
# To write and create xls files from zero
import xlsxwriter as xls
from datetime import datetime
# django settings info
from django.conf import settings

# To generate and modify a new xls file
from openpyxl import load_workbook
from openpyxl.utils import get_column_letter

from openpyxl.styles import (
    PatternFill, Border, Side,
    Alignment, Protection, Font,
    NamedStyle, Color, Fill,
)
from shutil import copyfile
from apps.common.utils import (
    Validators, ExportToExcel,
)
import os
from django.db.models import (
    Q, F, IntegerField, CharField, Count,
    IntegerField, Prefetch, Case, When,
    Subquery, ExpressionWrapper, OuterRef,
)
from django.db.models.functions import (
    Concat, Cast,
)
from apps.catalogos import (
    models as mdlcat,
)
from apps.invoice import (
    models as mdls,
    serializers as srlzped,
)


def getInvoicesList(**kwargs):
    outcome, error = [], None
    try:
        DATE_FORMAT = "%Y-%m-%d"
        wdata = kwargs

        wcliente = wdata.get('cliente') or ''
        winvoice = wdata.get('invoice') or ''
        wleyenda = wdata.get('sub_tipo_invoice') or ''

        date_from = datetime.strptime(
            wdata.get("fecha_desde"), DATE_FORMAT)
        date_to = datetime.strptime(wdata.get("fecha_hasta"), DATE_FORMAT)
        Qfilters = Q(
            id__isnull=False,
            fregistro__range=[date_from, date_to],
        )

        objuser = wdata.get("user")
        if hasattr(objuser, 'userextension'):
            if objuser.userextension.all_orders != 1:
                Qfilters.add(Q(username=objuser,), Qfilters.connector)
        else:
            Qfilters.add(Q(username=objuser,), Qfilters.connector)

        if wcliente != '':
            sentence = (
                Q(cliente__razon_social__icontains=wcliente) |
                Q(cliente_phone__icontains=wcliente) |
                Q(cliente_phone2__icontains=wcliente)
            )
            Qfilters.add(
                sentence, Qfilters.connector,
            )
        if winvoice != '':
            Qfilters.add(
                Q(order=winvoice),
                Qfilters.connector,
            )
        # added 2019.05.13
        if wleyenda != '':
            Qfilters.add(
                Q(sub_tipo_invoice__id=wleyenda), Qfilters.connector,
            )

        qsdetails = mdls.InvoiceDetails.objects.all() \
            .select_related("article__categoria__estado")
        qscliente = mdlcat.Clientes.objects.all() \
            .select_related("estado") \
            .select_related("tdoc__estado")

        wqueryset = mdls.InvoiceParent.objects \
            .filter(Qfilters) \
            .prefetch_related(Prefetch(
                "cliente", queryset=qscliente,
            )) \
            .select_related("estado") \
            .select_related("tipo_invoice__estado") \
            .select_related("sub_tipo_invoice__estado") \
            .select_related("forma_pago__estado") \
            .select_related("username") \
            .select_related("username_modif") \
            .select_related("vendedor") \
            .prefetch_related(Prefetch(
                "invoicedetails_set", queryset=qsdetails)
            ) \
            .annotate(ndetails=Count('invoicedetails')) \
            .order_by('-fhregistro')

        select_fields = kwargs.get("select_fields")

        # srlzdata = srlzped.InvoiceParentSerializer(
        #     wqueryset, fields=wfields, many=True
        # ).data
        outcome = wqueryset
    except Exception as e:
        error = str(e)
    return [outcome, error]


def getXlsInvoiceList(**kwargs):
    outcome, error = [], None
    try:
        DATE_FORMAT = "%Y-%m-%d"
        wdata = kwargs

        wcliente = wdata.get('cliente') or ''
        winvoice = wdata.get('invoice') or ''
        wleyenda = wdata.get('sub_tipo_invoice') or ''

        date_from = datetime.strptime(
            wdata.get("fecha_desde"), DATE_FORMAT)
        date_to = datetime.strptime(wdata.get("fecha_hasta"), DATE_FORMAT)
        Qfilters = Q(
            id__isnull=False,
            fregistro__range=[date_from, date_to],
        )

        objuser = wdata.get("user")
        if hasattr(objuser, 'userextension'):
            if objuser.userextension.all_orders != 1:
                Qfilters.add(Q(username=objuser,), Qfilters.connector)
        else:
            Qfilters.add(Q(username=objuser,), Qfilters.connector)

        if wcliente != '':
            sentence = (
                Q(cliente__razon_social__icontains=wcliente) |
                Q(cliente_phone__icontains=wcliente) |
                Q(cliente_phone2__icontains=wcliente)
            )
            Qfilters.add(
                sentence, Qfilters.connector,
            )
        if winvoice != '':
            Qfilters.add(
                Q(order=winvoice),
                Qfilters.connector,
            )
        # added 2019.05.13
        if wleyenda != '':
            Qfilters.add(
                Q(sub_tipo_invoice__id=wleyenda), Qfilters.connector,
            )

        select_fields = kwargs.get("select_fields")
        qsdetails = mdls.InvoiceDetails.objects.all() \
            .select_related("article__categoria__estado")
        qscliente = mdlcat.Clientes.objects.all() \
            .select_related("pais__estado") \
            .select_related("tdoc__estado") \
            .select_related("estado")
        qs = mdls.InvoiceParent.objects.filter(Qfilters) \
            .prefetch_related(Prefetch("cliente", queryset=qscliente)) \
            .prefetch_related(Prefetch(
                "invoicedetails_set", queryset=qsdetails,
            )) \
            .select_related("forma_pago__estado") \
            .select_related("tipo_invoice__estado") \
            .select_related("estado") \
            .select_related("username") \
            .select_related("username_modif") \
            .select_related("vendedor") \
            .annotate(ndetails=Count('invoicedetails')) \
            .order_by("-fhregistro")
        outcome = qs
    except Exception as e:
        error = str(e)
    return [outcome, error]


def getListBackOffice(**kwargs):
    outcome, error = [], None
    try:
        DATE_FORMAT = "%Y-%m-%d"
        date_from = datetime.strptime(
            kwargs.get("date_from"), DATE_FORMAT)
        date_to = datetime.strptime(
            kwargs.get("date_to"), DATE_FORMAT)

        qrange = Q()
        qfilter = Q(estado_id=1,)
        if kwargs.get("date_filter"):
            # qrange.add(
            #     Q(fecha_entrega__range=[date_from, date_to]) |
            #     Q(fecha_entrega_reprogramada__range=[date_from, date_to]),
            #     qrange.connector
            # )
            qrange.add(
                Q(fentrega__range=[date_from, date_to]),
                qrange.connector
            )
        else:
            qrange.add(Q(fregistro__range=[date_from, date_to]),
                       qrange.connector)

        if kwargs.get("customer"):
            sentence = (
                Q(cliente__razon_social__icontains=kwargs.get("customer")) |
                Q(cliente_phone__icontains=kwargs.get("customer")) |
                Q(cliente_phone2__icontains=kwargs.get("customer"))
            )
            qfilter.add(sentence, qfilter.connector)

        if kwargs.get("order"):
            qfilter.add(
                Q(order__iexact=kwargs.get("order")), qfilter.connector)

        # added at 2019.05.13
        if kwargs.get("sub_tipo_invoice"):
            qfilter.add(
                Q(sub_tipo_invoice__id=kwargs.get("sub_tipo_invoice")),
                qfilter.connector)

        select_fields = kwargs.get("select_fields")

        qscliente = mdlcat.Clientes.objects.all() \
            .select_related("pais__estado") \
            .select_related("tdoc__estado") \
            .select_related("estado")
        qs = mdls.InvoiceParent.objects.filter(qfilter) \
            .prefetch_related(Prefetch("cliente", queryset=qscliente)) \
            .select_related("forma_pago__estado") \
            .select_related("tipo_invoice__estado") \
            .select_related("sub_tipo_invoice__estado") \
            .select_related("tipo_seguimiento__estado") \
            .select_related("estado") \
            .select_related("username") \
            .select_related("username_modif") \
            .select_related("vendedor") \
            .annotate(
                fentrega=Case(
                    When(fecha_entrega_reprogramada__isnull=False,
                         then=F('fecha_entrega_reprogramada')),
                    default=F('fecha_entrega')),) \
            .filter(qrange) \
            .order_by("-fhregistro")

        outcome = qs
    except Exception as e:
        error = str(e)
    return [outcome, error]


def getNewCorrelative():
    qsorder = mdls.InvoiceParent.objects.all() \
        .annotate(last_order=Cast('order', IntegerField())) \
        .values("last_order") \
        .order_by("-last_order")[:1]
    item = 0
    if qsorder.count() > 0:
        item = qsorder.first().get("last_order")
    item = int(item + 1)
    return "{:06}".format(item)


def getStyles():
    return {
        "fullBorder": Border(
            left=Side(border_style='thin', color='00000000'),
            right=Side(border_style='thin', color='00000000'),
            top=Side(border_style='thin', color='00000000'),
            bottom=Side(border_style='thin', color='00000000')
        ),
    }


def getStyleFooterLabel():
    return NamedStyle(
        name="footer_style_label",
        font=Font(size=10, bold=True),
        border=getStyles().get("fullBorder"),
        fill=PatternFill(
            fill_type='solid',
            start_color='ffff66',
        ),
    )


def getStyleFooterValue():
    return NamedStyle(
        name="footer_style_value",
        font=Font(size=10, bold=True),
        border=getStyles().get("fullBorder"),
        fill=PatternFill(
            fill_type='solid',
            start_color='ffffff',
        ),
        alignment=Alignment(horizontal="right"),
    )


def getStyleDetailHeader():
    return NamedStyle(
        name="detail_style_header",
        font=Font(size=10, bold=True,),
        fill=PatternFill(
            fill_type='solid',
            start_color='ffff66',
        ),
        alignment=Alignment(horizontal="center", vertical="center",),
        border=Border(
            top=Side(border_style='thin', color='00000000'),
            bottom=Side(border_style='thin', color='00000000'),
        ),
    )


def getStyleDetail():
    return {
        "text": NamedStyle(
            name="detail_style_text",
            alignment=Alignment(horizontal="left",),
            font=Font(size=10),
            fill=PatternFill(
                fill_type='solid',
                start_color='ffffff',
            )),
        "number": NamedStyle(
            name="detail_style_number",
            alignment=Alignment(horizontal="right",),
            font=Font(size=10),
            fill=PatternFill(
                fill_type='solid',
                start_color='ffffff',
            )),
        "index": NamedStyle(
            name="detail_style_index",
            alignment=Alignment(horizontal="center",),
            font=Font(size=10),
            fill=PatternFill(
                fill_type='solid',
                start_color='ffffff',
            )),
    }


def validateInvoice(**kwargs):
    status, outcome = False, None
    try:
        Qfilters = Q(
            order__iexact=kwargs.get("order"),
            # tipo_doc=kwargs.get("tipo_doc"),
        )
        qs = mdls.InvoiceParent.objects.filter(Qfilters)
        if qs.count() > 0:
            raise ValueError(
                'Error, nro. documento ya se encuentra registrado')
        status = True
    except Exception as e:
        outcome = str(e)
    return [status, outcome]


def get_xls_backoffice(request, data):
    result, error = None, None
    datetime_format = "%d/%m/%Y %H:%M:%S"
    tz_format = "%Y-%m-%d %H:%M:%S"
    date_format = "%d/%m/%Y"
    time_format = "%H:%M:%S"
    tzdate_format = "%Y-%m-%d"
    try:
        wfields = [
            {"value": "Nro. Orden", "field": "order", "bold": True, "w": 15, },
            {"value": "Tipo", "field": "tipo_descrip", "bold": True, "w": 15, },
            {"value": "Fecha Registro", "field": "fhregistro_format", "bold": True,
             "format_in": tz_format, "format_out": date_format, "w": 20, },
            {"value": "Hora Registro", "field": "fhregistro_format", "bold": True,
             "format_in": tz_format, "format_out": time_format, "w": 20, },
            {"value": "Fecha Emision", "field": "fecha_emision", "bold": True,
             "format_in": tzdate_format, "format_out": date_format, "w": 20, },
            {"value": "Cliente", "field": "cliente_nombre", "bold": True, "w": 40, },
            {"value": "Nro. Telefono", "field": "cliente_phone",
                "bold": True, "w": 20, },
            {"value": "Ciudad", "field": "ciudad_descrip", "bold": True, "w": 20, },
            {"value": "Pais", "field": "pais_descrip", "bold": True, "w": 20, },
            {"value": "Leyenda",
                "field": "sub_tipo_invoice_descrip", "bold": True, "w": 30, },
            {"value": "Tipo Seguimiento",
                "field": "tipo_seguimiento_descrip", "bold": True, "w": 30, },
            {"value": "Total", "field": "total", "bold": True, },
            {"value": "Forma Pago", "field": "forma_pago_descrip",
                "bold": True, "w": 15, },
            {"value": "Fch.Entrega", "field": "fecha_entrega", "bold": True,
             "format_in": tzdate_format, "format_out": date_format, "w": 20, },
            {"value": "Fch.Reprogramada", "field": "fecha_entrega_reprogramada", "bold": True,
             "format_in": tzdate_format, "format_out": date_format, "w": 20, },
            {"value": "Vendedor", "field": "vendedor_nombre", "bold": True, "w": 30, },
            {"value": "Usuario", "field": "username", "bold": True, },
            {"value": "Fch.Modif.", "field": "fhmodificacion_format", "bold": True,
             "format_in": tz_format, "format_out": datetime_format, "w": 20, },
            {"value": "Usuario Modif.", "field": "username_modif", "bold": True, },
        ]
        wparameters = {
            "start_from": 4,
            "request": request,
            "defaultWidth": 12,
            "title": "Reporte General de BackOffice",
            "headers": wfields,
            "details": data,
        }
        objxls = ExportToExcel(**wparameters)
        outcome, error = objxls.generateFile()
        result = {
            "message": "Reporte fue generado satisfactoriamente.",
            "urlpath": outcome.get("filepath"),
        }
    except Exception as e:
        error = str(e)
    return [result, error]


def get_xls_invoice(request, data):
    result, error = None, None
    hash_format = "%Y%m%d%H%M%S%f"
    try:
        styles = getStyles()
        wfilename = datetime.now().strftime(hash_format)
        wtemplate = '{}others/template_invoice.xlsx'.format(
            settings.INTERNAL_PATH.get("static"),
        )
        wxlsfile = '{}invoice_report/xls_{}.xlsx'.format(
            settings.INTERNAL_PATH.get("media"), wfilename,
        )
        werror = Validators.ifDirectoryExists(os.path.dirname(wxlsfile))
        if werror:
            raise ValueError(str(werror))
        copyfile(wtemplate, wxlsfile)
        wb = load_workbook(wxlsfile)
        ws = wb.active

        # print values about parent
        wcells = [
            "E5", "E6", "E7", "E8", "E9", "K5", "K6", "K7",
        ]
        wvalues = [
            data.get("id"), data.get("cliente").get("razon_social"),
            data.get("cliente_address"), data.get("cliente_country"),
            data.get("observaciones"), data.get("fecha_emision"),
            data.get("cliente_ndoc"), data.get("cliente").get("telefono"),
        ]

        for windex, wcell in enumerate(wcells):
            ws[wcell].value = wvalues[windex]
        # print values about details
        stylDetail = getStyleDetail()
        styleDetHead = getStyleDetailHeader()
        wrow = 13
        det = {
            "h": ["C", "I", "K", "L", "E"],
            "v": [
                {"field": "item", "style": stylDetail.get("index"), },
                {"field": "cantidad", "style": stylDetail.get("number"), },
                {"field": "subtotal", "style": stylDetail.get("number"), },
                {"field": "total", "style": stylDetail.get("number"), },
                {"field": "article_description",
                    "style": stylDetail.get("text"), },
            ],
        }
        for wrange in ws["B12":"L12"]:
            for c in wrange:
                c.style = styleDetHead
        # print details
        for x in data.get("invoicedetails_set"):
            for n, y in enumerate(det.get("h")):
                wcell = ws['{}{}'.format(y, wrow)]
                wcell.value = x.get(det.get("v")[n].get("field"))
                wcell.style = det.get("v")[n].get("style")
            wrow += 1
        # print total values
        wrow += 3
        footer = [
            {"label": "Total Sale", "value": data.get("total"), },
            {"label": "Shipping", "value": 0, },
            {"label": "Total Order", "value": data.get("total"), },
        ]
        stylFooValue = getStyleFooterValue()
        stylFooLabel = getStyleFooterLabel()
        for x in footer:
            ws['K{}'.format(wrow)].value = x.get("label")
            ws['L{}'.format(wrow)].value = x.get("value")
            ws['L{}'.format(wrow)].style = stylFooValue
            ws['K{}'.format(wrow)].style = stylFooLabel
            wrow += 1

        wb.save(wxlsfile)
        wb.close()

        urlpath = request.build_absolute_uri('{}invoice_report/{}'.format(
            settings.MEDIA_URL, os.path.basename(wxlsfile),
        ))

        result = {
            "message": "Reporte fue generado satisfactoriamente.",
            "urlpath": urlpath,
        }
    except Exception as e:
        error = str(e)
    return [result, error]


def get_xls_report(request, data):
    result, error = None, None
    datetime_format = "%d/%m/%Y %H:%M:%S"
    tz_format = "%Y-%m-%d %H:%M:%S"
    date_format = "%d/%m/%Y"
    time_format = "%H:%M:%S"
    tzdate_format = "%Y-%m-%d"
    try:
        wfields = [
            {"value": "Nro. Orden", "field": "order", "bold": True, "w": 15, },
            {"value": "Tipo", "field": "tipo_descrip", "bold": True, "w": 15, },
            {"value": "Fecha Registro", "field": "fhregistro_format", "bold": True,
             "format_in": tz_format, "format_out": date_format, "w": 20, },
            {"value": "Hora Registro", "field": "fhregistro_format", "bold": True,
             "format_in": tz_format, "format_out": time_format, "w": 20, },
            {"value": "Fecha Emision", "field": "fecha_emision", "bold": True,
             "format_in": tzdate_format, "format_out": date_format, "w": 20, },
            {"value": "Cliente", "field": "cliente_nombre", "bold": True, "w": 40, },
            {"value": "Nro. Telefono", "field": "cliente_phone",
                "bold": True, "w": 20, },
            {"value": "Ciudad", "field": "ciudad_descrip", "bold": True, "w": 20, },
            {"value": "Pais", "field": "pais_descrip", "bold": True, "w": 20, },
            {"value": "Nro. Items", "field": "ndetails", "bold": True, },
            {"value": "Total", "field": "total", "bold": True, },
            {"value": "Forma Pago", "field": "forma_pago_descrip",
                "bold": True, "w": 15, },
            {"value": "Fch.Entrega", "field": "fecha_entrega", "bold": True,
             "format_in": tzdate_format, "format_out": date_format, "w": 20, },
            {"value": "Fch.Reprogramada", "field": "fecha_entrega_reprogramada", "bold": True,
             "format_in": tzdate_format, "format_out": date_format, "w": 20, },
            {"value": "Vendedor", "field": "vendedor_nombre", "bold": True, "w": 30, },
            {"value": "Usuario", "field": "username", "bold": True, },
            {"value": "Fch.Modif.", "field": "fhmodificacion_format", "bold": True,
             "format_in": tz_format, "format_out": datetime_format, "w": 20, },
            {"value": "Usuario Modif.", "field": "username_modif", "bold": True, },
        ]
        wparameters = {
            "start_from": 4,
            "request": request,
            "defaultWidth": 12,
            "title": "Reporte General de Invoices",
            "headers": wfields,
            "details": data,
        }
        objxls = ExportToExcel(**wparameters)
        outcome, error = objxls.generateFile()
        result = {
            "message": "Reporte fue generado satisfactoriamente.",
            "urlpath": outcome.get("filepath"),
        }
    except Exception as e:
        error = str(e)
    return [result, error]
