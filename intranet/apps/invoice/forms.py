
from apps.invoice import models as mdls
from django import forms
from django.contrib.auth import models as mdlauth
from django.db.models import Q, F
import json

# Model Forms

# Forms


class MFInvoiceDetails(forms.ModelForm):

    DEFAULT_MSG_REQUIRED = 'Este campo es requerido'
    DEFAULT_MSG_CHOICE = 'Ingrese una opcion valida, valor seleccionado no paso validacion'
    NO_REQUIRED = [
        'igv', 'invoice',
    ]

    class Meta:
        model = mdls.InvoiceDetails
        fields = '__all__'
        exclude = [
            'fregistro',
            'fhregistro', 'fhmodificacion',
            'username', 'username_modif',
        ]

    def __init__(self, *args, **kwargs):
        super(MFInvoiceDetails, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

        for wfield in self.fields.keys():
            lst_errors = self.fields[wfield].error_messages
            if lst_errors.get("required"):
                lst_errors["required"] = self.DEFAULT_MSG_REQUIRED
            if lst_errors.get("invalid_choice"):
                lst_errors["invalid_choice"] = self.DEFAULT_MSG_CHOICE

    def clean(self):
        cleaned_data = super().clean()
        for item in self.fields.keys():
            if isinstance(cleaned_data.get(item), str):
                cleaned_data[item] = cleaned_data.get(item).upper()
        return cleaned_data

    def get_error_first(self):
        wmessage = ''
        if self.errors:
            wresult = {}
            wlist_json = json.loads(self.errors.as_json())
            wkey = list(wlist_json.keys())[0]
            wvalues = wlist_json.get(wkey)[0].get("message")
            wresult["field"] = wkey.lower()
            wresult["message"] = wvalues.lower()
            wmessage = 'Error, {} : {}.'.format(
                wresult.get("field"), wresult.get("message"),
            )
        return wmessage


class MFInvoiceParent(forms.ModelForm):

    DEFAULT_MSG_REQUIRED = 'Este campo es requerido'
    DEFAULT_MSG_CHOICE = 'Ingrese una opcion valida, valor seleccionado no paso validacion'
    NO_REQUIRED = [
        'igv', 'cliente_country',
        'fecha_vigencia', 'fecha_emision',
        'fecha_entrega', 'nro_tarjeta',
        'hora_entrega', 'contesta_celular',
        'observaciones2', 'labora',
        'vendedor',
        'tarjeta_cvv', 'tarjeta_vigencia',
        'fecha_etd_courier', 'fecha_recepcion',
        'hora_entrega_hasta',
    ]

    class Meta:
        model = mdls.InvoiceParent
        fields = '__all__'
        exclude = [
            'fregistro',
            'fhregistro', 'fhmodificacion',
            'username', 'username_modif',
            # 'tipo_doc',
            'estado', 'sub_tipo_invoice',
            'tipo_seguimiento',
            'fecha_entrega_reprogramada',
            'hora_entrega_reprogramada',
            'hora_reprogramada_hasta',
            'motivo_reprogramacion',
        ]

    def __init__(self, *args, **kwargs):
        super(MFInvoiceParent, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

        for wfield in self.fields.keys():
            lst_errors = self.fields[wfield].error_messages
            if lst_errors.get("required"):
                lst_errors["required"] = self.DEFAULT_MSG_REQUIRED
            if lst_errors.get("invalid_choice"):
                lst_errors["invalid_choice"] = self.DEFAULT_MSG_CHOICE

    def clean(self):
        cleaned_data = super().clean()
        for item in self.fields.keys():
            if isinstance(cleaned_data.get(item), str):
                cleaned_data[item] = cleaned_data.get(item).upper()
        return cleaned_data

    def get_error_first(self):
        wresult = {}
        if self.errors:
            wlist_json = json.loads(self.errors.as_json())
            wkey = list(wlist_json.keys())[0]
            wvalues = wlist_json.get(wkey)[0].get("message")
            wresult["field"] = wkey.lower()
            wresult["message"] = wvalues.lower()
        return wresult


class MFBackOffice(forms.ModelForm):

    DEFAULT_MSG_REQUIRED = 'Este campo es requerido'
    DEFAULT_MSG_CHOICE = 'Ingrese una opcion valida, valor seleccionado no paso validacion'
    NO_REQUIRED = [
        "sub_tipo_invoice", "tipo_seguimiento",
        "fecha_entrega_reprogramada",
        "hora_entrega_reprogramada",
        "hora_reprogramada_hasta",
        "motivo_reprogramacion",
    ]

    class Meta:
        model = mdls.InvoiceParent
        fields = [
            "id", "order", "delivery_address",
            "sub_tipo_invoice", "tipo_seguimiento",
            # "fecha_entrega", "hora_entrega",
            "fecha_entrega_reprogramada",
            "hora_entrega_reprogramada",
            "hora_reprogramada_hasta",
            "motivo_reprogramacion",
        ]

    def __init__(self, *args, **kwargs):
        super(MFBackOffice, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

        for wfield in self.fields.keys():
            lst_errors = self.fields[wfield].error_messages
            if lst_errors.get("required"):
                lst_errors["required"] = self.DEFAULT_MSG_REQUIRED
            if lst_errors.get("invalid_choice"):
                lst_errors["invalid_choice"] = self.DEFAULT_MSG_CHOICE

    def clean(self):
        cleaned_data = super().clean()
        for item in self.fields.keys():
            if isinstance(cleaned_data.get(item), str):
                cleaned_data[item] = cleaned_data.get(item).upper()
        return cleaned_data

    def get_error_first(self):
        wresult = {}
        if self.errors:
            wlist_json = json.loads(self.errors.as_json())
            wkey = list(wlist_json.keys())[0]
            wvalues = wlist_json.get(wkey)[0].get("message")
            wresult["field"] = wkey.lower()
            wresult["message"] = wvalues.lower()
        return wresult
