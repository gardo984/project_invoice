from django.db import models
from decimal import Decimal
from django.core.validators import MaxValueValidator, MinValueValidator
from apps.common.models import CommonStructure
from datetime import datetime
# Create your models here.

DEFAULT_STATUS = 1


class InvoiceEstados(CommonStructure):
    descripcion = models.CharField(max_length=200, null=False, blank=False,)
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)
    username = None
    username_modif = None

    class Meta:
        verbose_name = "Invoice Estados"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
        ]

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    @property
    def username_modif(self):
        raise AttributeError(
            "'Manager' object has no attribute 'username_modif'")


class InvoiceParent(CommonStructure):
    forma_pago = models.ForeignKey('catalogos.FormaPago',
                                   on_delete=models.SET_NULL,
                                   null=True, blank=False,)
    order = models.CharField(
        max_length=25, null=False, blank=False,)
    cliente = models.ForeignKey(
        'catalogos.Clientes',
        on_delete=models.SET_NULL,
        null=True, default=None)
    cliente_ndoc = models.CharField(
        max_length=25, null=True, blank=True, default=None,)

    cliente_phone = models.CharField(
        max_length=50, null=True, blank=True, default=None,)
    cliente_phone2 = models.CharField(
        max_length=25, null=True, blank=True, default=None,)

    cliente_address = models.CharField(
        max_length=250, null=False, blank=False,)
    cliente_country = models.CharField(
        max_length=100, null=False, blank=False,)
    subtotal = models.DecimalField(max_digits=12,
                                   decimal_places=2,
                                   null=True,
                                   default=Decimal('0.00'),)
    igv = models.DecimalField(max_digits=12,
                              decimal_places=2,
                              null=True,
                              default=Decimal('0.00'),)
    total = models.DecimalField(max_digits=12,
                                decimal_places=2,
                                null=True,
                                default=Decimal('0.00'),)
    observaciones = models.TextField(max_length=200, null=True, blank=True,)
    fecha_emision = models.DateField(null=False, blank=False,
                                     default=datetime.now)
    fecha_venta = models.DateField(null=True, blank=False,
                                   default=None,)
    fecha_vigencia = models.DateField(null=True, blank=False,
                                      default=None)
    fecha_entrega = models.DateField(null=True, blank=False,
                                     default=None)
    estado = models.ForeignKey('catalogos.Estados',
                               on_delete=models.SET_NULL,
                               null=True,
                               default=DEFAULT_STATUS,)
    tipo_invoice = models.ForeignKey('catalogos.TipoInvoice',
                                     on_delete=models.SET_NULL,
                                     null=True, blank=False,)
    sub_tipo_invoice = models.ForeignKey('invoice.InvoiceEstados',
                                         on_delete=models.SET_NULL,
                                         null=True, blank=False,)
    # added at 2019.09.02
    tipo_seguimiento = models.ForeignKey(
        'catalogos.Combos',
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
        related_name="%(app_label)s_%(class)s_tipo_seguimiento_set",
    )

    nro_tarjeta = models.CharField(
        max_length=25, null=True, blank=True, default=None,)
    labora = models.CharField(
        max_length=50, null=True, blank=True, default=None,)
    contesta_celular = models.CharField(
        max_length=50, null=True, blank=True, default=None,)
    observaciones2 = models.TextField(max_length=200,
                                      default=None,
                                      null=True, blank=True,)
    hora_entrega = models.TimeField(
        default=None, auto_now=False, auto_now_add=False, null=True, blank=False,)
    delivery_address = models.CharField(max_length=250, null=True, blank=True,
                                        default=None,)
    # added at 2019.09.19
    vendedor = models.ForeignKey(
        'catalogos.Vendedores',
        null=True,
        on_delete=models.SET_NULL,
        default=None, blank=False)

    # added at 2019.09.20
    tarjeta_cvv = models.CharField(
        max_length=4, null=True, blank=True, default=None,)
    tarjeta_vigencia = models.CharField(
        max_length=7, null=True, blank=True, default=None,)
    fecha_recepcion = models.DateField(null=True, blank=False,
                                       default=None,)
    fecha_etd_courier = models.DateField(null=True, blank=False,
                                         default=None,)

    # added at 2019.10.02
    fecha_entrega_reprogramada = models.DateField(null=True, blank=False,
                                                  default=None)
    hora_entrega_reprogramada = models.TimeField(
        default=None, auto_now=False, auto_now_add=False, null=True, blank=False,)

    # added at 2019.10.05
    hora_entrega_hasta = models.TimeField(
        default=None, auto_now=False, auto_now_add=False, null=True, blank=False,)
    hora_reprogramada_hasta = models.TimeField(
        default=None, auto_now=False, auto_now_add=False, null=True, blank=False,)

    # added at 2019.10.13
    motivo_reprogramacion = models.CharField(max_length=150, null=True, blank=True,
                                             default=None,)

    class Meta:
        verbose_name = "Invoice Parent"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        permissions = (
            ('xlsreport_invoiceparent', 'Exportar Reporte Invoice Parent'),
            ('xlsinvoice_invoiceparent', 'Exportar Formato Invoice Parent'),
            ('display_invoiceparent', 'Visualizar Invoice Parent'),
            ('display_backoffice', 'Visualizar BackOffice'),
            ('change_backoffice', 'Modificar BackOffice'),
            ('xlsreport_backoffice', 'Exportar Reporte BackOffice'),
        )
        indexes = [
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['cliente_phone'],),
            models.Index(fields=['fecha_venta'],),
            models.Index(fields=['fecha_emision'],),
            models.Index(fields=['fecha_entrega'],),
            models.Index(fields=['nro_tarjeta'],),
            models.Index(fields=['hora_entrega'],),
            models.Index(fields=['fecha_recepcion'],),
            models.Index(fields=['fecha_etd_courier'],),
        ]

    def __str__(self):
        return str(self.id)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class InvoiceDetails(CommonStructure):
    item = models.PositiveIntegerField(
        null=False, blank=False,
        default=None,
        validators=[MaxValueValidator(
            100), MinValueValidator(1)]
    )
    invoice = models.ForeignKey(
        'invoice.InvoiceParent', on_delete=models.CASCADE, null=True, default=None)
    article = models.ForeignKey(
        'productos.Articulos', on_delete=models.SET_NULL, null=True, default=None)
    subtotal = models.DecimalField(max_digits=12,
                                   decimal_places=2,
                                   null=True,
                                   default=Decimal('0.00'),)
    igv = models.DecimalField(max_digits=12,
                              decimal_places=2,
                              null=True,
                              default=Decimal('0.00'),)
    total = models.DecimalField(max_digits=12,
                                decimal_places=2,
                                null=True,
                                default=Decimal('0.00'),)
    observaciones = models.TextField(max_length=200, null=True, blank=True,)
    cantidad = models.PositiveIntegerField(
        null=True, blank=False, default=1,
    )

    class Meta:
        verbose_name = "Invoice Details"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['item'],),
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]

    def __str__(self):
        return str(self.id)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)
