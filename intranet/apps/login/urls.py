from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from apps.login import views as v

app_name = 'module_login'

urlpatterns = [
    url(r'^$', v.LoginView.as_view(), name='main'),
    url(r'^refresh/$', v.RefreshView.as_view(), name='refresh'),
    url(r'^logout/$', v.LogoutView.as_view(), name='logout'),
    url(r'^validation/$', v.LoginValidation.as_view(), name='validation'),
    url(r'^change_password/$', v.LoginChangePasswd.as_view(),
        name='change_password'),
    url(r'^forbidden/$', v.LoginForbidden.as_view(),
        name='forbidden'),
    url(r'^additional_information/$', v.LoginAdditionalInfo.as_view(),
        name='additional_information'),
]
