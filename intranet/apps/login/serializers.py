#! /usr/bin/env python
# -*- coding:utf-8 -*-

from rest_framework import serializers
from django.contrib.auth import models as m
from apps.catalogos import models as mdlcat


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class GroupExtensionSerializer(DynamicFieldsModelSerializer):

    class Meta:
        model = mdlcat.GroupExtension
        fields = '__all__'


class UserExtensionSerializer(DynamicFieldsModelSerializer):

    class Meta:
        model = mdlcat.UserExtension
        fields = '__all__'


class UserSerializer(DynamicFieldsModelSerializer):

    fullname = serializers.SerializerMethodField()
    userextension = UserExtensionSerializer(
        read_only=True,
    )
    groups = None
    user_permissions = None

    class Meta:
        model = m.User
        fields = '__all__'

    def get_fullname(self, obj):
        wlast_name = obj.last_name or ''
        wfirst_name = obj.first_name or ''
        return '{} {}'.format(wfirst_name, wlast_name)


class GroupSerializer(DynamicFieldsModelSerializer):
    group_id = serializers.IntegerField()
    user_id = serializers.IntegerField()
    groupextension = GroupExtensionSerializer(
        read_only=True,
    )

    class Meta:
        model = m.Group
        fields = '__all__'


class PermissionSerializer(DynamicFieldsModelSerializer):

    user_id = serializers.IntegerField()

    class Meta:
        model = m.Permission
        fields = '__all__'
