from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth.models import (
    User,
)
from django.db.models import (
    Q, F,
)
from django.views import View
from rest_framework.response import Response
from django.http import JsonResponse
from django.conf import settings
from django.contrib.sessions import models as mdl

from time import sleep
import os
import json

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import (
    login_required,
)
from apps.login import (
    serializers as srlog,
    forms as frm,
    utils as utl,
)
from apps.common.utils import Validators
from datetime import datetime

# Create your views here.

DEFAULT_MAIN_PAGE = '/main/'
if settings.DEBUG == False:
    DEFAULT_MAIN_PAGE = os.path.join(settings.PREFIX_URL, 'main')


@method_decorator(login_required, name='dispatch')
class RefreshView(View):
    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(RefreshView, self).dispatch(*args, **kwargs)

    def post(self, request):
        try:
            DATE_FORMAT = "%m/%d/%Y %H:%M:%S"
            wresponse = self.defaultResponse.copy()
            request.session.set_expiry(settings.SESSION_COOKIE_AGE)
            wexpiration = {
                "date": request.session.get_expiry_date().strftime(
                    DATE_FORMAT),
            }
            wresponse["result"] = wexpiration

        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))


class LogoutView(View):

    def get(self, request):
        auth.logout(request)
        return redirect(DEFAULT_MAIN_PAGE)


class LoginForbidden(View):
    template_name = 'login/forbidden.html'
    template_header = 'WRC | 403 Acceso no autorizado'
    template_title = 'Acceso no autorizado'

    def get(self, request):
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())


class LoginView(View):

    template_name = 'login/login.html'
    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def get(self, request):
        warguments = {}
        if request.user.is_authenticated:
            return redirect(DEFAULT_MAIN_PAGE)
        return render(request, self.template_name, **warguments)


class LoginValidation(View):

    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wmeta = request.META.get("HTTP_REFERER")
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            status, error = self.userValidation(request)
            if error:
                raise ValueError(str(error))
            wparameters = {
                "username": wdata.get("username"),
                "password": wdata.get("passwd"),
            }
            user = auth.authenticate(**wparameters)
            if user is None:
                # 422 Unprocessable Entity
                wresponse["status"] = "422"
                raise ValueError(
                    str('Credenciales no pasaron autenticación, vuelva a intentarlo.'))
            if not user.is_active:
                wresponse["status"] = "422"
                raise ValueError(
                    str('Usuario no se encuentra habilitado, comunicarse con sistemas.'))

            # setting up session variables
            self.userSessionVariables(request, user)
            # create the session instance of login
            auth.login(request, user)
            next_page = DEFAULT_MAIN_PAGE
            if wmeta.rfind("/?next=") > -1:
                next_page = request.build_absolute_uri(wmeta.split("=")[1])

            wresponse["result"] = {
                "message": "Autenticación fue validada satisfactoriamente.",
                "next": next_page,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def userSessionVariables(self, request, user):
        try:
            from django.contrib.auth import models as m
            from django.db.models import Q, F, Count
            from apps.catalogos import (
                models as mdlcat, serializers as srl,
            )
            witems = m.Permission.objects.values("content_type_id") \
                .filter(Q(user=user) | Q(group__id__in=user.groups.all())) \
                .annotate(menusdetalle_id=F('menusdetalle'), nitems=Count('content_type_id')) \
                .order_by("content_type_id")
            wfilters = [item.get("menusdetalle_id") for item in witems]
            wmenus = mdlcat.MenusParent.objects.filter(
                detalles__id__in=wfilters, estado_id=1,) \
                .annotate(nitems=Count(F('id')))
            wsrlmenus = []
            for menu in wmenus:
                wsrl = srl.MenusParentSerializer(menu).data
                wsrl["detalles"] = srl.MenusDetalleSerializer(
                    menu.detalles.filter(id__in=wfilters, estado_id=1,),
                    many=True,
                ).data
                wsrlmenus.append(wsrl)

            request.session["menu"] = wsrlmenus
            request.session["username"] = user.username
            return
        except Exception as e:
            wmessage = str(e)
            print(wmessage)
            raise ValueError(wmessage)

    def userValidation(self, request):
        wstatus, werror = True, None
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not wdata.get("username"):
                wresponse['status'] = 400
                raise ValueError(
                    str('Error, Usuario de autenticación no ha sido ingresado.'))
            if not wdata.get("passwd"):
                wresponse['status'] = 400
                raise ValueError(
                    str('Error, Clave de acceso no ha sido ingresado.'))
        except Exception as e:
            wstatus = False
            werror = str(e)
        return [wstatus, werror]


@method_decorator(login_required, name='dispatch')
class LoginChangePasswd(View):

    template_name = 'login/change_password.html'
    template_header = 'Invoice | Cambio de Clave'
    template_title = 'Cambio de Clave'

    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(LoginChangePasswd, self).dispatch(*args, **kwargs)

    def get(self, request):
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if not request.user.is_authenticated:
                wresponse['status'] = 400
                raise ValueError(str('User no esta autenticado.'))
            status, error = self.changePasswordValidation(request)
            if error:
                raise ValueError(str(error))
            wexistence = User.objects.filter(
                username__exact=request.user.username).exists()
            if not wexistence:
                wresponse['status'] = 400
                raise ValueError(str('Usuario no existe.'))

            wuser = User.objects.get(username__exact=request.user.username)
            wuser.set_password(wdata.get("new_password"))
            wuser.save()
            wresponse[
                "result"] = "Cambio de clave fue procesada satisfactoriamente."
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def changePasswordValidation(self, request):
        wstatus, werror = True, None
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not wdata.get("password"):
                raise ValueError(
                    str('Error, Clave de autenticación no ha sido ingresado.'))
            if not wdata.get("new_password"):
                raise ValueError(
                    str('Error, Nueva clave de acceso no ha sido ingresado.'))
            if not wdata.get("new_password2"):
                raise ValueError(
                    str('Error, Confirmacion Nueva clave de acceso no ha sido ingresado.'))

            if wdata.get("new_password") != wdata.get("new_password2"):
                raise ValueError(
                    str('Error, nueva clave y confirmación son diferentes.'))

            wstatus = auth.authenticate(
                username=request.user.username, password=wdata.get("password")
            )
            if wstatus is None:
                raise ValueError(
                    str('Error, clave actual no paso validacion.'))
        except Exception as e:
            wstatus = False
            werror = str(e)
        return [wstatus, werror]


@method_decorator(login_required, name='dispatch')
class LoginAdditionalInfo(View):
    template_name = 'login/additional_information.html'
    template_header = 'Invoice | Información de Usuario'
    template_title = 'Información de Usuario'

    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(LoginAdditionalInfo, self).dispatch(*args, **kwargs)

    def get(self, request):
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if not request.user.is_authenticated:
                wresponse['status'] = 400
                raise ValueError(str('User no esta autenticado.'))

            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "get_user_info":
                    return self.getUserInformation(request)
                elif wtype == "save_user_info":
                    return self.saveUserInformation(request)
                else:
                    wresponse['status'] = 400
                    raise ValueError('Bad Request')
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def getUserInformation(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wuser_id = wdata.get("uid") or ''
            if wuser_id == '':
                wresponse['status'] = 400
                raise ValueError(str('UID no paso validacion.'))
            Qfilters = Q(id=wuser_id, is_active=True)
            objuser = User.objects.filter(Qfilters)
            if not objuser.exists():
                wresponse['status'] = 400
                raise ValueError(str('UID invalido.'))
            wfields = [
                "first_name", "last_name", "email",
            ]
            srlzuser = srlog.UserSerializer(objuser.first(), fields=wfields)
            wresponse["result"] = srlzuser.data
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def saveUserInformation(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            objuser = User.objects.filter(
                username__exact=request.user.username
            )
            if not objuser.exists():
                wresponse['status'] = 400
                raise ValueError(str('Usuario no existe.'))

            wdata["usuario"] = request.user.username
            wdata["clave"] = wdata.get("password")
            f = frm.MFUser(wdata)
            if not f.is_valid():
                werror = f.get_error_first()
                wresponse['status'] = 400
                raise ValueError(werror)

            wparameters = Validators.removeKeys(
                f.clean(), ['usuario', 'clave']
            )
            objuser.update(**wparameters)
            wresponse[
                "result"] = "Datos fueron procesados satisfactoriamente."
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))
