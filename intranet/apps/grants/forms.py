

from django import forms
from django.contrib.auth import models as mdlauth
from django.db.models import Q, F
from apps.catalogos import models as mdlcat
from apps.common import forms as frm
import json


class MFUser(forms.ModelForm):

    DEFAULT_MSG_REQUIRED = 'Este campo es requerido'
    DEFAULT_MSG_CHOICE = 'Ingrese una opcion valida, valor seleccionado no paso validacion'
    NO_REQUIRED = []
    REQUIRED = ['first_name', 'last_name', 'email', ]

    usuario = forms.CharField(min_length=4, max_length=50, required=True,
                              error_messages={
                                  'no_empty': DEFAULT_MSG_REQUIRED, },
                              )
    clave = forms.CharField(min_length=4, max_length=50, required=False,
                            error_messages={
                                'no_empty': DEFAULT_MSG_REQUIRED, },
                            )
    dni = forms.CharField(min_length=8, max_length=25, required=True,
                          error_messages={
                              'no_empty': DEFAULT_MSG_REQUIRED, },
                          )
    create = forms.IntegerField(required=True, initial=0,)
    all_customers = forms.IntegerField(required=True, initial=2,)
    all_orders = forms.IntegerField(required=True, initial=2,)

    class Meta:
        model = mdlauth.User
        fields = [
            "first_name", "last_name", "email",
            "is_active", "usuario", "dni", "clave",
            "create", "all_customers", "all_orders",
        ]

    def __init__(self, *args, **kwargs):
        super(MFUser, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False
        for wfield in self.REQUIRED:
            self.fields[wfield].required = True

        for wfield in self.fields.keys():
            lst_errors = self.fields[wfield].error_messages
            if lst_errors.get("required"):
                lst_errors["required"] = self.DEFAULT_MSG_REQUIRED
            if lst_errors.get("invalid_choice"):
                lst_errors["invalid_choice"] = self.DEFAULT_MSG_CHOICE

    def clean(self):
        cleaned_data = super().clean()
        # validation if user really exists
        if cleaned_data.get("create") == 1:
            qsuser = mdlauth.User.objects.filter(
                username__iexact=cleaned_data.get("usuario"))
            if qsuser.count() > 0:
                wmessage = {
                    "usuario": "existe, no paso validacion.",
                }
                raise forms.ValidationError(wmessage)

            wpassword = cleaned_data.get("clave") or ''
            if wpassword == '':
                wmessage = {
                    "clave": "ingrese contraseña de acceso.",
                }
                raise forms.ValidationError(wmessage)

        # change the string values to uppercase
        for item in self.fields.keys():
            if isinstance(cleaned_data.get(item), str):
                if item in ['clave', ]:
                    continue
                if item in ['email', 'usuario']:
                    cleaned_data[item] = cleaned_data.get(item).lower()
                    continue
                cleaned_data[item] = cleaned_data.get(item).upper()
        cleaned_data["username"] = cleaned_data.get("usuario")
        if cleaned_data.get("clave") != None:
            cleaned_data["password"] = cleaned_data.get("clave")
        return cleaned_data

    def get_error_first(self):
        wmessage = ''
        if self.errors:
            wresult = {}
            wlist_json = json.loads(self.errors.as_json())
            wkey = list(wlist_json.keys())[0]
            wvalues = wlist_json.get(wkey)[0].get("message")
            wresult["field"] = wkey.lower()
            wresult["message"] = wvalues.lower()
            wmessage = 'Error, {} : {}.'.format(
                wresult.get("field").upper(), wresult.get("message"),
            )
        return wmessage


class MFGroup(frm.MFDefault):

    name = forms.CharField(
        label='Nombre', max_length=80, strip=True, required=True,)
    estado = forms.IntegerField(label='Estado', required=True,)

    NO_REQUIRED = []

    class Meta:
        fields = ['name', 'estado', ]
        exclude = []

    def __init__(self, *args, **kwargs):
        super(MFGroup, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

    def clean(self):
        cleaned_data = super().clean()
        for item in self.fields.keys():
            if isinstance(cleaned_data.get(item), str):
                cleaned_data[item] = cleaned_data.get(item).upper()
        return cleaned_data


class MFVendedores(forms.ModelForm):

    DEFAULT_MSG_REQUIRED = 'Este campo es requerido'
    DEFAULT_MSG_CHOICE = 'Ingrese una opcion valida, valor seleccionado no paso validacion'
    NO_REQUIRED = [
    ]

    class Meta:
        model = mdlcat.Vendedores
        fields = '__all__'
        exclude = [
            'fregistro',
            'fhregistro', 'fhmodificacion',
            'username', 'username_modif',
        ]

    def __init__(self, *args, **kwargs):
        super(MFVendedores, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

        for wfield in self.fields.keys():
            lst_errors = self.fields[wfield].error_messages
            if lst_errors.get("required"):
                lst_errors["required"] = self.DEFAULT_MSG_REQUIRED
            if lst_errors.get("invalid_choice"):
                lst_errors["invalid_choice"] = self.DEFAULT_MSG_CHOICE

    def clean(self):
        cleaned_data = super().clean()
        for item in self.fields.keys():
            if isinstance(cleaned_data.get(item), str):
                cleaned_data[item] = cleaned_data.get(item).upper()
        return cleaned_data

    def get_error_first(self):
        wresult = {}
        if self.errors:
            wlist_json = json.loads(self.errors.as_json())
            wkey = list(wlist_json.keys())[0]
            wvalues = wlist_json.get(wkey)[0].get("message")
            wresult["field"] = wkey.lower()
            wresult["message"] = wvalues.lower()
        return wresult
