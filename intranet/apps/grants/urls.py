from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from apps.grants import views as v

app_name = 'module_grants'

urlpatterns = [
    url(r'^sellers/',
        include([
            url('^$', v.SellersView.as_view(), name="sellers_main"),
            url('^update/?$', v.SellersUpdateView.as_view(), name="sellers_update"),
            url('^remove/?$', v.SellersRemoveView.as_view(), name="sellers_remove"),
            url('^append/?$', v.SellersAppendView.as_view(), name="sellers_append"),
        ])),
    url(r'^profile/',
        include([
            url('^$', v.ProfileView.as_view(), name="profile_main"),
            url('^update/?$', v.ProfileUpdateView.as_view(), name="profile_update"),
            url('^remove/?$', v.ProfileRemoveView.as_view(), name="profile_remove"),
            url('^append/?$', v.ProfileAppendView.as_view(), name="profile_append"),
        ])),
    url(r'^users/',
        include([
            url('^$', v.UserView.as_view(), name="users_main"),
            url('^update/?$', v.UserUpdateView.as_view(), name="users_update"),
            url('^remove/?$', v.UserRemoveView.as_view(), name="users_remove"),
            url('^append/?$', v.UserAppendView.as_view(), name="users_append"),
        ])),
]
