from django.shortcuts import render, redirect
from django.conf import settings
from django.views import View
from rest_framework.response import Response
from django.http import (
    JsonResponse, Http404,
)
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import (
    login_required,
    permission_required,
)
from django.core.exceptions import PermissionDenied
from django.db.models import (
    Value, Case, When, CharField, Count,
    Q, F,
    Prefetch,
)

from apps.catalogos import (
    models as mdlcat,
    serializers as srlzcat,
)
from apps.connections.config import (
    MYSQLConnection,
)
from apps.grants import forms as frm
from apps.common import utils as utl
from datetime import datetime, timedelta
from django.contrib.auth import models as mdlauth
from apps.login import serializers as srlzlog
import os
import json
# Create your views here.

DEFAULT_MAIN_PAGE = '/main/'
if settings.DEBUG == False:
    DEFAULT_MAIN_PAGE = os.path.join(settings.PREFIX_URL, 'main')


# Sellers process
@method_decorator(login_required, name='dispatch')
class SellersView(View):

    template_name = 'grants/grants_main.html'
    template_title = 'de Vendedores'
    template_header = 'Invoice | Mantenimiento {}'.format(template_title)

    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(SellersView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.display_vendedores', raise_exception=True),)
    def get(self, request):
        header, title = self.template_header, self.template_title
        return render(request, self.template_name, locals())

    @method_decorator(permission_required(
        'catalogos.display_vendedores', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            Qlist = Q(id__isnull=False)
            if wdata.get("nombres"):
                wdescription = wdata.get("nombres") or ''
                Qlist.add(
                    Q(nombres__icontains=wdescription), Qlist.connector
                )
            if wdata.get("estado"):
                wstatus = wdata.get("estado") or 2
                Qlist.add(
                    Q(estado__id__iexact=wstatus), Qlist.connector
                )

            wfields = [
                'id', 'nombres', 'fhregistro', 'fhmodificacion',
                'estado',
                'username', 'username_modif',
            ]
            wresult = mdlcat.Vendedores.objects.filter(Qlist) \
                .select_related("estado") \
                .select_related("username") \
                .select_related("username_modif") \
                .order_by("id")

            page["total"] = wresult.count()
            wserial = srlzcat.VendedoresSerializer(
                wresult[wfrom:wto], fields=wfields, many=True,
            ).data
            wresponse["result"] = {
                "rows": wserial,
                "page": page,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class SellersUpdateView(View):

    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(SellersUpdateView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.change_vendedores', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            f = frm.MFVendedores(wdata)
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not f.is_valid():
                wresponse['status'] = 400
                raise ValueError(f.get_error_first())
            wparameters = f.clean()
            objItem = mdlcat.Vendedores.objects.filter(id=wdata.get("id"))
            if not objItem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, vendedor especificado no existe.'))

            # objItem = objItem.first()
            wparameters.update({
                "fhmodificacion": datetime.now(),
                "username_modif": request.user,
            })
            objItem.update(**wparameters)

            wresponse["result"] = {
                'id': objItem.first().pk,
                'message': 'Datos fueron procesados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class SellersRemoveView(View):
    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(SellersRemoveView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.delete_vendedores', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            Qlist = Q(id__isnull=False)
            if wdata.get("id"):
                Qlist.add(Q(id=wdata.get("id")), Qlist.connector)
            objItem = mdlcat.Vendedores.objects.filter(Qlist)
            if not objItem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, vendedor especificado no existe.'))

            objItem = objItem.first()
            objItem.estado = mdlcat.Estados.objects.get(id__iexact=2)
            objItem.save()
            wresponse["result"] = {
                'message': 'Vendedor fue desactivado satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class SellersAppendView(View):
    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(SellersAppendView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.add_vendedores', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            f = frm.MFVendedores(wdata)
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not f.is_valid():
                wresponse['status'] = 400
                raise ValueError(f.get_error_first())
            wparameters = f.clean()
            wparameters["username"] = request.user
            objItem = mdlcat.Vendedores(**wparameters)
            objItem.save()

            wresponse["result"] = {
                'id': objItem.pk,
                'message': 'Datos fueron registrados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

# Profile process


@method_decorator(login_required, name='dispatch')
class ProfileView(View):

    template_name = 'grants/grants_main.html'
    template_title = 'Perfiles de Sistema'
    template_header = 'Invoice | Mantenimiento {}'.format(template_title)

    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(ProfileView, self).dispatch(*args, **kwargs)

    def get(self, request):
        header, title = self.template_header, self.template_title
        return render(request, self.template_name, locals())

    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            Qlist = Q(id__isnull=False)
            if wdata.get("descripcion"):
                wdescription = wdata.get("descripcion") or ''
                Qlist.add(
                    Q(name__icontains=wdescription) |
                    Q(id__icontains=wdescription),
                    Qlist.connector
                )
            if wdata.get("estado"):
                wstatus = 1 if wdata.get("estado") == 1 else 0
                Qlist.add(
                    Q(groupextension__is_active=wstatus),
                    Qlist.connector
                )

            wfields = ['id', 'name', 'groupextension', ]
            wresult = mdlauth.Group.objects.filter(Qlist) \
                .select_related("groupextension") \
                .order_by("id")

            page["total"] = wresult.count()
            wserial = srlzlog.GroupSerializer(
                wresult[wfrom:wto], fields=wfields, many=True,
            ).data
            wresponse["result"] = {
                "rows": wserial,
                "page": page,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ProfileUpdateView(View):

    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(ProfileUpdateView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.change_group', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            f = frm.MFGroup(wdata)
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not f.is_valid():
                wresponse['status'] = 400
                raise ValueError(f.get_error_first())
            wparameters = f.clean()
            objItem = mdlauth.Group.objects.filter(id=wdata.get("id"))
            if not objItem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, grupo especificado no existe.'))
            objItem = objItem.first()
            objItem.name = wparameters.get("name")
            if hasattr(objItem, 'groupextension'):
                wstatus = 1 if wparameters.get("estado") == 1 else 0
                objItem.groupextension.is_active = wstatus
            objItem.save()

            if wdata.get("permissions"):
                objItem.permissions.remove(*objItem.permissions.all())
                wperms = mdlauth.Permission.objects.filter(
                    id__in=wdata.get("permissions") or []
                )
                if wperms.count() > 0:
                    objItem.permissions.add(*wperms)

            wresponse["result"] = {
                'id': objItem.pk,
                'message': 'Datos fueron procesados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ProfileRemoveView(View):
    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(ProfileRemoveView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.delete_user', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            Qlist = Q(id__isnull=False)
            if wdata.get("id"):
                Qlist.add(Q(id=wdata.get("id")), Qlist.connector)
            objItem = mdlauth.Group.objects.filter(Qlist)
            if not objItem.exists():
                wresponse['status'] = 422
                raise ValueError(str('Error, perfil especificado no existe.'))
            objItem = objItem.first()
            if hasattr(objItem, 'groupextension'):
                objItem.groupextension.is_active = 0
            objItem.save()
            wresponse["result"] = {
                'message': 'Concesionario fue desactivado satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ProfileAppendView(View):
    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(ProfileAppendView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.add_group', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            f = frm.MFGroup(wdata)
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not f.is_valid():
                wresponse['status'] = 400
                raise ValueError(f.get_error_first())
            wparameters = f.clean()
            # wparameters["username"] = request.user
            objItem = mdlauth.Group()
            objItem.name = wparameters.get("name")
            objItem.save()
            if hasattr(objItem, 'groupextension'):
                wstatus = 1 if wparameters.get("estado") == 1 else 0
                objItem.groupextension.is_active = wstatus
                objItem.groupextension.save()

            if wdata.get("permissions"):
                wperms = mdlauth.Permission.objects.filter(
                    id__in=wdata.get("permissions") or []
                )
                if wperms.count() > 0:
                    objItem.permissions.add(*wperms)

            wresponse["result"] = {
                'id': objItem.pk,
                'message': 'Datos fueron registrados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


# User process


@method_decorator(login_required, name='dispatch')
class UserView(View):

    template_name = 'grants/grants_usuarios.html'
    template_title = 'Usuarios de Sistema'
    template_header = 'Invoice | Mantenimiento {}'.format(template_title)

    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(UserView, self).dispatch(*args, **kwargs)

    def get(self, request):
        header, title = self.template_header, self.template_title
        return render(request, self.template_name, locals())

    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            Qlist = Q(id__isnull=False)
            if wdata.get("descripcion"):
                wdescription = wdata.get("descripcion") or ''
                Qlist.add(
                    Q(first_name__icontains=wdescription) |
                    Q(last_name__icontains=wdescription) |
                    Q(username__exact=wdescription),
                    Qlist.connector
                )
            if wdata.get("estado"):
                westado = 1 if wdata.get("estado") == 1 else 0
                Qlist.add(
                    Q(is_active=westado), Qlist.connector
                )
            wfields = [
                'id', 'username', 'first_name', 'last_name',
                'email', 'userextension', 'fullname',
                'is_active', 'date_joined',
            ]
            wresult = mdlauth.User.objects.filter(Qlist) \
                .order_by("first_name") \
                .select_related("userextension")

            page["total"] = wresult.count()
            wserial = srlzlog.UserSerializer(
                wresult[wfrom:wto], fields=wfields, many=True,
            ).data
            wresponse["result"] = {
                "rows": wserial,
                "page": page,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class UserUpdateView(View):

    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(UserUpdateView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.change_user', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            wdata.get("fields")["create"] = 0
            f = frm.MFUser(wdata.get("fields"))
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not f.is_valid():
                wresponse['status'] = 400
                raise ValueError(f.get_error_first())

            wparam = f.clean()
            objItem = mdlauth.User.objects.filter(
                username__iexact=wparam.get("usuario"))
            ufields = [
                "first_name", "last_name", "email",
                "username", "is_active",
            ]
            if objItem.exists():
                objItem.update(**{x: wparam.get(x) for x in ufields})
                objItem = objItem.first()
                # if new password was typed
                if wparam.get("password") != '':
                    objItem.set_password(wparam.get("password"))
                    objItem.save(update_fields=["password", ])
                # update the extended table of user
                if hasattr(objItem, 'userextension'):
                    objItem.userextension.dni = wparam.get("dni")
                    objItem.userextension.all_customers = wparam.get(
                        "all_customers")
                    objItem.userextension.all_orders = wparam.get(
                        "all_orders")
                    objItem.userextension.fhmodificacion = datetime.now()
                    objItem.userextension.save(
                        update_fields=[
                            "dni", "fhmodificacion", "all_customers",
                            "all_orders",
                        ])

            # update grants
            status, error = update_grants(objItem, request)
            if error:
                wresponse['status'] = 400
                raise ValueError(str(error))

            wresponse["result"] = {
                'id': objItem.pk,
                'message': 'Datos fueron registrados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class UserRemoveView(View):
    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(UserRemoveView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.delete_user', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            Qlist = Q(id__isnull=False)
            if wdata.get("id"):
                Qlist.add(Q(id=wdata.get("id")), Qlist.connector)
            objItem = mdlauth.User.objects.filter(Qlist)
            if not objItem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, usuario especificado no existe.'))
            objItem = objItem.first()
            objItem.is_active = 0
            objItem.save()

            wresponse["result"] = {
                'message': 'Concesionario fue desactivado satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class UserAppendView(View):
    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(UserAppendView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.add_user', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            wdata.get("fields")["create"] = 1
            f = frm.MFUser(wdata.get("fields"))
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not f.is_valid():
                wresponse['status'] = 400
                raise ValueError(f.get_error_first())

            wparam = f.clean()
            ufields = [
                "first_name", "last_name", "email",
                "username", "is_active",
            ]
            objItem = mdlauth.User(**{x: wparam.get(x) for x in ufields})
            objItem.set_password(wparam.get("clave"))
            objItem.save()
            if hasattr(objItem, 'userextension'):
                objItem.userextension.dni = wparam.get("dni")
                objItem.userextension.all_customers = wparam.get(
                    "all_customers")
                objItem.userextension.all_orders = wparam.get(
                    "all_orders")
                objItem.userextension.fhmodificacion = datetime.now()
                objItem.userextension.save(
                    update_fields=["dni", "fhmodificacion",
                                   "all_customers", "all_orders", ]
                )
            # update grants
            status, error = update_grants(objItem, request)
            if error:
                wresponse['status'] = 400
                raise ValueError(str(error))

            wresponse["result"] = {
                'id': objItem.pk,
                'message': 'Datos fueron registrados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


def update_grants(item, request):
    status, error = True, None
    try:
        wdata = json.loads(request.body.decode("utf8"))
        # user permissions
        wperms = mdlauth.Permission.objects.filter(
            id__in=wdata.get("permissions") or []
        )
        item.user_permissions.remove(
            *item.user_permissions.all())
        if wperms.count() > 0:
            item.user_permissions.add(*wperms)

        # group permissions member
        wgroups = mdlauth.Group.objects.filter(
            id__in=wdata.get("groups") or []
        )
        item.groups.remove(*item.groups.all())
        if wgroups.count() > 0:
            item.groups.add(*wgroups)

        # # clientes permissions
        # wclientes = mdlcat.Clientes.objects.filter(
        #     id__in=wdata.get("clientes") or []
        # )
        # item.catalogos_usercliente_user_set.all().delete()
        # if wclientes.count() > 0:
        #     objclientes = [mdlcat.UserCliente(
        #         cliente=x, user=item,
        #         fhregistro=datetime.now(),
        #         username=request.user,
        #     ) for x in wclientes]
        #     item.catalogos_usercliente_user_set.bulk_create(
        #         objclientes, wclientes.count()
        #     )

        status = True
    except Exception as e:
        status, error = False, str(e)
    return [status, error]
