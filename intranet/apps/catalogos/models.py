from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from apps.common.models import CommonStructure
from apps.connections.config import MYSQLConnection
from datetime import datetime
from django.db.models.manager import EmptyManager
from django.utils import timezone
# Create your models here.

DEFAULT_STATUS = 1


# added at 2019.09.19
class Vendedores(CommonStructure):
    nombres = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    estado = models.ForeignKey(
        'catalogos.Estados',
        on_delete=models.CASCADE, default=DEFAULT_STATUS)

    class Meta:
        verbose_name = "Vendedores"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['nombres'],),
        ]
        permissions = (
            ('display_vendedores', 'Visualizar Vendedores'),
        )

    def __str__(self):
        return str(self.nombres or '')

# added at 2019.07.12


class Combos(CommonStructure):
    descripcion = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.CASCADE,
        default=DEFAULT_STATUS)
    tipo = models.CharField(
        max_length=10, null=True, blank=True, default=None,)

    class Meta:
        verbose_name = "Combos"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
            models.Index(fields=['tipo'],),
        ]

    def __str__(self):
        return str(self.descripcion or '')


class UserCliente(CommonStructure):
    cliente = models.ForeignKey('catalogos.Clientes',
                                on_delete=models.CASCADE)
    user = models.ForeignKey('auth.User',
                             on_delete=models.CASCADE,
                             related_name="%(app_label)s_%(class)s_user_set",
                             )

    class Meta:
        verbose_name = "Usuario Cliente"
        ordering = ['fhregistro', ]
        get_latest_by = ['fhregistro', ]
        default_permissions = ()
        indexes = [
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]

    def __str__(self):
        return str(self.pk)


class GroupExtension(models.Model):
    group = models.OneToOneField(
        'auth.Group', on_delete=models.CASCADE, null=False, blank=False,
    )
    is_active = models.PositiveIntegerField(default=1, null=True, blank=False,)
    fhregistro = models.DateTimeField(
        null=True, default=timezone.now, blank=False)
    fhmodificacion = models.DateTimeField(null=True, default=None, blank=False)

    class Meta:
        verbose_name = "Grupo Extension"
        default_permissions = ()
        indexes = [
            # models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]

    def __str__(self):
        return str(self.group.pk)


class UserExtension(models.Model):
    username = models.OneToOneField(
        'auth.User', on_delete=models.CASCADE, null=False, blank=False,
    )
    is_lock = models.PositiveIntegerField(null=False, blank=False,
                                          default=0,
                                          validators=[
                                              MaxValueValidator(
                                                  1), MinValueValidator(0)
                                          ])
    is_new = models.PositiveIntegerField(null=False, blank=False,
                                         default=1,
                                         validators=[
                                             MaxValueValidator(
                                                 1), MinValueValidator(0)
                                         ])
    fecha_bloqueo = models.DateTimeField(
        null=True, blank=False, default=None,
    )
    fecha_cambio_clave = models.DateTimeField(
        null=True, blank=False, default=None,
    )
    dni = models.CharField(
        null=True,
        max_length=25,
        blank=True,
        default=None,
    )
    fhmodificacion = models.DateTimeField(
        default=None, null=True, blank=False,)
    all_customers = models.PositiveIntegerField(
        null=True, blank=False, default=2,
    )
    all_orders = models.PositiveIntegerField(
        null=True, blank=False, default=2,
    )

    class Meta:
        verbose_name = "Usuario Extension"
        default_permissions = ()


class MenusParent(CommonStructure):
    descripcion = models.CharField(
        max_length=50, null=False, blank=False, )
    icon_class = models.CharField(
        max_length=50, null=False, blank=False, default='fas fa-globe')
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.SET_NULL,
        null=True, default=DEFAULT_STATUS)

    class Meta:
        verbose_name = "Menus Parent"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class MenusDetalle(CommonStructure):
    descripcion = models.CharField(
        max_length=50, null=False, blank=False, )
    view_name = models.CharField(
        max_length=50, null=False, blank=False,)
    icon_class = models.CharField(
        max_length=50, null=False, blank=False, default='fas fa-globe')
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.SET_NULL,
        null=True, default=DEFAULT_STATUS)
    parent = models.ForeignKey(
        'catalogos.MenusParent',
        on_delete=models.SET_NULL,
        null=True,
        related_name="detalles",
    )
    permisos = models.ManyToManyField(
        'auth.permission',
        verbose_name=('Permisos por Menu Detalle'),
        blank=True,
        help_text=('Permisos especificos por Menu Detalle.'),
    )

    class Meta:
        verbose_name = "Menus Detalle"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Dias(CommonStructure):
    dia = models.PositiveIntegerField(null=False, blank=False,
                                      default=None,
                                      validators=[MaxValueValidator(
                                          7), MinValueValidator(1)]
                                      )
    descripcion = models.CharField(max_length=100, null=False, blank=False,)
    descripcion_english = models.CharField(
        max_length=100, null=False, blank=False,)
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)
    username = None
    username_modif = None

    class Meta:
        verbose_name = "Descripcion dia de semana"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        unique_together = ['dia', 'descripcion', 'estado', ]
        indexes = [
            models.Index(fields=['dia'],),
            models.Index(fields=['descripcion'],),
            models.Index(fields=['descripcion_english'],),
        ]

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    @property
    def username_modif(self):
        raise AttributeError(
            "'Manager' object has no attribute 'username_modif'")


class Meses(CommonStructure):
    mes = models.PositiveIntegerField(
        null=False, blank=False,
        default=None,
        validators=[
            MaxValueValidator(12),
            MinValueValidator(1)
        ],
    )
    descripcion = models.CharField(max_length=100, null=False, blank=False,)
    descripcion_english = models.CharField(
        max_length=100, null=False, blank=False,)
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)

    username = None
    username_modif = None

    class Meta:
        verbose_name = "Descripcion mes del año"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        unique_together = ['mes', 'descripcion', 'estado', ]
        indexes = [
            models.Index(fields=['mes'],),
            models.Index(fields=['descripcion'],),
            models.Index(fields=['descripcion_english'],),
        ]

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    @property
    def username_modif(self):
        raise AttributeError(
            "'Manager' object has no attribute 'username_modif'")

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Estados(CommonStructure):
    descripcion = models.CharField(max_length=200, null=False, blank=False,)
    estado = models.IntegerField(null=False, default=1, blank=False)
    username = None
    username_modif = None

    class Meta:
        verbose_name = "Estados"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
        ]

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    @property
    def username_modif(self):
        raise AttributeError(
            "'Manager' object has no attribute 'username_modif'")


class TipoDocumento(CommonStructure):
    codigo = models.CharField(max_length=2, null=False, blank=False,)
    doc_clie = models.CharField(max_length=10, null=False, blank=False,)
    documento = models.CharField(max_length=100, null=False, blank=False,)
    longitud = models.PositiveIntegerField(
        null=False, blank=False,
        default=0,
        validators=[
            MaxValueValidator(100),
            MinValueValidator(1)
        ],
    )
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)

    username = None
    username_modif = None

    class Meta:
        verbose_name = "Tipo de Documento"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['codigo'],),
            models.Index(fields=['doc_clie'],),
            models.Index(fields=['documento'],),
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]

    def __str__(self):
        return self.doc_clie

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    @property
    def username_modif(self):
        raise AttributeError(
            "'Manager' object has no attribute 'username_modif'")


class Paises(CommonStructure):
    codigo = models.CharField(max_length=5, null=True, blank=True,)
    descripcion = models.CharField(max_length=200, null=True, blank=True,)
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)
    fecha_inicio = models.CharField(max_length=10, null=True, blank=True,)
    fecha_fin = models.CharField(max_length=10, null=True, blank=True,)
    username = None
    username_modif = None

    class Meta:
        verbose_name = "Paises"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['codigo'],),
            models.Index(fields=['descripcion'],),
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]

    def __str__(self):
        return '{} - {}'.format(self.codigo, self.nombre)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    @property
    def username_modif(self):
        raise AttributeError(
            "'Manager' object has no attribute 'username_modif'")


class TipoInvoice(CommonStructure):
    descripcion = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.CASCADE,
        default=DEFAULT_STATUS)

    class Meta:
        verbose_name = "Invoice Tipo"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]

    def __str__(self):
        return str(self.descripcion or '')


class Tipificaciones(CommonStructure):
    descripcion = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.CASCADE,
        default=DEFAULT_STATUS)

    class Meta:
        verbose_name = "Invoice Tipificaciones"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
        ]


class Clientes(CommonStructure):
    razon_social = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        default=None,
    )
    direccion = models.CharField(
        max_length=100, null=True, blank=True, default=None,)
    tdoc = models.ForeignKey(
        'catalogos.TipoDocumento', on_delete=models.SET_NULL, null=True, default=None)
    ndoc = models.CharField(max_length=25, null=True,
                            blank=True, default=None,)
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)
    telefono = models.CharField(
        max_length=50, null=True, blank=True, default=None,)
    celular = models.CharField(
        max_length=50, null=True, blank=True, default=None,)
    email = models.CharField(max_length=50, null=True, blank=True,)
    observaciones = models.TextField(max_length=200, null=True, blank=True,)
    contacto = models.CharField(
        max_length=100, null=True, blank=True, default=None,)
    ciudad = models.CharField(max_length=100, null=True, blank=True,)
    pais = models.ForeignKey(
        'catalogos.Paises', on_delete=models.SET_NULL,
        null=True, default=None,)
    tipificacion = models.ForeignKey(
        'catalogos.Tipificaciones',
        on_delete=models.SET_NULL,
        null=True,
        default=None,)
    contactado = models.ForeignKey(
        'catalogos.Combos',
        on_delete=models.SET_NULL,
        null=True,
        default=None,)

    class Meta:
        verbose_name = "Clientes"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['razon_social'],),
            models.Index(fields=['ndoc'],),
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['telefono'],),
            models.Index(fields=['celular'],),
        ]
        permissions = (
            ('filter_status_clientes', 'Filtro Estado Clientes'),
            ('display_clientes', 'Visualizar Clientes'),
            ('xlsreport_clientes', 'Exportar Reporte Clientes'),
            ('change_mininum_clientes', 'Modificacion Minima Clientes'),
        )

    def __str__(self):
        return self.razon_social

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class ClientesCargas(CommonStructure):
    hashid = models.CharField(max_length=25, null=False,
                              blank=False,)
    file = models.FileField(
        upload_to='uploads/customer/%Y/%m/%d/',
        null=False, blank=False,)
    file_name = models.CharField(
        max_length=50, null=False, blank=False,)
    file_type = models.CharField(
        max_length=25, null=False, blank=False,)
    file_size = models.PositiveIntegerField(
        null=False, blank=False, default=0,
    )

    class Meta:
        verbose_name = "Clientes Importación"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        unique_together = ['hashid', ]
        indexes = [
            models.Index(fields=['file'],),
            models.Index(fields=['file_name'],),
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]

    def __str__(self):
        return self.hashid

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class TipoDocumentoPago(CommonStructure):
    codigo = models.CharField(max_length=10, null=False, blank=False,)
    descripcion = models.CharField(max_length=250, null=False, blank=False,)
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)

    username = None
    username_modif = None

    class Meta:
        verbose_name = "Tipo de Documento Pago"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['codigo'],),
            models.Index(fields=['descripcion'],),
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]

    def __str__(self):
        return '{}|{}'.format(self.codigo, self.descripcion)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    @property
    def username_modif(self):
        raise AttributeError(
            "'Manager' object has no attribute 'username_modif'")


class FormaPago(CommonStructure):
    codigo = models.CharField(
        max_length=6, null=True, blank=True, default=None,
    )
    descripcion = models.CharField(max_length=250, null=False, blank=False,)
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)

    class Meta:
        verbose_name = "Forma Pago"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['codigo'],),
            models.Index(fields=['descripcion'],),
        ]

    def __str__(self):
        return '{}'.format(self.descripcion)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)
