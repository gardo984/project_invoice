
from django.contrib.auth import models as mdlauth
from django.db.models.signals import post_save
from django.dispatch import receiver
from apps.catalogos import models as mdlcat
from datetime import datetime

# signal after create a user profile


@receiver(post_save, sender=mdlauth.User,)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        mdlcat.UserExtension.objects.create(username=instance)

# signal after made any changes to a user profile


@receiver(post_save, sender=mdlauth.User,)
def save_user_profile(sender, instance, **kwargs):
    if hasattr(instance, 'userextension'):
        instance.userextension.fhmodificacion = datetime.now()
        instance.userextension.save()
    else:
        mdlcat.UserExtension.objects.create(username=instance)

# signal after create a group profile


@receiver(post_save, sender=mdlauth.Group,)
def create_group_profile(sender, instance, created, **kwargs):
    if created:
        mdlcat.GroupExtension.objects.create(group=instance)

# signal after made any changes to a group profile


@receiver(post_save, sender=mdlauth.Group,)
def save_group_profile(sender, instance, **kwargs):
    if hasattr(instance, 'groupextension'):
        instance.groupextension.fhmodificacion = datetime.now()
        instance.groupextension.save()
    else:
        mdlcat.GroupExtension.objects.create(group=instance)
