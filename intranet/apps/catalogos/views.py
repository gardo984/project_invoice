from django.shortcuts import render
from django.views import View
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import JsonResponse
from django.db.models import (
    Value, Case, When, CharField, Q, F,
    OuterRef, Subquery, IntegerField,
    Prefetch, Count,
)
from apps.connections.config import (
    MYSQLConnection,
)
import json
from datetime import datetime

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import (
    login_required,
    permission_required,
)
from django.core.exceptions import PermissionDenied

from apps.invoice import (
    models as mdlinv,
)
from apps.catalogos import (
    models as mdls,
    serializers as srlz,
    forms as frm,
    utils as utlcat,
)
from apps.common import utils as utl
from django.conf import settings
import os

# Create your views here.

DEFAULT_FORBIDDEN_PAGE = '/login/forbidden'


@method_decorator(login_required, name='dispatch')
class ViewCustomer(View):
    template_name = 'catalogos/catalogos_clientes.html'
    template_header = 'Invoice | Clientes'
    template_title = 'Clientes'
    defaultResponse = {
        "ok": 1,
        "result": 'Process has been processed successfuly.',
        "status": 200,
    }

    def dispatch(self, *args, **kwargs):
        return super(ViewCustomer, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.display_clientes', login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        wresponse = self.defaultResponse.copy()
        header = self.template_header
        title = self.template_title
        import_example = request.build_absolute_uri(
            os.path.join(
                settings.MEDIA_URL, 'downloads/import_example.csv')
        )
        # print(import_example)
        return render(request, self.template_name, locals())

    @method_decorator(permission_required(
        'catalogos.display_clientes', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "add_catalogos_customer":
                    return self.addCustomer(request)
                if wtype == "add_customer_by_phone":
                    return self.addCustomerByPhone(request)
                if wtype == "get_catalogos_customers":
                    return self.getCustomers(request)
                if wtype == "remove_catalogos_customer":
                    return self.removeCustomer(request)
                if wtype == "update_catalogos_customer":
                    return self.updateCustomer(request)
                if wtype == "get_xls_report":
                    return self.getCustomerReport(request)
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'catalogos.display_clientes', raise_exception=True),)
    def getCustomers(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))

            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            select_fields = [
                "id", "fhregistro", "fhmodificacion", "razon_social",
                "direccion", "ndoc", "telefono", "celular", "email",
                "observaciones", "estado", "tdoc",
                "contacto", "username", "username_modif",
                "ciudad", "pais", "tipificacion", "leyenda",
                "contactado", "estado_descripcion",
                "pais_descripcion", "tdoc_descripcion",
                "fhregistro_format", "fhmodificacion_format",
                "contactado_descripcion", "tipificacion_descripcion",
            ]
            wdata.update({
                "user": request.user, "select_fields": select_fields,
            })
            outcome, err = utlcat.getListCustomers(**wdata)
            if err:
                raise ValueError(str(err))

            page["total"] = outcome.count()
            srlzdata = srlz.ClientesSerializer(
                outcome[wfrom:wto], fields=select_fields, many=True,
            ).data
            wresponse["result"] = {
                "rows": srlzdata,
                "page": page,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('catalogos.add_clientes', raise_exception=True),)
    def addCustomerByPhone(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wstatus, werror = self.ifCustomerExist(wdata)
            if werror:
                wresponse['status'] = 400
                raise ValueError(str(werror))

            wparameters = {
                "telefono": wdata.get("telefono"),
                "estado": mdls.Estados.objects.get(descripcion__iexact="activo"),
            }
            print(wparameters)
            objCliente = mdls.Clientes(**wparameters)
            objCliente.save()
            wresponse['result'] = {
                "id":  objCliente.pk,
                "message": "Cliente fue registrado satisfactoriamente",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('catalogos.add_clientes', raise_exception=True),)
    def addCustomer(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wstatus, werror = self.ifCustomerExist(wdata)
            if werror:
                wresponse['status'] = 400
                raise ValueError(str(werror))

            f = frm.MFClientes(wdata)
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")

            if not f.is_valid():
                werror = f.get_error_first()
                werr_msg = 'Error, {} : {}.'.format(
                    werror.get("field"), werror.get("message"),
                )
                wresponse['status'] = 400
                raise ValueError(werr_msg)

            wparameters = f.clean()
            objCliente = mdls.Clientes(**wparameters)
            objCliente.save()
            wresponse['result'] = {
                "id": objCliente.pk,
                "message": "Cliente fue registrado satisfactoriamente",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def ifCustomerExist(self, source):
        status, error = False, None
        try:
            w = source
            wphone = (w.get("telefono") or '').strip()
            if w.get("id"):
                wlist = mdls.Clientes.objects.filter(
                    telefono__iexact=wphone,
                ).exclude(id=w.get("id"))
            else:
                wlist = mdls.Clientes.objects.filter(
                    telefono__iexact=wphone,
                )

            if wlist.count() > 0:
                raise ValueError('Error, Cliente ya se encuentra registrado.')
        except Exception as e:
            status, error = True, str(e)
        return [status, error]

    @method_decorator(permission_required('catalogos.change_clientes', raise_exception=True),)
    def updateCustomer(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wid = wdata.get("id") or ''
            if wid == '':
                raise ValueError('Error, id es valor requerido.')

            objCliente = mdls.Clientes.objects.filter(id=int(wid))
            if not objCliente.exists():
                wresponse['status'] = 400
                raise ValueError("Error, Cliente no existe.")

            wstatus, werror = self.ifCustomerExist(wdata)
            if werror:
                wresponse['status'] = 400
                raise ValueError(str(werror))

            f = frm.MFClientes(wdata)
            if not f.is_valid():
                werror = f.get_error_first()
                werr_msg = 'Error, {} : {}.'.format(
                    werror.get("field"), werror.get("message"),
                )
                wresponse['status'] = 400
                raise ValueError(werr_msg)
            wparameters = f.clean()
            wparameters["fhmodificacion"] = datetime.now()
            wparameters["username_modif"] = request.user
            objCliente.update(**wparameters)
            wresponse['result'] = {
                "id": objCliente.get().id,
                "message": "Datos fueron procesados satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('catalogos.delete_clientes', raise_exception=True),)
    def removeCustomer(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widcustomer = wdata.get("item").get("id") or 0
            mdls.Clientes.objects.get(id=int(widcustomer)).delete()
            wresponse['result'] = {
                "message": "Cliente fue eliminado satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('catalogos.xlsreport_clientes', raise_exception=True),)
    def getCustomerReport(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            select_fields = [
                "id", "fhregistro", "fhmodificacion", "razon_social",
                "direccion", "ndoc", "telefono", "celular", "email",
                "observaciones", "estado", "tdoc",
                "contacto", "username", "username_modif",
                "ciudad", "pais", "tipificacion", "leyenda",
                "contactado", "estado_descripcion",
                "pais_descripcion", "tdoc_descripcion",
                "fhregistro_format", "fhmodificacion_format",
                "contactado_descripcion", "tipificacion_descripcion",
            ]
            wdata.update({
                "user": request.user, "select_fields": select_fields,
            })
            outcome, err = utlcat.getListCustomers(**wdata)
            if err:
                raise ValueError(str(err))

            srlzdata = srlz.ClientesSerializer(
                outcome, fields=select_fields, many=True,
            ).data
            wreport, werror = utlcat.get_xls_report(request, srlzdata)
            if werror:
                raise ValueError(str(werror))
            wresponse["result"] = wreport
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ViewCustomerImport(View):

    defaultResponse = {
        "ok": 1,
        "result": 'Process has been processed successfuly.',
        "status": 200,
    }

    def dispatch(self, *args, **kwargs):
        return super(ViewCustomerImport, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required('catalogos.add_clientes', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            # upload files
            if request.POST.get("details"):
                return self.uploadFiles(request)
            # get list files
            wdata = json.loads(request.body.decode("utf8"))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "get_list_files":
                    return self.getListFiles(request)
                elif wtype == "remove_file":
                    return self.removeFile(request)
            wresponse["status"] = 400
            raise ValueError(str('Bad Request'))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def removeFile(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            whash = wdata.get("hashid") or ''
            Qfilters = Q(id__isnull=False, hashid=whash)
            objFile = mdls.ClientesCargas.objects.filter(
                Qfilters
            )
            if not objFile.exists():
                wresponse["status"] = 400
                raise ValueError(str('Error, Hash ID no valido.'))
            objFile.get().delete()
            wresponse["result"] = "Archivo fue eliminado satisfactoriamente."
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def getListFiles(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            outcome = mdls.ClientesCargas.objects.all()
            srzoutcome = srlz.ClientesCargasSerializer(
                outcome, many=True).data
            wresponse["result"] = srzoutcome
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def importData(self, wfilename):
        status, error = True, None
        try:
            lines, error = utl.Validators.getContentFile(wfilename)
            if error:
                raise ValueError(str(error))
            headers = [x.replace("\n", '') for x in lines[0].split(';')]
            rows = [dict(zip(headers, x.split(';'))) for x in lines[1:]]
            # mandatory = [
            #     "razon_social", "direccion", "ndoc",
            #     "tdoc_id", "ciudad", "pais_id",
            # ]
            mandatory = ["telefono", ]
            # validating if mandatory fields were set
            print(headers)
            for x in rows:
                for y in mandatory:
                    if len(x.get(y) or '') == 0:
                        raise ValueError(
                            str('error, hay campos mandatorios vacios.'))
            # validating if there are customer that really exists
            # lst_documents = [(x.get("ndoc") or '').strip() for x in rows]
            lst_documents = [(x.get("telefono") or '').strip() for x in rows]
            z = mdls.Clientes.objects.filter(telefono__in=lst_documents)
            if z.exists():
                raise ValueError(str('error, hay clientes existentes.'))

            for x in rows:
                # validating if type of document really exists
                wtipo_doc = x.get("tdoc_id", '').strip()
                wpais = x.get("pais_id", '').strip()
                if wtipo_doc != '':
                    z = mdls.TipoDocumento.objects.filter(
                        doc_clie__iexact=wtipo_doc)
                    if not z.exists():
                        wmessage = "error, tipo de documento '{}' no existe.".format(
                            wtipo_doc)
                        raise ValueError(str(wmessage))
                    x["tdoc_id"] = z.first().id
                else:
                    x["tdoc_id"] = None

                # validating if country really exists
                if wpais != '':
                    z = mdls.Paises.objects.filter(descripcion__iexact=wpais)
                    if not z.exists():
                        wmessage = "error, pais '{}' no existe.".format(wpais)
                        raise ValueError(str(wmessage))
                    x["pais_id"] = z.first().id
                else:
                    x["pais_id"] = None

            items = [mdls.Clientes(**x) for x in rows]
            mdls.Clientes.objects.bulk_create(items, len(items))

        except Exception as e:
            status, error = False, str(e)
        return [status, error]

    def uploadFiles(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = request.POST
            wdetails = json.loads(wdata.get("details"))
            if len(wdetails) == 0:
                wresponse["status"] = 422
                raise ValueError(str('Error, no hay archivos que procesar.'))
            for item in wdetails:
                wparam = {
                    "file": request.FILES.get(item.get("name")),
                    "file_name": item.get("name"),
                    "file_size": item.get("size"),
                    "file_type": item.get("name").split('.')[-1],
                    "hashid": utl.Validators.getMd5Hash(
                        utl.Validators.getDatetimeHash()
                    ),
                    "username": request.user,
                }
                # validating filetype
                wallowed = ['csv', 'txt', ]
                if wparam.get("file_type") not in wallowed:
                    wresponse["status"] = 422
                    wmsg = 'Error, formato {} no valido.'.format(
                        wparam.get("file_type"))
                    raise ValueError(str(wmsg))
                # validating size
                obj = mdls.ClientesCargas(**wparam)
                obj.save()
                status, error = self.importData(obj.file.path)
                if error:
                    obj.delete()
                    wresponse["status"] = 422
                    raise ValueError(str(error))
            wresponse[
                "result"] = "Carga de datos fue procesada satisfactoriamente."
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))
