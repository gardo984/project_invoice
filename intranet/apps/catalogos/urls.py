from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from apps.catalogos import views as v

app_name = 'module_catalogos'

urlpatterns = [
    url(r'^customer/$', v.ViewCustomer.as_view(), name='customer'),
    url(r'^customer/importacion/$',
        v.ViewCustomerImport.as_view(),
        name='customer_import'),
]
