
from apps.catalogos import models as mdls
from django import forms
from django.contrib.auth import models as mdlauth
from django.db.models import Q, F
import json

# Model Forms


class MenusDetallePermisosForm(forms.ModelForm):

    class Meta:
        model = mdls.MenusDetalle
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(MenusDetallePermisosForm, self).__init__(*args, **kwargs)
        Wexceptions = ["admin", "contenttypes", "sessions"]
        Qlist = Q(content_type__app_label__in=Wexceptions)
        self.fields[
            "permisos"].queryset = mdlauth.Permission.objects.exclude(Qlist)

# Forms


class MFClientes(forms.ModelForm):

    DEFAULT_MSG_REQUIRED = 'Este campo es requerido'
    DEFAULT_MSG_CHOICE = 'Ingrese una opcion valida, valor seleccionado no paso validacion'
    NO_REQUIRED = [
        'tdoc', 'pais', 'razon_social', 'tipificacion',
        'contactado',
    ]
    ADDITIONAL_REQUIRED = ["telefono", ]

    class Meta:
        model = mdls.Clientes
        fields = '__all__'
        exclude = [
            'fregistro', 'fhregistro', 'fhmodificacion',
            'username', 'username_modif', ]

    def __init__(self, *args, **kwargs):
        super(MFClientes, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

        for wfield in self.ADDITIONAL_REQUIRED:
            self.fields[wfield].required = True

        for wfield in self.fields.keys():
            lst_errors = self.fields[wfield].error_messages
            if lst_errors.get("required"):
                lst_errors["required"] = self.DEFAULT_MSG_REQUIRED
            if lst_errors.get("invalid_choice"):
                lst_errors["invalid_choice"] = self.DEFAULT_MSG_CHOICE

    def clean(self):
        cleaned_data = super().clean()
        wtipo_doc = cleaned_data.get("tdoc") or ''
        wndoc = cleaned_data.get("ndoc") or ''
        if wtipo_doc != '' and len(wndoc) > 0:
            if len(wndoc) < wtipo_doc.longitud:
                wmessage = {
                    "ndoc": "longitud para {} no paso validación".format(wtipo_doc),
                }
                raise forms.ValidationError(wmessage)

        for item in self.fields.keys():
            if isinstance(cleaned_data.get(item), str):
                if item in ["email", ]:
                    cleaned_data[item] = cleaned_data.get(item).lower()
                    continue
                cleaned_data[item] = cleaned_data.get(item).upper()
        return cleaned_data

    def get_error_first(self):
        wresult = {}
        if self.errors:
            wlist_json = json.loads(self.errors.as_json())
            wkey = list(wlist_json.keys())[0]
            wvalues = wlist_json.get(wkey)[0].get("message")
            wresult["field"] = wkey.lower()
            wresult["message"] = wvalues.lower()
        return wresult


"""
class MenusDetalleForm(forms.ModelForm):

	fields_readonly = [
		"id", "fhmodificacion", "fhregistro",
		"icon_class",
	]
	class Meta:
		model = mdls.MenusDetalle
		fields = [
			"id","descripcion",
			"icon_class", "view_name",
			"parent", "fhregistro",
			"fhmodificacion", "estado",
		]
		# widgets = {
		# 	"fhregistro" : 
		# }


	def __init__(self, *args, **kwargs):
		super(MenusDetalleForm, self).__init__(*args, **kwargs)
		instance = getattr(self, 'instance', None)
		if instance and instance.pk:
			self.fields['permisos']= mdls
			# pass
			# self.fields['icon_class'].widget.attrs['readonly'] = True
			# for item in self.fields_readonly:
			# 	if item in self.fields:
			# 		self.fields[item].widget.attrs['disabled'] = True
"""
