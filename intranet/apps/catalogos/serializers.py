#! /usr/bin/env python
# -*- coding:utf-8 -*-

from rest_framework import serializers
from apps.catalogos import models as m
from apps.common import serializers as srlcommon
# from apps.metas import serializers as srlmetas


# added at 2019.07.14

class CombosSerializer(srlcommon.DynamicFieldsModelSerializer):

    class Meta:
        model = m.Combos
        fields = '__all__'


class EstadosSerializer(srlcommon.DynamicFieldsModelSerializer):

    class Meta:
        model = m.Estados
        fields = '__all__'


class MenusDetalleSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.MenusDetalle
        fields = '__all__'


class MenusParentSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)
    detalles = MenusDetalleSerializer(
        fields=[
            'id', 'descripcion', 'view_name', 'icon_class',
        ],
        read_only=True,
        many=True,
    )

    class Meta:
        model = m.MenusParent
        fields = '__all__'


class TipoDocumentoSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.TipoDocumento
        fields = '__all__'


class PaisesSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.Paises
        fields = '__all__'


class TipoInvoiceSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.Tipificaciones
        fields = '__all__'


class TipificacionesSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.Tipificaciones
        fields = '__all__'


class ClientesSerializer(srlcommon.DynamicFieldsModelSerializer):
    leyenda = serializers.IntegerField()
    user_id = serializers.IntegerField()
    tipificacion = TipificacionesSerializer(
        fields=['id', 'descripcion'], read_only=True,
    )
    estado = EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)
    tdoc = TipoDocumentoSerializer(
        fields=[
            'id', 'codigo', 'doc_clie',
            'documento', 'estado'
        ],
        read_only=True,
    )
    pais = PaisesSerializer(
        fields=['id', 'codigo', 'descripcion'], read_only=True,)

    # added at 2019.07.14
    contactado = CombosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    cliente_nombre = serializers.SerializerMethodField()
    estado_descripcion = serializers.SerializerMethodField()
    pais_descripcion = serializers.SerializerMethodField()
    tdoc_descripcion = serializers.SerializerMethodField()
    contactado_descripcion = serializers.SerializerMethodField()
    tipificacion_descripcion = serializers.SerializerMethodField()

    class Meta:
        model = m.Clientes
        fields = '__all__'

    def get_estado_descripcion(self, obj):
        if hasattr(obj.estado, 'descripcion'):
            return obj.estado.descripcion
        return ''

    def get_pais_descripcion(self, obj):
        if hasattr(obj.pais, 'descripcion'):
            return obj.pais.descripcion
        return ''

    def get_tdoc_descripcion(self, obj):
        if hasattr(obj.tdoc, 'doc_clie'):
            return obj.tdoc.doc_clie
        return ''

    def get_contactado_descripcion(self, obj):
        if hasattr(obj.contactado, 'descripcion'):
            return obj.contactado.descripcion
        return ''

    def get_tipificacion_descripcion(self, obj):
        if hasattr(obj.tipificacion, 'descripcion'):
            return obj.tipificacion.descripcion
        return ''


class ClientesCargasSerializer(srlcommon.DynamicFieldsModelSerializer):

    file_url = serializers.SerializerMethodField()

    class Meta:
        model = m.ClientesCargas
        fields = '__all__'

    def get_file_url(self, obj):
        return obj.file.url


class TipoDocumentoPagoSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.TipoDocumentoPago
        fields = '__all__'


class FormaPagoSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.FormaPago
        fields = '__all__'

# added at 2019.09.19


class VendedoresSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.Vendedores
        fields = '__all__'
