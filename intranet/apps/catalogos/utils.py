from apps.common.utils import (
    Validators, ExportToExcel,
)
from django.db.models import (
    Value, Case, When, CharField, Q, F,
    OuterRef, Subquery, IntegerField,
    Prefetch, Count,
)
from apps.catalogos import (
    models as mdls,
    serializers as srlz,
    forms as frm,
    utils as utlcat,
)
from apps.invoice import (
    models as mdlinv,
)


def getListCustomers(**kwargs):
    outcome, error = [], None
    try:
        wdata = kwargs
        wcliente = (wdata.get('cliente') or '').strip()
        westado = wdata.get('estado') or ''
        wtipificacion = wdata.get('tipificacion') or ''

        Qfilters = Q(id__isnull=False)
        if wcliente != '':
            Qfilters.add(
                Q(razon_social__icontains=wcliente) |
                Q(telefono__icontains=wcliente) |
                Q(celular__icontains=wcliente),
                Qfilters.connector,
            )
        if westado != '':
            Qfilters.add(
                Q(estado__id=westado),
                Qfilters.connector,
            )
        if wtipificacion != '':
            Qfilters.add(
                Q(tipificacion__id=wtipificacion),
                Qfilters.connector,
            )

        objuser = wdata.get("user")
        if hasattr(objuser, 'userextension'):
            wgrant = objuser.userextension.all_customers
            if wgrant != 1:
                list_source = objuser.catalogos_usercliente_user_set.all()
                list_customer = [x.cliente_id for x in list_source]
                Qfilters.add(
                    Q(id__in=list_customer),
                    Qfilters.connector,
                )

        select_fields = kwargs.get("select_fields")

        qscustomers = mdlinv.InvoiceParent.objects.filter(
            cliente=OuterRef('id'), estado_id=1,
        ).annotate(nrows=Count('id'),) \
            .values('nrows')[:1]

        field_leyenda = Case(
            When(ndocuments__gt=0, then=Value('1')),
            default=Value('0'),
            output_field=IntegerField(),)
        field_documents = Subquery(qscustomers)

        wqueryset = mdls.Clientes.objects.filter(Qfilters) \
            .select_related("estado") \
            .select_related("tipificacion") \
            .select_related("contactado") \
            .select_related("tdoc__estado") \
            .select_related("pais__estado") \
            .select_related("username") \
            .select_related("username_modif") \
            .annotate(ndocuments=field_documents,) \
            .annotate(leyenda=field_leyenda,)
        outcome = wqueryset
    except Exception as e:
        error = str(e)
    return [outcome, error]


def get_xls_report(request, data):
    result, error = None, None
    datetime_format = "%d/%m/%Y %H:%M:%S"
    tz_format = "%Y-%m-%d %H:%M:%S"
    date_format = "%d/%m/%Y"
    time_format = "%H:%M:%S"
    try:
        wfields = [
            {"value": "ID", "field": "id", "bold": True, "w": 15, },
            {"value": "Fecha Registro", "field": "fhregistro_format", "bold": True,
             "format_in": tz_format, "format_out": date_format, "w": 20, },
            {"value": "Hora Registro", "field": "fhregistro_format", "bold": True,
             "format_in": tz_format, "format_out": time_format, "w": 20, },
            {"value": "Razon Social", "field": "razon_social", "bold": True, "w": 40, },
            {"value": "Direccion", "field": "direccion", "bold": True, "w": 40, },
            {"value": "T.Doc", "field": "tdoc_descripcion", "bold": True, "w": 20, },
            {"value": "Nro.Doc", "field": "ndoc", "bold": True, "w": 20, },
            {"value": "Telefono", "field": "telefono", "bold": True, "w": 20, },
            {"value": "Celular", "field": "celular", "bold": True, "w": 20, },
            {"value": "Email", "field": "email", "bold": True, "w": 20, },
            {"value": "Contacto", "field": "contacto", "bold": True, "w": 20, },
            {"value": "Obs.", "field": "observaciones", "bold": True, "w": 40, },
            {"value": "Pais", "field": "pais_descripcion", "bold": True, "w": 20, },
            {"value": "Ciudad", "field": "ciudad", "bold": True, "w": 20, },
            {"value": "Estado", "field": "estado_descripcion", "bold": True, "w": 20, },
            {"value": "Tipificacion", "field": "tipificacion_descripcion",
                "bold": True, "w": 30, },
            {"value": "Contactado", "field": "contactado_descripcion",
                "bold": True, "w": 30, },
            {"value": "Usuario", "field": "username", "bold": True, },
            {"value": "Fch.Modif.", "field": "fhmodificacion_format", "bold": True,
             "format_in": tz_format, "format_out": datetime_format, "w": 20, },
            {"value": "Usuario Modif", "field": "username_modif", "bold": True, },
        ]
        wparameters = {
            "start_from": 4,
            "request": request,
            "defaultWidth": 12,
            "title": "Reporte General de Clientes",
            "headers": wfields,
            "details": data,
        }
        objxls = ExportToExcel(**wparameters)
        outcome, error = objxls.generateFile()
        result = {
            "message": "Reporte fue generado satisfactoriamente.",
            "urlpath": outcome.get("filepath"),
        }
    except Exception as e:
        error = str(e)
    return [result, error]
