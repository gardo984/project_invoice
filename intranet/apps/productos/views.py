from django.shortcuts import render
from django.views import View
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import JsonResponse
from django.db.models import (
    Value, Case, When, CharField, Q, F,
    Count,
    Prefetch,
)
from apps.connections.config import (
    MYSQLConnection,
)
import json

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import (
    login_required,
    permission_required,
)
from django.core.exceptions import PermissionDenied
from apps.productos import (
    models as mdls,
    serializers as srlz,
    forms as frm,
    utils as utlprod,
)
from apps.catalogos import models as mdlcat
from datetime import datetime
from apps.common import utils as utl
# Create your views here.

DEFAULT_FORBIDDEN_PAGE = '/login/forbidden'


defaultResponse = {
    "ok": 1,
    "result": 'Process has been processed successfuly.',
    "status": 200,
}


@method_decorator(login_required, name='dispatch')
class ViewCategory(View):
    template_name = 'productos/products_category.html'
    template_header = 'Invoice | Categorias'
    template_title = 'Categorias'

    def dispatch(self, *args, **kwargs):
        return super(ViewCategory, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required('productos.add_categoria', login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        wresponse = defaultResponse.copy()
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    @method_decorator(permission_required('productos.add_categoria', raise_exception=True),)
    def post(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "get_products_categories":
                    return self.getCategories(request)
                if wtype == "remove_products_category":
                    return self.removeCategory(request)
                if wtype == "update_products_category":
                    return self.updateCategory(request)
                if wtype == "add_products_category":
                    return self.addCategory(request)
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('productos.add_categoria', raise_exception=True),)
    def getCategories(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wfilters = {}
            wcategoria = wdata.get('categoria') or ''
            westado = wdata.get('estado') or ''
            Qfilters = Q(id__isnull=False)
            if wcategoria != '':
                Qfilters.add(
                    Q(descripcion__icontains=wcategoria),
                    Qfilters.connector,
                )
            if westado != '':
                Qfilters.add(
                    Q(estado__id=westado),
                    Qfilters.connector,
                )

            wqueryset = mdls.Categoria.objects.filter(Qfilters) \
                .select_related("estado")
            woutcome = srlz.CategoriaSerializer(wqueryset, many=True).data
            wresponse['result'] = woutcome
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('productos.add_categoria', raise_exception=True),)
    def addCategory(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wcategoria = wdata.get("categoria")
            westado = wdata.get("estado")
            objCategory = mdls.Categoria(
                descripcion=wcategoria,
                estado=mdlcat.Estados.objects.get(id=westado)
            )
            objCategory.save()
            wresponse['result'] = {
                "id": objCategory.pk,
                "message": "Categoria fue registrada satisfactoriamente",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('productos.change_categoria', raise_exception=True),)
    def updateCategory(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widcategoria = wdata.get("id") or 0
            objCategory = mdls.Categoria.objects.get(id=widcategoria)
            objCategory.descripcion = wdata.get("categoria")
            objCategory.estado = mdlcat.Estados.objects.get(
                id=int(wdata.get("estado")))
            objCategory.save()
            wresponse['result'] = {
                "id": objCategory.pk,
                "message": "Cambios fueron actualizados satisfactoriamente",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('productos.delete_categoria', raise_exception=True),)
    def removeCategory(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widcategoria = wdata.get("item").get("id") or 0
            mdls.Categoria.objects.get(id=int(widcategoria)).delete()
            wresponse['result'] = {
                "message": "Categoria fue eliminada satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ViewArticle(View):
    template_name = 'productos/products_articles.html'
    template_header = 'Invoice | Articulos'
    template_title = 'Articulos'

    def dispatch(self, *args, **kwargs):
        return super(ViewArticle, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required('productos.add_articulos', login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        wresponse = defaultResponse.copy()
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    @method_decorator(permission_required('productos.add_articulos', raise_exception=True),)
    def post(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "get_products_articles":
                    return self.getArticles(request)
                if wtype == "remove_products_article":
                    return self.removeArticle(request)
                if wtype == "update_products_article":
                    return self.updateArticle(request)
                if wtype == "add_products_article":
                    return self.addArticle(request)
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('productos.add_articulos', raise_exception=True),)
    def getArticles(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wcategoria = wdata.get('categoria') or ''
            warticulo = wdata.get('articulo') or ''
            westado = wdata.get('estado') or ''
            Qfilters = Q(id__isnull=False)
            if wcategoria != '':
                Qfilters.add(
                    Q(categoria__id=wcategoria),
                    Qfilters.connector,
                )
            if warticulo != '':
                Qfilters.add(
                    Q(descripcion__icontains=warticulo),
                    Qfilters.connector,
                )
            if westado != '':
                Qfilters.add(
                    Q(estado__id=westado),
                    Qfilters.connector,
                )

            wqueryset = mdls.Articulos.objects.filter(Qfilters) \
                .select_related("estado") \
                .select_related("categoria__estado")

            woutcome = srlz.ArticulosSerializer(wqueryset, many=True).data
            wresponse['result'] = woutcome
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('productos.add_articulos', raise_exception=True),)
    def addArticle(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            warticulo = wdata.get("articulo")
            wcategoria = wdata.get("categoria")
            westado = wdata.get("estado")
            objArticle = mdls.Articulos(
                descripcion=warticulo,
                categoria=mdls.Categoria.objects.get(id=wcategoria),
                estado=mdlcat.Estados.objects.get(id=westado),
            )
            objArticle.save()
            wresponse['result'] = {
                "id": objArticle.pk,
                "message": "Articulo fue registrada satisfactoriamente",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('productos.change_articulos', raise_exception=True),)
    def updateArticle(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widarticulo = wdata.get("id") or 0
            objArticle = mdls.Articulos.objects.get(id=widarticulo)
            objArticle.descripcion = wdata.get("articulo")
            objArticle.categoria = mdls.Categoria.objects.get(
                id=int(wdata.get("categoria")))
            objArticle.estado = mdlcat.Estados.objects.get(
                id=int(wdata.get("estado")))
            objArticle.save()
            wresponse['result'] = {
                "id": objArticle.pk,
                "message": "Cambios fueron actualizados satisfactoriamente",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('productos.delete_articulos', raise_exception=True),)
    def removeArticle(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widarticulo = wdata.get("item").get("id") or 0
            mdls.Articulos.objects.get(id=int(widarticulo)).delete()
            wresponse['result'] = {
                "message": "Articulo fue eliminada satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ViewPurcharseOrder(View):
    template_name = 'productos/products_purcharse_order.html'
    template_header = 'Productos | Orden de Compra'
    template_title = 'Orden de Compra'

    def dispatch(self, *args, **kwargs):
        return super(ViewPurcharseOrder, self).dispatch(*args, **kwargs)

    def get(self, request):
        wresponse = defaultResponse.copy()
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    def post(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "get_ordenes_compra":
                    return self.get_list_items(request)
                if wtype == "get_orden_compra":
                    return self.get_item(request)
                if wtype == "remove_orden_compra":
                    return self.remove_item(request)
                if wtype == "update_orden_compra":
                    return self.update_item(request)
                if wtype == "add_orden_compra":
                    return self.add_item(request)
                if wtype == "get_xls_report":
                    return self.get_general_report(request)
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def get_item(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not wdata.get("id"):
                wresponse["status"] = 400
                raise ValueError('Bad Request')

            Qfilters = Q(id__isnull=False, id__iexact=wdata.get("id"))
            qsdetails = mdls.OrdenCompraDetails.objects.all() \
                .select_related("producto__categoria")
            objItem = mdls.OrdenCompra.objects.filter(Qfilters) \
                .select_related("username") \
                .select_related("username_modif") \
                .prefetch_related(Prefetch(
                    "ordencompradetails_set", queryset=qsdetails,
                ))

            if not objItem.exists():
                wresponse["status"] = 422
                raise ValueError('Error, ID especificado no existe.')

            wfields = [
                "id", "fhregistro", "fhmodificacion",
                "documento_compra", "observaciones",
                "username", "username_modif",
                "anulado", "documento_fecha_emision",
                "ordencompradetails_set",
            ]
            wserial = srlz.OrdenCompraSerializer(
                objItem.first(),
                fields=wfields,
            ).data
            for item in wserial.get("ordencompradetails_set"):
                item["visible"] = True

            wresponse["result"] = wserial
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def get_list_items(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))

            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            wdescripcion = wdata.get('descripcion') or ''
            Qfilters = Q(
                id__isnull=False, anulado=1,
            )
            if wdescripcion != '':
                Qfilters.add(
                    Q(documento_compra__icontains=wdescripcion),
                    Qfilters.connector,
                )

            qsdetails = mdls.OrdenCompraDetails.objects.all() \
                .select_related("producto__categoria")

            wresult = mdls.OrdenCompra.objects.filter(Qfilters) \
                .select_related("username") \
                .select_related("username_modif") \
                .prefetch_related(Prefetch(
                    "ordencompradetails_set", queryset=qsdetails
                )) \
                .annotate(ndetails=Count(F('ordencompradetails'))) \
                .order_by('-fhregistro')

            page["total"] = wresult.count()
            wserial = srlz.OrdenCompraSerializer(
                wresult[wfrom:wto], many=True,
            ).data
            wresponse["result"] = {
                "rows": wserial,
                "page": page,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('productos.add_ordencompra', raise_exception=True),)
    def add_item(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            f = frm.MFOrdenCompra(wdata)
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            # parent layer validation
            if not f.is_valid():
                wresponse['status'] = 400
                raise ValueError(f.get_error_first())
            wparameters = f.clean()
            wparameters.update({
                "username": request.user,
            })
            objItem = mdls.OrdenCompra(**wparameters)
            objItem.save()
            # details layer validation
            wdetails = wdata.get("details") or []
            result, error = self.validateDetails(
                request.user, objItem, wdetails,
            )
            if error:
                raise ValueError(str(error))
            #--------------------------
            wresponse['result'] = {
                "id": objItem.pk,
                "message": "Cambios fueron actualizados satisfactoriamente",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('productos.change_ordencompra', raise_exception=True),)
    def update_item(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            worden_id = wdata.get("id") or 0
            # print(wdata)
            objItem = mdls.OrdenCompra.objects.filter(
                id__iexact=worden_id)
            if not objItem.exists():
                raise ValueError('Error, ID especificado no existe.')

            f = frm.MFOrdenCompra(wdata)
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            # parent layer validation
            if not f.is_valid():
                wresponse['status'] = 400
                raise ValueError(f.get_error_first())
            wparameters = f.clean()
            wparameters.update({
                "fhmodificacion": datetime.now(), "username_modif": request.user,
            })
            # details layer validation
            wdetails = wdata.get("details") or []
            result, error = self.validateDetails(
                request.user, objItem.first(), wdetails,
            )
            if error:
                raise ValueError(str(error))
            #--------------------------

            objItem.update(**wparameters)

            wresponse['result'] = {
                "id": objItem.first().pk,
                "message": "Cambios fueron actualizados satisfactoriamente",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('productos.delete_ordencompra', raise_exception=True),)
    def remove_item(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widarticulo = wdata.get("item").get("id") or 0
            objItem = mdls.OrdenCompra.objects.filter(
                id__iexact=int(widarticulo))
            if not objItem.exists():
                wresponse["status"] = 400
                raise ValueError("Error, ID orden compra no existe.")
            objItem.update(
                anulado=2, fhmodificacion=datetime.now(), username_modif=request.user,
            )
            wresponse['result'] = {
                "message": "Anulacion fue procesada satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required('productos.xlsreport_ordencompra', raise_exception=True),)
    def get_general_report(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            outcome, err = utlprod.getXlsComprasList(**wdata)
            if err:
                raise ValueError(err)
            wreport, err = utlprod.get_xls_report(request, outcome)
            if err:
                raise ValueError(err)
            wresponse["result"] = wreport
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def validateDetails(self, obj_user, obj_orden, details=[],):
        result, error = None, None
        try:
            objItem = obj_orden
            objItem.ordencompradetails_set.all().delete()
            items = []
            for item in details:
                wrequeriment = {
                    "producto": item.get("producto").get("id"),
                    "item": item.get("item"),
                    "cantidad": item.get("cantidad"),
                    "orden_compra": objItem.id,
                    "username": obj_user.id,
                }
                fdet = frm.MFOrdenCompraDetails(wrequeriment)
                if not fdet.is_bound:
                    raise ValueError("Bad Request")
                # parent layer validation
                if not fdet.is_valid():
                    raise ValueError(fdet.get_error_first())
                wparameters = fdet.clean()
                items.append(mdls.OrdenCompraDetails(**wparameters))
            mdls.OrdenCompraDetails.objects.bulk_create(items, len(items))

        except Exception as e:
            error = str(e)
        return [result, error]
