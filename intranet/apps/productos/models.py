from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from apps.common.models import CommonStructure
from apps.connections.config import MYSQLConnection
from datetime import datetime
from django.db.models.manager import EmptyManager
from decimal import Decimal
# Create your models here.

DEFAULT_STATUS = 1


class OrdenCompra(CommonStructure):
    documento_compra = models.CharField(
        max_length=50, default=None, null=True, blank=True,)
    documento_fecha_emision = models.DateField(
        default=None, blank=False,
        null=True,
        auto_now=False, auto_now_add=False,
    )
    observaciones = models.TextField(
        default=None, null=True, blank=True,)
    anulado = models.PositiveIntegerField(
        null=False, blank=False, default=1,)

    class Meta:
        verbose_name = "Orden de compra Parent"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]
        permissions = (
            ('xlsreport_ordencompra', 'Exportar Reporte Orden Compra'),
        )


class OrdenCompraDetails(CommonStructure):
    orden_compra = models.ForeignKey(
        'productos.OrdenCompra',
        on_delete=models.CASCADE,
        null=False, blank=False,)
    producto = models.ForeignKey(
        'productos.Articulos', on_delete=models.CASCADE,
        null=False, blank=False,)
    cantidad = models.PositiveIntegerField(
        null=True,
        default=0, blank=False,)
    item = models.PositiveIntegerField(null=True, default=None,)

    class Meta:
        verbose_name = "Orden de compra Details"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['fhregistro'],),
        ]


class Categoria(CommonStructure):
    descripcion = models.CharField(max_length=200, null=False, blank=False,)
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)

    class Meta:
        verbose_name = "Categorias"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
        ]

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Articulos(CommonStructure):
    descripcion = models.CharField(max_length=200, null=False, blank=False,)
    precio = models.DecimalField(max_digits=12,
                                 decimal_places=2,
                                 null=True,
                                 default=Decimal('0.00'),)
    categoria = models.ForeignKey(
        'productos.Categoria', on_delete=models.CASCADE,)
    estado = models.ForeignKey(
        'catalogos.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)

    class Meta:
        verbose_name = "Articulos"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
        ]

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)
