#! /usr/bin/env python
# -*- coding:utf-8 -*-

from rest_framework import serializers
from apps.catalogos import serializers as srlcat
from apps.productos import models as m
from apps.common import serializers as srlcommon


class CategoriaSerializer(srlcommon.DynamicFieldsModelSerializer):

    estado = srlcat.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.Categoria
        fields = '__all__'


class ArticulosSerializer(srlcommon.DynamicFieldsModelSerializer):

    estado = srlcat.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)
    categoria = CategoriaSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.Articulos
        fields = '__all__'


class OrdenCompraDetailsSerializer(srlcommon.DynamicFieldsModelSerializer):

    producto = ArticulosSerializer(
        fields=["id", "descripcion", "categoria", ],
        read_only=True,
    )

    class Meta:
        model = m.OrdenCompraDetails
        fields = '__all__'


class OrdenCompraSerializer(srlcommon.DynamicFieldsModelSerializer):
    ndetails = serializers.IntegerField()
    ordencompradetails_set = OrdenCompraDetailsSerializer(
        fields=[
            "id", "item", "producto",
            "cantidad", "orden_compra",
        ],
        read_only=True, many=True,
    )

    class Meta:
        model = m.OrdenCompra
        fields = '__all__'
