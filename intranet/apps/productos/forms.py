from apps.productos import models as mdls
from django import forms
from django.contrib.auth import models as mdlauth
from django.db.models import Q, F
import json


class MFOrdenCompra(forms.ModelForm):

    DEFAULT_MSG_REQUIRED = 'Este campo es requerido'
    DEFAULT_MSG_CHOICE = 'Ingrese una opcion valida, valor seleccionado no paso validacion'
    NO_REQUIRED = []

    class Meta:
        model = mdls.OrdenCompra
        fields = '__all__'
        exclude = [
            'fregistro', 'fhregistro', 'fhmodificacion',
            'username', 'username_modif', "anulado", ]

    def __init__(self, *args, **kwargs):
        super(MFOrdenCompra, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

        for wfield in self.fields.keys():
            lst_errors = self.fields[wfield].error_messages
            if lst_errors.get("required"):
                lst_errors["required"] = self.DEFAULT_MSG_REQUIRED
            if lst_errors.get("invalid_choice"):
                lst_errors["invalid_choice"] = self.DEFAULT_MSG_CHOICE

    def clean(self):
        cleaned_data = super().clean()
        for item in self.fields.keys():
            if isinstance(cleaned_data.get(item), str):
                cleaned_data[item] = cleaned_data.get(item).upper()
        return cleaned_data

    def get_error_first(self):
        wmessage = ''
        if self.errors:
            wresult = {}
            wlist_json = json.loads(self.errors.as_json())
            wkey = list(wlist_json.keys())[0]
            wvalues = wlist_json.get(wkey)[0].get("message")
            wresult["field"] = wkey.lower()
            wresult["message"] = wvalues.lower()
            wmessage = 'Error, {} : {}.'.format(
                wresult.get("field").upper(), wresult.get("message"),
            )
        return wmessage


class MFOrdenCompraDetails(forms.ModelForm):

    DEFAULT_MSG_REQUIRED = 'Este campo es requerido'
    DEFAULT_MSG_CHOICE = 'Ingrese una opcion valida, valor seleccionado no paso validacion'
    NO_REQUIRED = []

    class Meta:
        model = mdls.OrdenCompraDetails
        fields = '__all__'
        exclude = ['fregistro', 'fhregistro',
                   'fhmodificacion', 'username_modif', ]

    def __init__(self, *args, **kwargs):
        super(MFOrdenCompraDetails, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

        for wfield in self.fields.keys():
            lst_errors = self.fields[wfield].error_messages
            if lst_errors.get("required"):
                lst_errors["required"] = self.DEFAULT_MSG_REQUIRED
            if lst_errors.get("invalid_choice"):
                lst_errors["invalid_choice"] = self.DEFAULT_MSG_CHOICE

    def clean(self):
        cleaned_data = super().clean()
        for item in self.fields.keys():
            if isinstance(cleaned_data.get(item), str):
                cleaned_data[item] = cleaned_data.get(item).upper()
        return cleaned_data

    def get_error_first(self):
        wmessage = ''
        if self.errors:
            wresult = {}
            wlist_json = json.loads(self.errors.as_json())
            wkey = list(wlist_json.keys())[0]
            wvalues = wlist_json.get(wkey)[0].get("message")
            wresult["field"] = wkey.lower()
            wresult["message"] = wvalues.lower()
            wmessage = 'Error, {} : {}.'.format(
                wresult.get("field").upper(), wresult.get("message"),
            )
        return wmessage
