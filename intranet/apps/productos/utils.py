# To write and create xls files from zero
import xlsxwriter as xls
from datetime import datetime
# django settings info
from django.conf import settings

# To generate and modify a new xls file
from openpyxl import load_workbook
from openpyxl.utils import get_column_letter

from openpyxl.styles import (
    PatternFill, Border, Side,
    Alignment, Protection, Font,
    NamedStyle, Color, Fill,
)
from shutil import copyfile
from apps.common.utils import (
    Validators, ExportToExcel,
)
from apps.productos import (
    models as mdlprod,
    serializers as srlzprod,
)
from django.db.models import (
    Value, Case, When, CharField,
    IntegerField, Q, F,
    Count, Prefetch, Subquery, OuterRef,
)

import os


def getXlsComprasList(**kwargs):
    outcome, error = [], None
    try:
        wdescripcion = kwargs.get('descripcion') or ''
        Qfilters = Q(id__isnull=False)
        if wdescripcion != '':
            Qfilters.add(
                Q(documento_compra__icontains=wdescripcion),
                Qfilters.connector,
            )

        wfields = [
            "id", "fhregistro_format", "fhmodificacion_format",
            "documento_compra", "documento_fecha_emision",
            "observaciones", "username", "username_modif",
            "ndetails",
        ]
        qsdet = mdlprod.OrdenCompraDetails.objects.all() \
            .select_related("producto__estado")
        qs = mdlprod.OrdenCompra.objects.filter(Qfilters) \
            .select_related("username") \
            .select_related("username_modif") \
            .prefetch_related(Prefetch("ordencompradetails_set", queryset=qsdet)) \
            .annotate(ndetails=Count('ordencompradetails')) \
            .order_by('-fhregistro')

        wsource = srlzprod.OrdenCompraSerializer(
            qs, fields=wfields, many=True,
        ).data
        outcome = wsource
    except Exception as e:
        error = str(e)
    return [outcome, error]


def get_xls_report(request, data):
    result, error = None, None
    date_format = "%d/%m/%Y %H:%M:%S"
    tz_format = "%Y-%m-%d %H:%M:%S"
    try:
        wfields = [
            {"value": "Nro. O.Compra", "field": "id", "bold": True, "w": 15, },
            {"value": "Fch.Registro", "field": "fhregistro_format", "bold": True,
             "format_in": tz_format, "format_out": date_format, "w": 20, },
            {"value": "Doc. Fh.Emision", "field": "documento_fecha_emision", "bold": True,
             "format_in": "%Y-%m-%d", "format_out": "%d/%m/%Y", "w": 15, },
            {"value": "Doc. Compra", "field": "documento_compra",
                "bold": True, "w": 40, },
            {"value": "Nro. Items", "field": "ndetails", "bold": True, },
            {"value": "Usuario", "field": "username", "bold": True, },
            {"value": "Fch.Modif.", "field": "fhmodificacion_format", "bold": True,
             "format_in": tz_format, "format_out": date_format, "w": 20, },
            {"value": "Usuario Modif.", "field": "username_modif", "bold": True, },
        ]
        wparameters = {
            "start_from": 4,
            "request": request,
            "defaultWidth": 12,
            "title": "Reporte General de Ordenes Compra",
            "headers": wfields,
            "details": data,
        }
        objxls = ExportToExcel(**wparameters)
        outcome, error = objxls.generateFile()
        result = {
            "message": "Reporte fue generado satisfactoriamente.",
            "urlpath": outcome.get("filepath"),
        }
    except Exception as e:
        error = str(e)
    return [result, error]
