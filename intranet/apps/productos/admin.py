from django.contrib import admin
from apps.productos import models as mdls

# Register your models here.

class CategoriaAdmin(admin.ModelAdmin):
	 list_display = [
	     "id", "descripcion",
	     "_format_fhregistro", "_format_fhmodificacion",
	     "estado",
	 ]
	 list_display_links = ["id", "descripcion"]
	 fields = [
	     "id", "descripcion",
	     "fhregistro", "fhmodificacion",
	     "estado",
	 ]
	 readonly_fields = ["id", "fhregistro", "fhmodificacion"]
	 search_fields = [
	     "id", "descripcion", "estado__descripcion",
	 ]
	 ordering = ["-fhregistro"]
	 list_per_page = 20
	 empty_value_display = ""

	 def _format_fhregistro(self, obj):
	     wvalue = None
	     if not obj.fhregistro is None:
	         wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
	     return wvalue
	 _format_fhregistro.short_description = "Fecha Creacion"
	 _format_fhregistro.admin_order_field = "-fhregistro"

	 def _format_fhmodificacion(self, obj):
	     wvalue = None
	     if not obj.fhmodificacion is None:
	         wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
	     return wvalue
	 _format_fhmodificacion.short_description = "Fecha Modificacion"
	 _format_fhmodificacion.admin_order_field = "-fhmodificacion"

class ArticulosAdmin(admin.ModelAdmin):
	 list_display = [
	     "id", "descripcion", "categoria", "precio",
	     "_format_fhregistro", "_format_fhmodificacion",
	     "estado",
	 ]
	 list_display_links = ["id", "descripcion"]
	 fields = [
	     "id", "descripcion", "categoria",
	     "precio",
	     "fhregistro", "fhmodificacion",
	     "estado",
	 ]
	 readonly_fields = ["id", "fhregistro", "fhmodificacion"]
	 search_fields = [
	     "id", "descripcion", 
	     "categoria__descripcion", "estado__descripcion",
	 ]
	 ordering = ["-fhregistro"]
	 list_per_page = 20
	 empty_value_display = ""

	 def _format_fhregistro(self, obj):
	     wvalue = None	
	     if not obj.fhregistro is None:
	         wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
	     return wvalue
	 _format_fhregistro.short_description = "Fecha Creacion"
	 _format_fhregistro.admin_order_field = "-fhregistro"

	 def _format_fhmodificacion(self, obj):
	     wvalue = None
	     if not obj.fhmodificacion is None:
	         wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
	     return wvalue
	 _format_fhmodificacion.short_description = "Fecha Modificacion"
	 _format_fhmodificacion.admin_order_field = "-fhmodificacion"


wmodels = [
	 mdls.Categoria, mdls.Articulos,
]
winterfaces = [
	 CategoriaAdmin, ArticulosAdmin
]

for windex, witem in enumerate(wmodels):
	admin.site.register(wmodels[windex], winterfaces[windex])