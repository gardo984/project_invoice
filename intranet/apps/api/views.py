from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from apps.connections.config import (
    SQLConnection,
    MYSQLConnection,
)
# from apps.api.querys import (
#     cmd_obtener_ventas,
#     cmd_obtener_ventas_anuladas,
#     cmd_obtener_empleados,
#     cmd_metas_obtener_ventas,
#     cmd_metas_eliminar_ventas,
#     cmd_metas_eliminar_empleados,
#     cmd_metas_transfer_empleados,
# )

from apps.invoice import (
    models as mdlinv,
    serializers as srzinv,
)

from apps.productos import (
    models as mdl,
    serializers as srz,
)
from apps.catalogos import (
    models as mdlcat,
    serializers as srzcat,
)
from intranet import settings
from datetime import datetime
from django.db.models import (
    Count, Max, Min, Avg, Sum, Q, F,
    Case, When, Value,
    IntegerField, CharField,
)

# To generate and modify a new xls file
from openpyxl import load_workbook
from shutil import copyfile
import os

# To subtract month from date value
from dateutil.relativedelta import relativedelta
from calendar import monthrange

# To Send Email
from apps.mail.views import SendMailer
from apps.common.utils import Validators
from apps.login import (
    serializers as srlzlog,
)
from django.contrib.auth import models as mdlauth
# Create your views here.

defaultResponse = {
    "ok": 1,
    "result": 'Request has been processed successfuly.',
    "status": 200,
}


# added at 2019.09.19

class APIGetVendedores(APIView):

    def get(self, request):
        wresponse = defaultResponse.copy()
        try:
            outcome = mdlcat.Vendedores.objects.filter(
                estado=1,
            )
            wfields = [
                'id', 'nombres',
            ]
            srzoutcome = srzcat.VendedoresSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("nombres") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

# added at 2019.09.02


class APIGetTipoSeguimiento(APIView):

    def get(self, request):
        wresponse = defaultResponse.copy()
        try:
            outcome = mdlcat.Combos.objects.filter(
                estado=1, tipo__iexact="CBLF",
            )
            wfields = [
                'id', 'descripcion',
            ]
            srzoutcome = srzcat.CombosSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

# added at 2019.07.14


class APIGetContactados(APIView):

    def get(self, request):
        wresponse = defaultResponse.copy()
        try:
            outcome = mdlcat.Combos.objects.filter(
                estado=1, tipo__iexact="CBLC",
            )
            wfields = [
                'id', 'descripcion',
            ]
            srzoutcome = srzcat.CombosSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetFormaPago(APIView):

    def get(self, request):
        wresponse = defaultResponse.copy()
        try:
            outcome = mdlcat.FormaPago.objects.filter(estado=1) \
                .select_related("estado")

            wfields = [
                "id", "codigo", "descripcion",
            ]
            srzoutcome = srzcat.FormaPagoSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetTipoInvoice(APIView):

    def get(self, request):
        wresponse = defaultResponse.copy()
        try:
            outcome = mdlcat.TipoInvoice.objects.filter(estado=1) \
                .select_related("estado")

            wfields = [
                "id", "descripcion",
            ]
            srzoutcome = srzcat.TipoInvoiceSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetTipificacion(APIView):

    def get(self, request):
        wresponse = defaultResponse.copy()
        try:
            outcome = mdlcat.Tipificaciones.objects.filter(estado=1) \
                .select_related("estado")

            wfields = [
                "id", "descripcion",
            ]
            srzoutcome = srzcat.TipificacionesSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetPerfilPermisos(APIView):

    def get(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = request.GET
            wgroupname = None
            if wdata.get("uid"):
                wgroupname = wdata.get("uid")
                objgroup = mdlauth.Group.objects.filter(id__iexact=wgroupname)
                if not objgroup.exists():
                    raise ValueError(str('Error, perfil no existe'))

            fields, error = self.obtener_fields(wgroupname)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))

            permisos, error = self.obtener_permisos(wgroupname)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))

            wresponse["result"] = {
                "fields": fields,
                "permisos": permisos,
            }
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

    def obtener_fields(self, groupname):
        result, error = None, None
        try:
            if not groupname is None:
                objgroup = mdlauth.Group.objects \
                    .filter(id__iexact=groupname) \
                    .select_related("groupextension") \
                    .first()
                wfields = ["id", "name", ]
                srlzoutcome = srlzlog.GroupSerializer(objgroup, fields=wfields)
                result = srlzoutcome.data
            else:
                result = []
        except Exception as e:
            error = str(e)
        return [result, error]

    def obtener_permisos(self, groupname):
        result, error = [], None
        try:
            if groupname is None:
                wgroup_id, wgroup_grants = None, []
            else:
                objgroup = mdlauth.Group.objects.filter(
                    id__iexact=groupname).first()
                wgroup_grants = [x.id
                                 for x in objgroup.permissions.all()
                                 .prefetch_related('content_type')]
                wgroup_id = objgroup.id

            woutcome = mdlauth.Permission.objects.all() \
                .prefetch_related("content_type") \
                .annotate(group_id=Case(
                    When(id__in=wgroup_grants,
                         then=Value(wgroup_id)),
                    default=None,
                    output_field=IntegerField()
                )).order_by("content_type_id")

            wfields = ["id", "name", "group_id", ]
            srlzoutcome = srlzlog.GroupSerializer(
                woutcome, fields=wfields, many=True,
            )
            result = srlzoutcome.data
            for item in result:
                item["label"], item["key"] = item.get("name"), item.get("id")

        except Exception as e:
            error = str(e)
        return [result, error]


class APIGetUsuarioPermisos(APIView):

    def get(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = request.GET
            wusername = None
            if wdata.get("uid"):
                wusername = wdata.get("uid")
                objuser = mdlauth.User.objects.filter(
                    username__iexact=wusername)
                if not objuser.exists():
                    raise ValueError(str('Error, usuario no existe'))

            fields, error = self.obtener_fields(wusername)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))
            permisos, error = self.obtener_permisos(wusername)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))
            perfiles, error = self.obtener_perfiles(wusername)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))

            # permisos_clientes, error = self.obtener_perm_clientes(wusername)
            # if error:
            #     wresponse["status"] = 422
            #     raise ValueError(str(error))
            # permisos_concesionarios, error = self.obtener_perm_concesionarios(
            #     wusername)
            # if error:
            #     wresponse["status"] = 422
            #     raise ValueError(str(error))

            wresponse["result"] = {
                "fields": fields,
                "permisos": permisos,
                "perfiles": perfiles,
                # "clientes": permisos_clientes,
                # "concesionarios": permisos_concesionarios,
            }
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

    def obtener_fields(self, username):
        result, error = None, None
        try:
            if not username is None:
                objuser = mdlauth.User.objects \
                    .filter(username__iexact=username) \
                    .select_related("userextension") \
                    .first()

                wfields = [
                    "id", "first_name", "last_name", "email",
                    "userextension", "is_active", "username",
                    "fullname",
                ]
                srlzoutcome = srlzlog.UserSerializer(
                    objuser, fields=wfields)
                result = srlzoutcome.data
            else:
                result = []
        except Exception as e:
            error = str(e)
        return [result, error]

    def obtener_perfiles(self, username):
        result, error = None, None
        try:
            if username is None:
                wuser_id, wuser_profiles = None, []
            else:
                wuser = mdlauth.User.objects.get(username__iexact=username)
                wuser_id = wuser.id
                wuser_profiles = [x.id for x in wuser.groups.all()]

            woutcome = mdlauth.Group.objects.all() \
                .annotate(user_id=Case(
                    When(id__in=wuser_profiles, then=Value(wuser_id)),
                    default=None,
                    output_field=IntegerField(),
                ))

            wfields = ["id", "name", "user_id", ]
            srlzoutcome = srlzlog.GroupSerializer(
                woutcome, fields=wfields, many=True)
            result = srlzoutcome.data
            for item in result:
                item["label"] = item.get("name")
                item["key"] = item.get("id")

        except Exception as e:
            error = str(e)
        return [result, error]

    def obtener_permisos(self, username):
        result, error = [], None
        try:
            if username is None:
                wuser_id, wuser_grants = None, []
            else:
                wuser = mdlauth.User.objects.get(username__iexact=username)
                wuser_id = wuser.id
                wuser_grants = [x.codename
                                for x in mdlauth.Permission.objects.filter(
                                    user__username__iexact=username)]

            woutcome = mdlauth.Permission.objects \
                .prefetch_related('content_type') \
                .annotate(user_id=Case(
                    When(codename__in=wuser_grants,
                         then=Value(wuser_id)),
                    default=None,
                    output_field=IntegerField(),
                )).order_by("content_type")

            wfields = [
                "id", "name", "codename", "content_type", "user_id",
            ]
            srlzoutcome = srlzlog.PermissionSerializer(
                woutcome, fields=wfields, many=True,
            )
            result = srlzoutcome.data
            for item in result:
                item["label"], item["key"] = item.get("name"), item.get("id")

        except Exception as e:
            error = str(e)
        return [result, error]

    def obtener_perm_clientes(self, username):
        result, error = [], None
        try:
            if username is None:
                wuser_id, wuser_clientes = None, []
            else:
                wuser = mdlauth.User.objects.get(username__iexact=username)
                wuser_id = wuser.id
                wuser_clientes = [x.cliente_id
                                  for x in wuser.catalogos_usercliente_user_set.all()]

            woutcome = mdlcat.Clientes.objects.all() \
                .select_related("estado") \
                .annotate(user_id=Case(
                    When(id__in=wuser_clientes, then=Value(wuser_id)),
                    default=Value(None),
                    output_field=IntegerField()
                )).order_by('-fhregistro')

            wfields = [
                "id", "razon_social", "user_id", "telefono",
            ]
            srlzoutcome = srzcat.ClientesSerializer(
                woutcome, fields=wfields, many=True,
            )
            result = srlzoutcome.data
            for item in result:
                wphone = (item.get("telefono") or '').strip()
                wname = (item.get("razon_social") or '').strip()
                item["label"] = '{} - {}'.format(
                    wname, wphone)
                item["key"] = item.get("id")

        except Exception as e:
            error = str(e)
        return [result, error]


class APIGetTipoDocumentoPago(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            outcome = mdlcat.TipoDocumentoPago.objects.filter(
                estado=mdlcat.Estados.objects.get(descripcion__exact='ACTIVO')
            )
            wfields = [
                "id", "codigo", "descripcion", "estado",
            ]
            srzoutcome = srzcat.TipoDocumentoPagoSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetEstadosInvoice(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            outcome = mdlinv.InvoiceEstados.objects \
                .filter(estado=1) \
                .select_related("estado")

            wfields = [
                "id", "descripcion", "estado",
            ]
            srzoutcome = srzinv.InvoiceEstadosSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetPaises(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            outcome = mdlcat.Paises.objects.filter(estado=1) \
                .select_related("estado")

            wfields = [
                "id", "codigo", "descripcion", "estado"
            ]
            srzoutcome = srzcat.PaisesSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetClientes(APIView):

    def get(self, request):
        wresponse = defaultResponse.copy()
        try:
            Qfilters = Q(estado=1,)
            wid = request.GET.get("id") or ''
            wfilter = request.GET.get("filter") or ''

            if wfilter != '':
                Qfilters.add(
                    Q(razon_social__icontains=wfilter) |
                    Q(telefono__icontains=wfilter),
                    Qfilters.connector,
                )
            if wid != '':
                Qfilters.add(
                    Q(id__iexact=wid), Qfilters.connector,
                )

            outcome = mdlcat.Clientes.objects.filter(Qfilters) \
                .select_related("estado") \
                .select_related("pais__estado") \
                .select_related("tdoc__estado")

            wfields = [
                "id", "razon_social",
                "direccion",
                "tdoc", "ndoc",
                "ciudad", "pais",
                "estado", "telefono",
            ]
            srzoutcome = srzcat.ClientesSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                pattern = '{} - {}'.format(
                    item.get("razon_social") or '',
                    item.get("telefono") or '',
                ).strip()
                item["key"] = item.get("id") or ''
                item["value"] = pattern
                item["label"] = pattern
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetArticulos(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            Qfilters = Q(id__isnull=False)
            wcategoria = request.GET.get("categoria") or ''
            if wcategoria != '':
                Qfilters.add(
                    Q(categoria__id=wcategoria),
                    Qfilters.connector,
                )

            outcome = mdl.Articulos.objects.filter(Qfilters) \
                .select_related("categoria__estado") \
                .select_related("estado")

            wfields = ["id", "categoria", "descripcion", "estado"]
            srzoutcome = srz.ArticulosSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetTipoDocumento(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            outcome = mdlcat.TipoDocumento.objects.all() \
                .select_related("estado")

            wfields = [
                "id", "codigo", "doc_clie",
                "documento", "estado",
            ]
            srzoutcome = srzcat.TipoDocumentoSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("doc_clie") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetCategorias(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            outcome = mdl.Categoria.objects.all() \
                .select_related("estado")

            wfields = ["id", "descripcion", "estado"]
            srzoutcome = srz.CategoriaSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetEstados(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            outcome = mdlcat.Estados.objects.filter(estado=1)
            wfields = [
                'id', 'descripcion', 'estado',
            ]
            srzoutcome = srzcat.EstadosSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


# class APISendEmailMetas(APIView):
#     defaultResponse = {
#         "ok": 1,
#         "result": 'Request has been processed successfuly.',
#         "status": 200,
#     }

#     def get(self, request):
#         wresponse = self.defaultResponse.copy()
#         try:
#             connection = MYSQLConnection()
#             wcurrent_date = (datetime.now() + relativedelta(months=-1))
#             wcmd = cmd_metas_obtener_ventas.format(wcurrent_date.month, 'null')
#             werror, wresult = connection.get_query_dict(wcmd)
#             if werror:
#                 raise ValueError(str(werror))
#             # generate xls files
#             error, wresult = self.generate_files(wresult, request)
#             if error:
#                 raise ValueError(str(error))
#             # send emails
#             error, wresult = self.send_emails(wresult)
#             if error:
#                 raise ValueError(str(error))
#             wresponse["result"] = wresult
#             return Response(wresponse, status=wresponse.get("status"))
#         except Exception as e:
#             wresponse['ok'] = 0
#             wresponse['result'] = str(e)
#             if wresponse.get('status') == 200:
#                 wresponse['status'] = 500
#             return Response(wresponse, status=wresponse.get("status"))

#     def generate_files(self, items, request):
#         error = None
#         try:
#             wstatic = os.path.join(settings.BASE_DIR, 'static', 'others')
#             wmedia = os.path.join(settings.BASE_DIR, 'media', 'metas_report')
#             wtemplate = os.path.join(wstatic, 'template_metas.xlsx')
#             for item in items:
#                 winterval = datetime.now().strftime("%Y%m%d%H%M%S%f")
#                 wfilename = 'xls_{}.xlsx'.format(winterval)
#                 xlsfile = os.path.join(wmedia, wfilename)
#                 copyfile(wtemplate, xlsfile)
#                 wb = load_workbook(xlsfile)
#                 ws = wb.active
#                 ws.title = item.get("empleado_nombres") or ''
#                 wcells = ["B2", "E4", "D18", "D19", "D20"]
#                 wvalues = [
#                     "empleado_nombres", "sueldo",
#                     "meta", "nventas", "nventas_anuladas"
#                 ]
#                 for windex, wcell in enumerate(wcells):
#                     ws[wcell].value = item.get(wvalues[windex])
#                 wb.save(xlsfile)
#                 wb.close()
#                 item["urlpath"] = request.build_absolute_uri(
#                     os.path.join(settings.MEDIA_URL, 'metas_report', wfilename)
#                 )
#         except Exception as e:
#             error = str(e)
#         return [error, items]

#     def send_emails(self, items):
#         error = None
#         try:
#             for item in items:
#                 wcontext = {
#                     'content_header': 'Metas de ventas {}'.format(item.get("empleado_nombres")),
#                     'content_body': 'Revisar reporte generado en {}.'.format(item.get("urlpath")),
#                 }
#                 wsettings = {
#                     'subject': wcontext.get("content_header"),
#                     'body': wcontext.get("content_body"),
#                     "to": list([item.get("empleado_email")], ) or list(),
#                     "template": 'mail/mail_template.html',
#                     "context": wcontext,
#                 }
#                 objMail = SendMailer(**wsettings)
#                 error, status = objMail.BuildMessage()
#                 if error:
#                     print(str(error))
#                 objMail.Send()
#         except Exception as e:
#             error = str(e)
#         return [error, items]
