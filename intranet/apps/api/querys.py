#! /usr/bin/env python
#-*- encoding : UTF-8 -*-

# SQL Server procedures
cmd_obtener_ventas = """
	exec sp_cupones_venta '{0}','{1}','','','';
"""

cmd_obtener_ventas_anuladas = """
	exec sp_cupones_anulados '{0}','{1}','','','';
"""
cmd_obtener_empleados = """
	exec sp_empleados '';  
"""

# MySQL procedures
cmd_metas_eliminar_ventas = "truncate table metas_cuponesmodel ;"
cmd_metas_obtener_ventas = """
	call sp_metas_obtener_ventas ({0},{1});
"""
cmd_metas_eliminar_empleados = "truncate table metas_empleadosmodel ;"
cmd_metas_transfer_empleados = "call sp_metas_actualizar_empleados() ;"
