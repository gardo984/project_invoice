from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.conf import settings
from apps.api import views as v
from rest_framework_swagger.views import get_swagger_view

app_name = 'module_api'

whost = None
if settings.DEBUG == False:
    whost = settings.PREFIX_URL

schema_view = get_swagger_view(
    title="API Interface for WRC",
    url=whost,
)

urlpatterns = [
    url(r'^$', schema_view, name='root'),
    url(
        r'^metas/',
        include([
            # url(r'^obtener_ventas/$', v.APIGetVentas.as_view(),
            #     name='sql_obtener_ventas'),
            # url(r'^actualizar_marcas/?$', v.APIUpdateMarcas.as_view(),
            #     name='actualizar_marcas'),
            # url(r'^actualizar_concesionarios/?$', v.APIUpdateConcesionarios.as_view(),
            #     name='actualizar_concesionarios'),
            # url(r'^enviar_emails/?$', v.APISendEmailMetas.as_view(),
            #     name='enviar_emails'),
            # url(r'^obtener_empleados/$', v.APIGetEmpleados.as_view(),
            #     name='sql_obtener_empleados'),
            # url(r'^actualizar_empleados/?$', v.APIUpdateEmpleados.as_view(),
            #     name='actualizar_empleados'),
            # url(r'^actualizar_sucursales/?$', v.APIUpdateSucursales.as_view(),
            #     name='actualizar_sucursales'),
            # url(r'^actualizar_areas/?$', v.APIUpdateArea.as_view(),
            #     name='actualizar_areas'),
            # url(r'^actualizar_puestos/?$', v.APIUpdatePuestos.as_view(),
            #     name='actualizar_puestos'),
        ]),
    ),
    url(
        r'^metas_intranet/',
        include([
            # url(r'^metas_obtener_jefes/?$',
            #     v.APIGetJefes.as_view(),
            #     name='metas_obtener_jefes'),
            # url(r'^metas_obtener_empleados/?$',
            #     v.APIMetasEmpleados.as_view(),
            #     name='metas_obtener_empleados'),
            # url(r'^metas_obtener_marcas/?$',
            #     v.APIGetMarcas.as_view(),
            #     name='metas_obtener_marcas'),
        ]),
    ),
    url(
        r'^catalogos_intranet/',
        include([
            url(r'^metas_obtener_estados/?$',
                v.APIGetEstados.as_view(),
                name='metas_obtener_estados'),
            url(r'^metas_obtener_categorias/?$',
                v.APIGetCategorias.as_view(),
                name='metas_obtener_categorias'),
            url(r'^metas_obtener_articulos/?$',
                v.APIGetArticulos.as_view(),
                name='metas_obtener_articulos'),
            url(r'^metas_obtener_tdocumento/?$',
                v.APIGetTipoDocumento.as_view(),
                name='metas_obtener_tdocumento'),
            url(r'^metas_obtener_tdocumento_pago/?$',
                v.APIGetTipoDocumentoPago.as_view(),
                name='metas_obtener_tdocumento_pago'),
            url(r'^metas_obtener_clientes/?$',
                v.APIGetClientes.as_view(),
                name='metas_obtener_clientes'),
            url(r'^metas_obtener_paises/?$',
                v.APIGetPaises.as_view(),
                name='metas_obtener_paises'),
            url(r'^invoice_obtener_estados/?$',
                v.APIGetEstadosInvoice.as_view(),
                name='invoice_obtener_estados'),
            url(r'^catalogos_obtener_usuario_permisos/?$',
                v.APIGetUsuarioPermisos.as_view(),
                name='catalogos_obtener_usuario_permisos'),
            url(r'^catalogos_obtener_perfil_permisos/?$',
                v.APIGetPerfilPermisos.as_view(),
                name='catalogos_obtener_perfil_permisos'),
            url(r'^metas_obtener_tipificaciones/?$',
                v.APIGetTipificacion.as_view(),
                name='metas_obtener_tipificaciones'),
            url(r'^metas_obtener_tipo_invoice/?$',
                v.APIGetTipoInvoice.as_view(),
                name='metas_obtener_tipo_invoice'),
            url(r'^metas_obtener_forma_pago/?$',
                v.APIGetFormaPago.as_view(),
                name='metas_obtener_forma_pago'),
            url(r'^metas_obtener_contactados/?$',
                v.APIGetContactados.as_view(),
                name='metas_obtener_contactados'),
            # added at 2019.09.02
            url(r'^metas_obtener_tipo_seguimiento/?$',
                v.APIGetTipoSeguimiento.as_view(),
                name='metas_obtener_tipo_seguimiento'),
            # added at 2019.09.19
            url(r'^metas_obtener_vendedores/?$',
                v.APIGetVendedores.as_view(),
                name='metas_obtener_vendedores'),
        ]),
    ),
]
