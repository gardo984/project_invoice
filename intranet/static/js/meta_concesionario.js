const listas = {
	list_jefes : [],
	list_marcas : [],
} ;
const vApp = new Vue({
	delimiters : ['${','}'],
	el : '#vContainer',
	data: {
		jefe : null,
		marca : null,
		list : listas,
		details : [],
		item_meta : [],
		checked_enable : false,
		checked_disable : false,
	},
	methods : {
		getParameters(){
			return {
				marca : this.marca,
				jefe : this.jefe,
				concesionarios : this.lst_items_enabled,
			} ;
		},
		enableItem(){
			if (this.lst_items_disabled.length==0){
				swal('Error','No hay registros seleccionados que habilitar.',"error")
				return false;
			} ;
			this.lst_items_disabled.forEach(function(wele,windex){
				if (wele.check==true){
					wele.asignado =true ;
					wele.check = false ;
				} ;
			}) ;
		},
		disableItem(){
			if (this.lst_items_enabled.length==0){
				swal('Error','No hay registros seleccionados que deshabilitar.',"error")
				return false;
			} ;
			this.lst_items_enabled.forEach(function(wele,windex){
				if (wele.check==true){
					wele.asignado =false ;
					wele.check = false ;
				} ;
			}) ;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			Object.assign(wparameters,{
				wtype : 'save_information',
			}) ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					this.getSearch();
				}
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response) {
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, registrando asignacion de concesionarios.',
						icon:'warning',
					});
				} ;
			}) ;
		},
		confirmDialog() {
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation() ;
						break;
				} ;
			}) ;
		},
		getJefes(){
			axios({
				method : 'get',
				url: URI_JEFES,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_jefes = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de Jefes.',
					icon:'warning',
				});
			}) ;
		},
		getMarcas(){
			axios({
				method : 'get',
				url: URI_MARCAS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_marcas = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de Marcas.',
					icon:'warning',
				});
			}) ;
		},
		getSearch(){
			let wparameters = {
				marca : this.marca,
				jefe : this.jefe,
				wtype : 'get_metas_concesionarios',
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				let outcome = response.result.filter(row=>{
					// row.asignado = false;
					row.check = false ;
					return true;
				}) ;
				this.details = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				if (err.response) {
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de Concesionarios.',
						icon:'warning',
					});
				} ;
			}) ;
		},
		check_items(wvalue,wlist){
			wlist.forEach(function(wele,windex){
				wele.check=wvalue ;
			}) ;
		},
	},
	watch : {
		'checked_enable': function(wvalue){
			this.check_items(wvalue,this.lst_items_enabled)
		},
		'checked_disable': function(wvalue){
			this.check_items(wvalue,this.lst_items_disabled)
		},
	},
	computed : {
		btn_status(){
			if (this.details.length>0 
					&& this.marca
					&& this.jefe ) {
				return false ;
			}else{
				return true ;
			} ;
		},
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
		},
		lst_items_disabled(){
			return this.details.filter(row=>{
				return row.asignado == false ;
			})
		},
		lst_items_enabled(){
			return this.details.filter(row=>{
				return row.asignado == true ;
			})
		},
		totales(){
			return  {
				filas :this.lst_items.length,
				habilitados: this.lst_items_enabled.length,
				nohabilitados:this.lst_items_disabled.length,
			}
		},
	},
	mounted(){
		this.getJefes();
		this.getMarcas();
	},
}) ;