

const listas = {
	list_estados : [],
	list_tdocumentos : [],
	list_paises : [],
	list_tipificaciones: [],
	list_contactado: [],
	page : {
		page_size:0,
		page_total:0,
		page_count:7,
		page_current:1,
	},
} ;

const vApp = new Vue({
	delimiters : ['${','}'],
	el:"#vContainer",
	data : {
		estado:null,
		tipificacion:null,
		details : [],
		list : listas,
		item_cliente: [],
		cliente : null,
		dialog_status: true,
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods:{
		handleReportXls(){
			if(this.lst_items.length==0) return false;
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_xls_report',
			})
			axios({
				method : 'post',
				url:URI_HOME,
				data : wparameters,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				if (response.ok){
					swal({
						title:'Aviso',
						text:response.result.message,
						closeOnClickOutside:false,
						closeOnEsc:false,
						icon : 'success',
					}).then(rsp=>{
						window.open(response.result.urlpath,'_blank')
					})
				};
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let defaultmsg = 'Error, procesando exportado de clientes.';
				swal({
					title:wmessage,
					text: defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleImportFile(){
			this.dialog_status = false;
			vUpload.dialog_status = true ;
			vUpload.handleGetFiles();
		},
		handleAddCustomerByPhone(){
			if(!this.cliente){
				swal("Error",'Cliente/Teléfono no puede estar vacio.','error');
				return false;
			} ;
			let wparameters = {
				wtype : 'add_customer_by_phone',
				telefono : this.cliente,
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					vApp.getCustomers();
				}
			}).catch( err=> {
				console.log(err.response) ;
				let rsp = err.response
				let wmessage = rsp.status+ ' ' +rsp.statusText ;
				let  defaultmsg='Error, agregando cliente por telefono.';
				swal({
					title:wmessage,
					text: rsp.data.result||defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		removeProcess(){
			let wparameters = {
				wtype : 'remove_catalogos_customer',
				item : this.item_cliente,
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					this.item_cliente = [],
					swal('Aviso',response.result.message,"success");
					vApp.getCustomers();
				}
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, eliminando cliente.',
						icon:'warning',
					});
				}
			}) ;
		},
		handleCleanFields(){
			let lsvalues=[null,null,null,] ;
			let lsfields=["estado","tipificacion","cliente",] ;
			for (x in lsvalues){
				this[lsfields[x]] = lsvalues[x];
			}
		},	
		handleModifyCustomer(item){
			vModal.dialog_status = true;
			this.item_cliente = item;
			this.setValues()
		},
		handleRemoveCustomer(item){
			this.item_cliente = item;
			let wmessage  = `Se procederá con el eliminado de cliente: ${this.item_cliente.razon_social}, ¿Desea Continuar?`
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.removeProcess();
						break;
				} ;
			}) ;
		},
		handleChangePage(npage){
			this.list.page.page_current = npage;
			this.handleSearch();
		},
		handleSearch(){
			this.getCustomers();
		},
		handleAddCustomer(){
			if (vModal.errors.length>0){
				vModal.errors = [];
			} ;
			vModal.dialog_status = true;
			vModal.estado = 1
		},
		getParameters(){
			return {
				cliente : this.cliente,
				tipificacion : this.tipificacion,
				estado : this.estado,
				npage : this.list.page.page_current,
			} ;
		},
		getCustomers(){
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_catalogos_customers',
			})
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result.rows;
				this.list.page.page_total = response.result.page.total;
				this.list.page.page_size = response.result.page.size;
				this.list.page.page_count = response.result.page.count;
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo lista de clientes.',
					icon:'warning',
				});
			}) ;
		},
		getTipoDocumento(){
			axios({
				method : 'get',
				url: URI_TDOCUMENTO,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_tdocumentos = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de t.documentos.',
					icon:'warning',
				});
			}) ;
		},
		getTipificacion(){
			axios({
				method : 'get',
				url: URI_TIPIFICACION,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_tipificaciones = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de tipificaciones.',
					icon:'warning',
				});
			}) ;
		},
		getContactados(){
			axios({
				method : 'get',
				url: URI_CONTACTADOS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_contactado = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de contactados.',
					icon:'warning',
				});
			}) ;
		},
		getPaises(){
			axios({
				method : 'get',
				url: URI_PAISES,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_paises = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de paises.',
					icon:'warning',
				});
			}) ;
		},
		getEstados(){
			axios({
				method : 'get',
				url: URI_ESTADO,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_estados = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de estados.',
					icon:'warning',
				});
			}) ;
		},
		setValues(){
			if(this.item_cliente){
				vModal.id = this.item_cliente.id ;
				vModal.razon_social = this.item_cliente.razon_social ;
				vModal.direccion = this.item_cliente.direccion ;
				vModal.tdoc = this.item_cliente.tdoc?this.item_cliente.tdoc.id:null ;
				vModal.ndoc = this.item_cliente.ndoc ;
				vModal.telefono = this.item_cliente.telefono ;
				vModal.celular = this.item_cliente.celular ;
				vModal.email = this.item_cliente.email ;
				vModal.contacto = this.item_cliente.contacto ;
				vModal.observaciones = this.item_cliente.observaciones ;
				vModal.estado = this.item_cliente.estado?this.item_cliente.estado.id:2;
				vModal.ciudad = this.item_cliente.ciudad;
				vModal.pais = this.item_cliente.pais?this.item_cliente.pais.id:null ;
				vModal.tipificacion = this.item_cliente.tipificacion?this.item_cliente.tipificacion.id:null;
				vModal.contactado = this.item_cliente.contactado?this.item_cliente.contactado.id:null;
			} ;
		},
		
	},
	computed:{
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true ;
				} catch (ex) {
					return false;
				}
			})
		},
		totales(){
			return {
				filas : this.lst_items.length,
			} ;
		},
		styleData(){
			return this.dialog_status?'appear':'dissapear';
		},
	},
	mounted(){
		this.getEstados();
		this.getTipoDocumento()
		this.getCustomers();
		this.getPaises();
		this.getTipificacion();
		this.getContactados();
	},
}) ;

const vModal = new Vue({
	el:"#vModal",
	data : {
		id:null,
		dialog_status:false,
		razon_social :null,
		direccion :null,
		tdoc :null,
		ndoc :null,
		telefono :null,
		celular :null,
		email :null,
		contacto :null,
		observaciones :null,
		ciudad :null,
		pais:null,
		estado :null,
		tipificacion :null,
		contactado:null,
		list : listas,
		errors : [],
		show_all: all_flds,
	},
	methods : {
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
		validatingFields(){
			let wparameters = this.getParameters();
			// if(!wparameters.razon_social) {
			// 	swal("Error",'Ingrese razon social','error');
			// 	return false;
			// } ;
			// if(!wparameters.direccion) {
			// 	swal("Error",'Ingrese direccion','error');
			// 	return false;
			// } ;
			// if(!wparameters.tdoc) {
			// 	swal("Error",'Ingrese tipo documento','error');
			// 	return false;
			// } ;
			// if(!wparameters.ndoc) {
			// 	swal("Error",'Seleccione nro. documento','error');
			// 	return false;
			// } ;
			if(!wparameters.telefono) {
				swal("Error",'Ingrese nro. telefono','error');
				return false;
			} ;
			if(!wparameters.estado) {
				swal("Error",'Seleccione Estado','error');
				return false;
			} ;
			return true;
		},
		getParameters(){
			return {
				id : vApp.item_cliente.id ? vApp.item_cliente.id:null,
				razon_social : this.razon_social,
				direccion : this.direccion,
				tdoc : this.tdoc,
				ndoc : this.ndoc,
				telefono : this.telefono,
				celular : this.celular,
				email : this.email,
				contacto : this.contacto,
				observaciones : this.observaciones,
				estado: this.estado,
				tipificacion: this.tipificacion,
				contactado: this.contactado,
				ciudad: this.ciudad,
				pais: this.pais,
				wtype : vApp.item_cliente.id ?'update_catalogos_customer':'add_catalogos_customer',
			} ;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					vApp.getCustomers();
					this.handleCloseDialog()
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let default_msg = 'Error, registrando articulo.';
				swal({
					title:wmessage,
					text: err.response.data.result||default_msg,
					icon:'warning',
				});
			}) ;
		},
		cleanFields(){
			let wfields = [
				'id','razon_social',"direccion",
				'tdoc','ndoc',"telefono",
				'celular','email',"contacto",
				'observaciones','estado',
				'errors','pais','ciudad',
				'tipificacion','contactado',
			] ;
			let wvalues = [
				null,null,null,null,
				null,null,null,null,
				null,null,null,[],null,null,
				null,null,
			] ;
			for(item in wfields){
				this[wfields[item]] = wvalues[item];
			};
			if(vApp.item_cliente){
				vApp.item_cliente= [];
			} ;
		},
		handleCloseDialog(){
			this.dialog_status = false;
			this.cleanFields();
		},
		handleSaveCustomer(){
			if(this.areThereErrors) {
				swal('Aviso','Hay campos por completar.','error')
				return false ;
			} ;
			// if(!this.validatingFields()) return false;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation() ;
						break;
				} ;
			}) ;
		},
		handleBlur(e){
			if(e.target) {
				this.validatingValue(e.target.name,e.target.value) ;
				return ;
			} ;
			if(e.name) {
				this.validatingValue(e.name,e.value) ;
				return ;
			} ;
		}
	},
	watch : {
		// 'razon_social': function(wnew, wold){
		// 	this.validatingValue('razon_social',wnew) ;
		// },
		'telefono': function(wnew, wold){
			this.validatingValue('telefono',wnew) ;
		},
		// 'direccion': function(wnew, wold){
		// 	this.validatingValue('direccion',wnew) ;
		// },
		// 'tdoc': function(wnew, wold){
		// 	this.validatingValue('tdoc',wnew) ;
		// },
		// 'ndoc': function(wnew, wold){
		// 	this.validatingValue('ndoc',wnew) ;
		// },
		'estado': function(wnew, wold){
			this.validatingValue('estado',wnew) ;
		},
	},
	computed : {
		areThereErrors(){
			// let wfields = [
			// 	"razon_social","direccion","tdoc",
			// 	"ndoc",'estado',
			// ]
			let wfields = ["telefono","estado",]
			for(item in wfields){
				this.validatingValue(wfields[item],this[wfields[item]])
			}
			return this.errors.length>0?true:false;
		},
	},
}) ;

const vUpload = new Vue({
	delimiters : ['${','}'],
	el:"#vModalUpload",
	data : {
		dialog_status:false,
		file :null,
		list : listas,
		fileList : [],
		details : [],
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods : {
		handleExceed(){
			console.log("exceed in quantity of files to upload!!")
		},
		handleGetFiles(){
			let wparameters = {
				wtype:'get_list_files',
			}
			axios({
				method : 'post',
				url: URI_UPLOAD,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result;
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo archivos importados.',
					icon:'warning',
				});
			}) ;
		},
		handleUploadFile(){
			let details = this.$refs.upload.uploadFiles;
			if(details.length==0){
				swal('Error','No hay archivos que procesar','error');
				return false;
			}
			let frmData = new FormData();
			for (x in details){
				let wposition = (parseInt(x)+1).toString();
				frmData.append(`${details[x].name}`,details[x].raw);
			};
			frmData.append('details',JSON.stringify(details));

			axios({
				method : 'post',
				url: URI_UPLOAD,
				data : frmData,
				responseType : 'json',
				headers: {
					'Content-Type':'multipart/form-data',
				},
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result,"success");
					this.handleGetFiles();
					this.cleanFields();
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let default_msg = 'Error, procesando importacion de clientes.'
				swal({
					title:wmessage,
					text: err.response.data.result|| default_msg,
					icon:'warning',
				});
			}) ;
		},
		handleReturnBack(){
			this.dialog_status = false;
			vApp.dialog_status = true ;
			vApp.handleSearch();
		},
		processRemoveFile(hashid){
			let wparameters = {
				wtype:'remove_file',
				hashid: hashid,
			}
			axios({
				method : 'post',
				url: URI_UPLOAD,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result,"success");
					this.handleGetFiles();
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let default_msg = 'Error, procesando eliminado de archivo.';
				swal({
					title:wmessage,
					text: err.response.data.result|| default_msg,
					icon:'warning',
				});
			}) ;
		},
		handleRemoveFile(file){
			swal({
				title : 'Confirmación',
				text:`Se procederá el eliminado del archivo '${file.file_name}', ¿Desea continuar?.`,
				buttons : {
					yes : "Si",
					no : "No",
				},
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.processRemoveFile(file.hashid) ;
						break;
				} ;
			}) ;
		},
		handleDownloadFile(file){
			window.open(file,'_blank');
		},
		cleanFields(){
			vUpload.$refs.upload.clearFiles() ;
		},
		handleCloseDialog(){
			this.dialog_status = false;
			this.cleanFields();
		},
		handleSaveArticle(){
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation() ;
						break;
				} ;
			}) ;
		},
	},
	computed : {
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true ;
				} catch (ex) {
					return false;
				}
				return true ;
			});
		},
		styleModal(){
			return this.dialog_status?'appear':'dissapear';
		},
	},
}) ;