
const listas = {
	list_estados : [],
	list_accesos:[],
	page : {
		page_size:0,
		page_total:0,
		page_count:7,
		page_current:1,
	},
} ;
const vApp = new Vue({
	delimiters : ['${','}'],
	el : '#vContainer',
	data: {
		descripcion:null,
		estado :null,
		list : listas,
		details : [],
		item : [],
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods : {
		handleClean(){
			let wfields = [ 'descripcion','estado', ] ;
			let wvalues = [ null,null, ] ;
			for(item in wfields){
				this[wfields[item]] = wvalues[item];
			};
		},
		getParameters(){
			return {
				descripcion : this.descripcion || null,
				estado : this.estado || null,
				npage : this.list.page.page_current,
			} ;
		},
		getEstados(){
			axios({
				method : 'get',
				url: URI_ESTADO,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_estados = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de estados.',
					icon:'warning',
				});
			}) ;
		},
		removeProcess(){
			let wparameters = { 
				id: this.item.id,
			} ;
			axios({
				method : 'post',
				url: URI_REMOVE,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					this.item = [],
					this.handleSearch();
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let response  = err.response ;
				let defaultmsg = 'Error, procesando eliminacion perfil.';
				swal({
					title:wmessage,
					text: response.data?response.data.result:defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleModify(item){
			vApp.item = item ;
			vModal.dialog_status=true ;
			vModal.setDefaultValues();
		},
		handleRemove(item){
			this.item = item;
			let wmessage  = `Se procederá con el eliminado de perfil : ${this.item.name}, ¿Desea Continuar?`
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.removeProcess();
						break;
				} ;
			}) ;
		},
		handleChangePage(npage){
			this.list.page.page_current = npage;
			this.handleSearch();
		},
		handleSearch(){
			let wparameters = this.getParameters()
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result.rows ;
				this.list.page.page_total = response.result.page.total;
				this.list.page.page_size = response.result.page.size;
				this.list.page.page_count = response.result.page.count;
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de perfil.',
						icon:'warning',
					});
				}
			}) 
		},
		handleAddItem(){
			vModal.dialog_status=true ;
			// vModal.estado=1;
			vModal.getProfileGrants();
		},
	},
	computed : {
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true;
				} catch (ex){
					return false;
				}
				return ;
			});
		},
		totales(){
			return {
				filas: this.lst_items.length,
			} ;
		},
	},
	mounted(){
		this.getEstados();
		this.handleSearch();
	},
}) ;

const fields = {
	id:null,
	name:null,
	estado:null,
	accesos : [],
}
const vModal = new Vue({
	delimiters : ['${','}'],
	el: '#vModal',
	data : {
		dialog_status : false,
		form : fields,
		list : listas,
		errors:[],
	},
	methods : {
		handleProfilesFilter(query, item) {
         	return item.label.toLowerCase().indexOf(query.toLowerCase()) > -1;
        },
		setDefaultValues(){
			if(vApp.item.id){
				this.form.id = vApp.item.id ;
				this.form.name = vApp.item.name ;
				let wstatus = vApp.item.groupextension?vApp.item.groupextension.is_active:0;
				this.form.estado = (wstatus==1?1:2) ;
				this.getProfileGrants();
			}
		},
		setGrantValues(){
			this.list.list_accesos.forEach((obj,index)=>{
				if(obj.group_id!=null){
					vModal.form.accesos.push(obj.id) ;
				}
			}) ;
		},
		getParameters(){
			return {
				id : this.form.id ? this.form.id:null,
				name : this.form.name,
				estado : this.form.estado,
				permissions :  this.form.accesos,
			} ;
		},
		getProfileGrants(){
			axios({
				method : 'get',
				url: URI_GRANTS,
				params : {uid:this.form.id,},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_accesos = response.result.permisos;
				this.setGrantValues();
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo datos de usuario.',
					icon:'warning',
				});
			}) ;
		},
		cleanFields(){
			cleanObject(this.form) ;
			this.errors = [];
			this.list.list_accesos = [];
			this.$refs.elt_permisos.clearQuery("left");
			this.$refs.elt_permisos.clearQuery("right");
			if(vApp.item){
				vApp.item= [];
			} ;
		},
		handleCloseDialog() {
			this.dialog_status = false ;
			this.cleanFields();
		},
		handleCancelDialog() {
			this.handleCloseDialog() ;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			let wpath = !this.form.id?URI_APPEND:URI_UPDATE;
			axios({
				method : 'post',
				url: wpath,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					vApp.handleSearch();
					this.handleCloseDialog()
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let response = err.response ;
				let defaultmsg = 'Error, procesando guardado de datos perfil.';
				swal({
					title:wmessage,
					text: response.data? response.data.result:defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleConfirmDialog() {
			if(this.areThereErrors){
				swal('Aviso','Hay campos por completar.','error') ;
				return false;
			} ;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation();
						break;
				} ;
			}) ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
		handleBlur(e){
			if(e.target) {
				this.validatingValue(e.target.name,e.target.value) ;
				return ;
			} ;
			if(e.name) {
				this.validatingValue(e.name,e.value) ;
				return ;
			} ;
		}
	},
	computed:{
		areThereErrors(){
			let wfields = [ 'name','estado',]
			for(item in wfields){
				this.validatingValue(wfields[item],this.form[wfields[item]])
			}
			return this.errors.length>0?true:false;
		},
	},
	watch:{
		'form.name' : function(wnew,wold){
			this.validatingValue('name',wnew) ;
		},
		'form.estado' : function(wnew,wold){
			this.validatingValue('estado',wnew) ;
		},
	},
}) ;