

const listas = {
	list_estados : [],
	list_tdocumento : [],
	list_fpago:[],
	list_clientes : [],
	list_categorias : [],
	list_articulos : [],
	list_tinvoice:[],
	list_vendedores:[],
	page : {
		page_size:0,
		page_total:0,
		page_count:7,
		page_current:1,
	},
} ;

const detail_item = {
	item : null,
	article : {
		id :null,
		descripcion : null,
		categoria : {
			id:null,
			descripcion:null,
		}
	},
	cantidad : null,
	subtotal : null,
}

const wrequests = { 
	delimiters : ['${','}'],
	name :"wrequests",
	template : '#container-requests',
	data(){
		return {
			invoice : null,
			cliente :null,
			fecha_desde : null,
			fecha_hasta : null,
			sub_tipo_invoice:null,
			list : listas,
			details : [],
			item_invoice : [],
			errors : [],
		}
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods: {
		handleChangePage(npage){
			this.list.page.page_current = npage;
			this.handleSearch();
		},
		handleExportAll(){
			if(this.lst_items.length==0) return false;
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_xls_report',

			}) ;
			axios({
				method : 'post',
				url:URI_HOME,
				data : wparameters,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				if (response.ok){
					swal({
						title:'Aviso',
						text:response.result.message,
						closeOnClickOutside:false,
						closeOnEsc:false,
						icon : 'success',
					}).then(rsp=>{
						window.open(response.result.urlpath,'_blank')
					})
				};
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let defaultmsg = 'Error, procesando exportado de invoices.';
				swal({
					title:wmessage,
					text: defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleCleanFields(){
			let wdate_to = new Date();
			let wdate_from = wdate_to.addDays(wdate_to,-7) ;
			wdate_from = moment(wdate_from).format("Y-MM-D") 
			wdate_to = moment(wdate_to).format("Y-MM-D") 
			let witems = ['invoice','cliente','fecha_desde','fecha_hasta','sub_tipo_invoice'] ;
			let wvalues = [null,null,wdate_from,wdate_to,null] ;
			for(item in witems) {
				this[witems[item]] = wvalues[item] ;
			} ;
		},
		getParameters(){
			return {
				invoice : this.invoice,
				cliente : this.cliente,
				fecha_desde : this.fecha_desde,
				fecha_hasta : this.fecha_hasta,
				sub_tipo_invoice : this.sub_tipo_invoice,
				npage : this.list.page.page_current,
			} ;
		},
		handleNew(){
			this.$emit('change-page','/order') ;
		},
		handleSearch(){
			if(this.areThereErrors) {
				swal('Aviso','Hay campos por completar.','error')
				return false ;
			} ;
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_invoices',
			})
			axios({
				method : 'post',
				url:URI_HOME,
				data : wparameters,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result.rows;
				this.list.page.page_total = response.result.page.total;
				this.list.page.page_size = response.result.page.size;
				this.list.page.page_count = response.result.page.count;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de invoices.',
					icon:'warning',
				});
			}) ;
		},
		modifyInvoice(item){
			this.$emit('change-page','/order',item) ;
		},
		removeInvoice(item){
			this.item_invoice = item;
			let wmessage  = `Se procederá con el eliminado ` +
				 `del invoice nro. ${item.id}, ¿Desea Continuar?` ;
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.removeProcess();
						break;
				} ;
			}) ;
		},
		removeProcess(){
			let wparameters = {
				wtype : 'remove_invoice',
				item : this.item_invoice,
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					this.item_feriado = [],
					swal('Aviso',response.result.message,"success");
					this.handleSearch();
				}
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, eliminando reservas.',
						icon:'warning',
					});
				}
			}) ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
		StyleRowStatus(item){
			let wclass = '' ;
			if(!item.sub_tipo_invoice) return false;
			switch(item.sub_tipo_invoice.descripcion){
				case 'ENTREGADO':
						wclass = 'flags green'
					break;
				case 'HOLD':
						wclass = 'flags yellow'
					break;
				case 'CANCELADO':
						wclass = 'flags red'
					break;
				case 'CONFIRMADO':
						wclass = 'flags blue'
					break;
				case 'RECHAZADO':
						wclass = 'flags orange'
					break;
			}
			return wclass ;
		}
	},
	computed: {
		areThereErrors(){
			return this.errors.length>0?true:false;
		},
		totales(){
			return {
				filas : this.lst_items.length,
			} ;
		},
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
		},
	},
	watch : {
		'fecha_desde': function(wnew, wold){
			this.validatingValue('fecha_desde',wnew) ;
		},
		'fecha_hasta': function(wnew, wold){
			this.validatingValue('fecha_hasta',wnew) ;
		},
	},
	mounted(){
		this.handleCleanFields() ;
		this.handleSearch();
	},
}

const wreserva ={ 
	delimiters : ['${','}'],
	name :"wreserva",
	template : '#container-form',
	data(){
		return {
			cliente: null,
			fecha_emision:null,
			fecha_venta:null,
			fecha_entrega:null,
			list : listas,
			details : [],
			remote_cliente: {
				loading:false,
				list : [],
			},
			item_invoice : [],
			errors : [],
			telefono :null,
			telefono2 :null,
			ciudad :null,
			direccion :null,
			observaciones :null,
			estado : null,
			forma_pago :null,
			tipo_invoice:null,
			order : null,
			nro_tarjeta:null,
			hora_entrega:null,
			labora:null,
			observaciones2:null,
			contesta_celular:null,
			vendedor:null,
			tarjeta_cvv:null,
			tarjeta_vigencia:null,
			fecha_etd_courier:null,
			fecha_recepcion:null,
			hora_entrega_hasta:null,
			delivery_address:null,
			child : detail_item,
			range_time: {
				start: '08:30',
				step: '00:15',
				end: '18:30'
			},
		}
	},
	methods : {
		getArticulos(){
			if (!this.child.article.categoria.id){
				return false;
			} ;
			axios({
				method : 'get',
				url: URI_ARTICULOS,
				params : { 
					categoria: this.child.article.categoria.id,
				},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_articulos = response.result;
			}).catch( err=> {
				if (err.response) {
					console.log(err.response.data) ;
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de articulos.',
						icon:'warning',
					});
				}
			}) ;
		},
		StyleRowStatus(wposition,witem){
			if(witem.reserva_id) return 'box-busy' ;
			return (wposition % 2==0)?'box-even':'box-odd';
		},
		handleExportInvoice(){
			console.log("xd") ;
			let wparameters = {
				uid: this.item_invoice.id?this.item_invoice.id:null,
				wtype: 'get_xls_invoice',
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal({
						title:'Aviso',
						text: response.result.message,
						icon :"success",
						closeOnClickOutside:false,
						closeOnEsc:false,
					}).then(rsp=>{
						window.open(response.result.urlpath,'_blank');
					});
				}
			}).catch( err=> {
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let defaultmsg = 'Error, procesando guardado de datos en invoice.';
				swal({
					title:wmessage,
					text: err.response.data.result||defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleChangeCantidad(){

		},
		handleAddArticle(){
			if (!this.child.article.categoria.id){
				swal('Error','Seleccione categoria a agregar.','error') ;
				return false;
			} ;
			if (!this.child.article.id){
				swal('Error','Seleccione articulo a agregar.','error') ;
				return false;
			} ;
			if(this.child.article.id){
				let witem = this.child.article.id ;
				let warticle = this.list.list_articulos.filter(row=> {
					return row.id ==  witem ;
				}) ;
				let wcantidad = parseInt(this.child.cantidad||1);
				let wsubtotal = parseFloat(this.child.subtotal||0);
				if (warticle.length>0) {
					this.details.push({
						check : false,
						item : (this.details.length+1),
						article : {
							id : warticle[0].id,
							descripcion : warticle[0].value,
							categoria : {
								id : warticle[0].categoria.id,
								descripcion : warticle[0].categoria.descripcion,
							},
						},
						cantidad : this.child.cantidad,
						subtotal : parseFloat(this.child.subtotal).toFixed(2),
						total : parseFloat((wcantidad*wsubtotal)).toFixed(2),
						visible : true,
					}) ;
					this.handleCleanDetailFields();
				}
			} ;
		},
		handleUpdateArticle(){
			let witem = this.child ;;
			let warticle = this.list.list_articulos.filter(row=> {
				return row.id ==  witem.article.id ;
			}) ;
			let wcantidad = parseInt(this.child.cantidad||1);
			let wsubtotal = parseFloat(this.child.subtotal||0);
			this.details.forEach(function(wobj,windex){
				if(wobj.item == witem.item){
					wobj.article.id = warticle[0].id ;
					wobj.article.descripcion = warticle[0].descripcion ;
					wobj.article.categoria.id = warticle[0].categoria.id ;
					wobj.article.categoria.descripcion = warticle[0].categoria.descripcion ;
					wobj.cantidad = wcantidad ;
					wobj.subtotal = wsubtotal ;
					wobj.total = parseFloat((wcantidad*wsubtotal)).toFixed(2) ;
					cleanObject(witem) ;
					return ;
				} ;
			}) ;

		},
		handleCancelArticleChanges(){
			this.handleCleanDetailFields() ;
		},
		handleCleanDetailFields(){
			cleanObject(this.child) ;
		},
		fixIndexDetails(){
			let x =0 ;
			for(wposition in this.lst_items){
				x+=1;
				this.lst_items[wposition].item = x ;
			} ;
		},
		handleModifyArticle(item){
			if(item.item){
				this.child.item = item.item ;
				this.child.article.id = item.article.id ;
				this.child.article.categoria.id = item.article.categoria.id ;
				this.child.cantidad = item.cantidad ;
				this.child.subtotal = item.subtotal ;
			}
		},
		handleRemoveArticle(item){
			if(item){
				let wmessage = `Se procederá con la eliminación de ${item.article.descripcion}, ¿Desea continuar?` ;
				swal({
					title : 'Confirmación',
					text : wmessage,
					buttons : {
						yes: 'Si',
						no: 'No',
					},
					icon : 'info',
				}).then(rsp=>{
					switch(rsp){
						case 'yes' :
								this.lst_items.forEach((wobj,windex)=> {
									if(wobj.item == item.item){
										wobj.visible = false ;
										return ;
									} ;
									this.fixIndexDetails();
								}) ;
							break;
					}
				}) ;
			} ;
		},
		getClients(wparams){
			axios({
				method : 'get',
				url: URI_CLIENTES,
				params : wparams,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.remote_cliente.list = response.result;
				// this.list.list_clientes = response.result;
				// return response.result;
			}).catch( err=> {
				if (err.response) {
					console.log(err.response.data) ;
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de Clientes.',
						icon:'warning',
					});
				}
			}) ;
		},
		searchCliente(query){
			 if (query !== '') {
		          this.remote_cliente.loading = true;
		          setTimeout(() => {
		            this.remote_cliente.loading = false;
		            let params = { filter:query,} ;
		            this.getClients(params)
		            // this.remote_cliente.list = this.getClient(query) ;
		            // console.log(this.remote_cliente.list) ;
		            // this.remote_cliente.list = this.list.list_clientes.filter(item => {
		            //   return item.value.toLowerCase()
		            //     	.indexOf(query.toLowerCase()) > -1;
		            // });
		          }, 2000);
	        } else {
	         	 this.remote_cliente.list = [];
	        }
		},
		getParameters(){
			return  {
				 id: this.item_invoice.id ?this.item_invoice.id:null,
				 order: this.item_invoice.id ?this.item_invoice.order:null,
				 cliente : this.cliente?this.cliente:null,
				 fecha_emision : this.fecha_venta?moment(this.fecha_venta).format('Y-MM-D'):null,
				 fecha_venta : this.fecha_venta?moment(this.fecha_venta).format('Y-MM-D'):null,
				 fecha_entrega : this.fecha_entrega?moment(this.fecha_entrega).format('Y-MM-D'):null,
				 cliente_phone : this.telefono,
				 cliente_phone2 : this.telefono2,
				 cliente_country	 : this.ciudad,
				 cliente_address : this.direccion,
				 observaciones : this.observaciones,
				 subtotal: this.summary.subtotal,
				 total: this.summary.total,
				 estado: this.estado,
				 order : this.order,
				 forma_pago : this.forma_pago,
				 tipo_invoice : this.tipo_invoice,
				 nro_tarjeta: this.nro_tarjeta,
				 articulos : this.lst_items.length>0 ?this.lst_items:[],
				 observaciones2 : this.observaciones2,
				 contesta_celular : this.contesta_celular,
				 labora : this.labora,
				 hora_entrega : this.hora_entrega?this.hora_entrega:null,
				 vendedor : this.vendedor,
				 tarjeta_cvv : this.tarjeta_cvv,
				 tarjeta_vigencia : this.tarjeta_vigencia,
				 fecha_recepcion : this.fecha_recepcion?moment(this.fecha_recepcion).format('Y-MM-D'):null,
				 fecha_etd_courier : this.fecha_etd_courier?moment(this.fecha_etd_courier).format('Y-MM-D'):null,
				 hora_entrega_hasta : this.hora_entrega_hasta?this.hora_entrega_hasta:null,
				 delivery_address : this.delivery_address,
				 wtype : this.item_invoice.id ?'update_invoice':'add_invoice',
			} ;
		},
		validatingFields(){
			let wparameters = this.getParameters();
			if(!wparameters.cliente) {
				swal("Error",'Ingrese cliente','error');
				return false;
			} ;
			if(!wparameters.fecha) {
				swal("Error",'Seleccione fecha','error');
				return false;
			} ;
			if(!wparameters.telefono) {
				swal("Error",'Ingrese nro. telefono','error');
				return false;
			} ;
			if(!wparameters.direccion) {
				swal("Error",'Ingrese direccion.','error');
				return false;
			} ;
			if(!wparameters.ciudad) {
				swal("Error",'Ingrese ciudad','error');
				return false;
			} ;
			
			return true;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					// swal('Aviso',response.result.message,"success");
					swal({
						title:'Aviso',
						text:response.result.message,
						icon:'success',
						closeOnClickOutside:false,
						closeOnEsc:false,
					}).then(rsp=>{
						if (!this.item_invoice.id){
							this.item_invoice = {
								id:response.result.id,
							};
						} ;
						this.$emit('close-dialog') ;
					});
				}
			}).catch( err=> {
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let defaultmsg = 'Error, procesando guardado de datos en invoice.';
				swal({
					title:wmessage,
					text: err.response.data.result||defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleConfirmDialog(){
			if(this.areThereErrors){
				swal('Aviso','Hay campos por completar.','error') ;
				return false;
			} ;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
				icon : 'info'
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation() ;
						break;
				} ;
			}) ;
		},
		handleCancelOperation(){
			let wmessage  = `Se cancelaran los cambios ingresados, ¿Desea Continuar?` ;
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
				icon:'warning',
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.$emit('close-dialog') ;
						break;
				} ;
			}) ;
		},
		setDefaultValues(){
			// this.fecha_emision = new Date();
			this.fecha_venta = new Date();
		},
		setValues(){
			if(this.item_invoice){
				this.id = this.item_invoice.id;
				this.order = this.item_invoice.order;
				let wcliente =  this.item_invoice.cliente.id ;
				// this.remote_cliente.list = this.list.list_clientes.filter(item => {
	   //            	return item.id == wcliente;
	   //          });
	   			let params = {id:wcliente,} ;
	   			this.getClients(params);
	   			setTimeout(() => {
					this.cliente = wcliente;
	   			}, 1000) ;
	            this.telefono = this.item_invoice.cliente_phone;
	            this.telefono2 = this.item_invoice.cliente_phone2;
				this.direccion = this.item_invoice.cliente_address;
				this.ciudad = this.item_invoice.cliente_country;
				this.observaciones = this.item_invoice.observaciones;
				this.fecha_emision = this.item_invoice.fecha_venta;
				this.fecha_venta = this.item_invoice.fecha_venta ;
				this.fecha_entrega = this.item_invoice.fecha_entrega ;
				this.estado = this.item_invoice.estado?this.item_invoice.estado.id:null;
				// this.order = this.item_invoice.order ;
				this.tipo_invoice = this.item_invoice.tipo_invoice?this.item_invoice.tipo_invoice.id:null;
				this.forma_pago = this.item_invoice.forma_pago?this.item_invoice.forma_pago.id:null;
				this.nro_tarjeta = this.item_invoice.nro_tarjeta ;
				this.labora = this.item_invoice.labora;
				this.contesta_celular = this.item_invoice.contesta_celular;
				this.observaciones2 = this.item_invoice.observaciones2;

				if(this.item_invoice.fecha_entrega!=null){
					let whora_entrega =` ${this.item_invoice.fecha_entrega} ${this.item_invoice.hora_entrega}`;
					this.hora_entrega = moment(whora_entrega,"Y-MM-D H:mm:ss").format("H:mm");
					let whora_entrega_hasta =` ${this.item_invoice.fecha_entrega} ${this.item_invoice.hora_entrega_hasta}`;
					this.hora_entrega_hasta = moment(whora_entrega_hasta,"Y-MM-D H:mm:ss").format("H:mm");
				}
				
				this.vendedor = this.item_invoice.vendedor?this.item_invoice.vendedor.id:null;
				this.fecha_etd_courier = this.item_invoice.fecha_etd_courier?this.item_invoice.fecha_etd_courier:null ;
				this.fecha_recepcion = this.item_invoice.fecha_recepcion?this.item_invoice.fecha_recepcion:null ;
				this.tarjeta_vigencia = this.item_invoice.tarjeta_vigencia;
				this.tarjeta_cvv = this.item_invoice.tarjeta_cvv;
				this.delivery_address = this.item_invoice.delivery_address;
			} ;
		},
		getInvoice(wid){
			let wparameters = {
				id:wid, 
				wtype : 'get_invoice_item',
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers : axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.item_invoice = response.result;
				this.details = this.item_invoice.invoicedetails_set ;
				this.setValues();
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo reserva.',
					icon:'warning',
				});
			}) ;	
		},
		validatingStatus(){
			let wparams = this.$route.params ;
			if(Object.keys(wparams).length>0){
				this.getInvoice(wparams.uid);
			}else{
				this.setDefaultValues();
				this.$refs.cliente.focus() ;
			} ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
		handleBlur(e){
			if(e.target) {
				this.validatingValue(e.target.name,e.target.value) ;
				return ;
			} ;
			if(e.name) {
				this.validatingValue(e.name,e.value) ;
				return ;
			} ;
		}
	},
	watch : {
		'cliente' : function(wnew,wold){
			this.validatingValue('cliente',wnew) ;
			// let item = this.list.list_clientes.filter(row=>{
			// 	return row.id == wnew ;
			// }) ;
			let item = this.remote_cliente.list.filter(row=>{
				return row.id == wnew ;
			}) ;
			if(item.length>0) {
				this.telefono = item[0].telefono ;
				this.direccion = item[0].direccion ;
				let city = item[0].ciudad || '' ;
				let country = item[0].pais?item[0].pais.descripcion:'' ;
				this.ciudad = city+', '+country ;
				return ; 
			}
			this.planta = null ;
			this.telefono = null ;
			this.direccion = null ;
			this.ciudad=null;
		},
		'forma_pago' : function(wnew,wold){
			this.validatingValue('forma_pago',wnew) ;
		},
		'fecha_venta' : function(wnew,wold){
			this.validatingValue('fecha',wnew) ;
		},
		'telefono' : function(wnew,wold){
			this.validatingValue('telefono',wnew) ;
		},
		// 'telefono2' : function(wnew,wold){
		// 	this.validatingValue('telefono2',wnew) ;
		// },
		'direccion' : function(wnew,wold){
			this.validatingValue('direccion',wnew) ;
		},
		'tipo_invoice' : function(wnew,wold){
			this.validatingValue('tipo_invoice',wnew) ;
		},
		'child.article.categoria.id': function(wnew,wold){
			this.getArticulos();
		},
	},
	computed : {
		tarjeta_status(){
			if(this.forma_pago==2){
				return false
			}else{
				return true ;
			}
		},
		areThereErrors(){
			let wfields = [
				'cliente','fecha_venta',
				'telefono',
				// 'telefono2',
				'direccion',
				'forma_pago',
				'tipo_invoice',
			]
			for(item in wfields){
				this.validatingValue(wfields[item],this[wfields[item]])
			}
			return this.errors.length>0?true:false;
		},
		summary(){
			let _total = 0;
			let _subtotal = 0;
			for(item in this.lst_items){
				_subtotal += parseFloat(this.lst_items[item].subtotal||0) ;
				_total += parseFloat(this.lst_items[item].total||0) ;
			};
			return {
				subtotal: _subtotal.toFixed(2),
				total: _total.toFixed(2),
			} ;
		},
		lst_items(){
			return this.details.filter(row=>{
				try {
					return row.visible==true ;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
		},
	},
	created(){
				
	},
	mounted(){
		this.validatingStatus();
	},
}

const routes =[
	{path:'/',component: wrequests,},
	{ path:'/order',component: wreserva,},
	{ 
		path:'/order/:uid',
		component: wreserva,
		props : true,
	},
]  ;
const router = new VueRouter({
	routes: routes
})
const vcontainer = new Vue({
	delimiters : ['${','}'],
	el : "#vTabs",
	data : {
		list : listas,
	},
	router,
	methods : {
		getClientes(){
			axios({
				method : 'get',
				url: URI_CLIENTES,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_clientes = response.result;
			}).catch( err=> {
				if (err.response) {
					console.log(err.response.data) ;
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de Clientes.',
						icon:'warning',
					});
				}
			}) ;
		},
		getVendedores(){
			axios({
				method : 'get',
				url: URI_VENDEDOR,
				params : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_vendedores = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de vendedores.',
					icon:'warning',
				});
			}) ;
		},
		getTInvoices(){
			axios({
				method : 'get',
				url: URI_TIPO_INV,
				params : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_tinvoice = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de tipo invoice.',
					icon:'warning',
				});
			}) ;
		},
		getFormaPago(){
			axios({
				method : 'get',
				url: URI_FPAGO,
				params : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_fpago = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de forma de pago.',
					icon:'warning',
				});
			}) ;
		},
		getTDocumentos(){
			axios({
				method : 'get',
				url: URI_TDOCUMENTO,
				params : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_tdocumento = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de tipo documento.',
					icon:'warning',
				});
			}) ;
		},
		getCategorias(){
			axios({
				method : 'get',
				url: URI_CATEGORIAS,
				params : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_categorias = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de categorias.',
					icon:'warning',
				});
			}) ;
		},
		getEstados(){
			axios({
				method : 'get',
				url: URI_ESTADOS_VENTA,
				params : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_estados = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de estado venta.',
					icon:'warning',
				});
			}) ;
		},
		onChangePage(wpage,wparams){
			let wurl = wpage ;
			if (wparams){
				wurl = `${wurl}/${wparams.id}`
			};
			this.$router.push({
				path:wurl,
				params: wparams,
			});
		},
		onCloseDialog(){
			this.$router.push({path:'/'});	
		},
	},
	mounted(){
		// this.getClientes();
		this.getTDocumentos();
		this.getFormaPago();
		this.getCategorias();
		this.getEstados();
		this.getTInvoices();
		this.getVendedores();
	},
}) ;
