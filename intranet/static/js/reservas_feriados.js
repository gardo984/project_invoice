

const listas = {
	list_estados : [],
	list_salas : [],
	list_plantas : [],
} ;
const vApp = new Vue({
	delimiters : ['${','}'],
	el : '#vContainer',
	data: {
		fecha:null,
		list : listas,
		details : [],
		item_feriado : [],
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods : {
		cleanFields(){
			let witems = ['sala','estado','planta',]
			for(item in witems) {
				this[witems[item]] = null ;
			}
		},
		getParameters(){
			return {
				fecha : this.fecha,
			} ;
		},
		setValues(){
			if(this.item_feriado.id){
				vModal.fecha = this.item_feriado.fecha ;
				vModal.descripcion = this.item_feriado.descripcion;
				vModal.estado = this.item_feriado.estado.id ;
			} ;
		},
		showDialog() {
			vModal.dialog_status = true;
		},
		getEstados(){
			axios({
				method : 'get',
				url: URI_ESTADO,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_estados = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de Marcas.',
					icon:'warning',
				});
			}) ;
		},
		newItem(){
			vModal.style_locked = false ;
		},
		getSearch(){
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_reservas_feriados',
			})
			axios({
				method : 'post',
				url:URI_HOME,
				data : wparameters,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de feriados.',
					icon:'warning',
				});
			}) ;
		},
		modifyMeta(item){
			this.item_feriado= item;
			this.setValues();
			vModal.style_locked = false;
		},
		removeMeta(item){
			this.item_feriado = item;
			let wfecha = moment(this.item_feriado.fecha).format('DD/MM/YYYY')
			let wmessage  = `Se procederá con el eliminado de '${wfecha} ` + 
					`${this.item_feriado.descripcion}', ¿Desea Continuar?` ;
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.removeProcess();
						break;
				} ;
			}) ;
		},
		removeProcess(){
			let wparameters = {
				wtype : 'remove_reserva_feriado',
				item : this.item_feriado,
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					this.item_feriado = [],
					swal('Aviso',response.result.message,"success");
					this.getSearch();
				}
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, eliminando feriado de reservas.',
						icon:'warning',
					});
				}
			}) ;
		},
	},
	computed : {
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
			return ;
		},
		totales(){
			let wnohabilitados = 0 ;
			let whabilitados = 0 ;
			this.lst_items.forEach(function(witem,windex){
				whabilitados += (witem.estado.id==1?1:0);
				wnohabilitados += (witem.estado.id==1?0:1);
			}) ;
			return {
				habilitados: whabilitados,
				nohabilitados: wnohabilitados,
				filas: this.lst_items.length,
			} ;
		},
	},
	mounted(){
		this.getSearch();
		this.getEstados();
	},
}) ;

const vModal = new Vue({
	delimiters : ['${','}'],
	el: '#vModal',
	data : {
		fecha : null,
		descripcion : null,
		estado : null,
		item_feriado : [],
		list : listas,
		style_locked : true,
	},
	methods : {
		getParameters(){
			return {
				id : vApp.item_feriado.id ? vApp.item_feriado.id:null,
				fecha : this.fecha? this.fecha:null,
				descripcion : this.descripcion,
				estado : this.estado ? this.estado:null,
				wtype : vApp.item_feriado.id ?'update_reserva_feriado':'save_information',
			} ;
		},
		validatingFields(){
			let wparameters = this.getParameters();
			if(!wparameters.fecha) {
				swal("Error",'Ingrese Fecha','error');
				return false;
			} ;
			if(!wparameters.descripcion) {
				swal("Error",'Ingrese descripcion','error');
				return false;
			} ;
			if(!wparameters.estado) {
				swal("Error",'Seleccione Estado','error');
				return false;
			} ;
			return true;
		},
		cleanFields(){
			let wfields = [
				'descripcion','estado','fecha',
			] ;
			let wvalues = [
				null,null,null,
			] ;
			for(item in wfields){
				this[wfields[item]] = wvalues[item];
			};
			if(vApp.item_feriado){
				vApp.item_feriado= [];
			} ;
		},
		closeDialog() {
			this.cleanFields();
			this.style_locked = true;
		},
		cancelDialog() {
			this.closeDialog() ;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					vApp.getSearch();
					this.closeDialog()
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, procesando guardado de datos feriado de reservas.',
					icon:'warning',
				});
			}) ;
		},
		confirmDialog() {
			if(!this.validatingFields()) return false;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation() ;
						break;
				} ;
			}) ;
		},
	}
}) ;