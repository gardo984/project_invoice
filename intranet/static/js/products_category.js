

const listas = {
	list_estados : [],
} ;

const vApp = new Vue({
	delimiters : ['${','}'],
	el:"#vContainer",
	data : {
		categoria :null,
		estado:null,
		details : [],
		list : listas,
		item_categoria: [],
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods:{
		removeProcess(){
			let wparameters = {
				wtype : 'remove_products_category',
				item : this.item_categoria,
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					this.item_categoria = [],
					swal('Aviso',response.result.message,"success");
					vApp.getCategories();
				}
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, eliminando categoria.',
						icon:'warning',
					});
				}
			}) ;
		},
		handleModifyCategory(item){
			vModal.dialog_status = true;
			this.item_categoria = item;
			this.setValues()
		},
		handleRemoveCategory(item){
			this.item_categoria = item;
			let wmessage  = `Se procederá con el eliminado de categoria: ${this.item_categoria.descripcion}, ¿Desea Continuar?`
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.removeProcess();
						break;
				} ;
			}) ;
		},
		handleSearch(){
			this.getCategories();
		},
		handleAddCategory(){
			vModal.dialog_status = true;
			vModal.estado = 1;
		},
		getParameters(){
			return {
				categoria : this.categoria,
				estado : this.estado,
			} ;
		},
		getCategories(){
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_products_categories',
			})
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result;
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo lista de categorias.',
					icon:'warning',
				});
			}) ;
		},
		getEstados(){
			axios({
				method : 'get',
				url: URI_ESTADO,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_estados = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de estados.',
					icon:'warning',
				});
			}) ;
		},
		setValues(){
			if(this.item_categoria){
				vModal.id = this.item_categoria.id ;
				vModal.categoria = this.item_categoria.descripcion ;
				vModal.estado = this.item_categoria.estado.id;
			} ;
		},
	},
	computed:{
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true ;
				} catch (ex) {
					return false;
				}
			})
		},
		totales(){
			return {
				filas : this.lst_items.length,
			} ;
		},
	},
	mounted(){
		this.getEstados();
		this.getCategories();
	},
}) ;

const vModal = new Vue({
	el:"#vModal",
	data : {
		id:null,
		dialog_status:false,
		estado :null,
		categoria :null,
		list : listas,
	},
	methods : {
		validatingFields(){
			let wparameters = this.getParameters();
			if(!wparameters.categoria) {
				swal("Error",'Ingrese Categoria','error');
				return false;
			} ;
			if(!wparameters.estado) {
				swal("Error",'Seleccione Estado','error');
				return false;
			} ;
			return true;
		},
		getParameters(){
			return {
				id : vApp.item_categoria.id ? vApp.item_categoria.id:null,
				categoria:this.categoria,
				estado: this.estado,
				wtype : vApp.item_categoria.id ?'update_products_category':'add_products_category',
			} ;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					vApp.getCategories();
					this.handleCloseDialog()
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, registrando categoria.',
					icon:'warning',
				});
			}) ;
		},
		cleanFields(){
			let wfields = [
				'estado','categoria',
			] ;
			let wvalues = [
				null,null,
			] ;
			for(item in wfields){
				this[wfields[item]] = wvalues[item];
			};
			if(vApp.item_categoria){
				vApp.item_categoria= [];
			} ;
		},
		handleCloseDialog(){
			this.dialog_status = false;
			this.cleanFields();
		},
		handleSaveCategory(){
			if(!this.validatingFields()) return false;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation() ;
						break;
				} ;
			}) ;
		},
	},
	mounted(){

	}
}) ;