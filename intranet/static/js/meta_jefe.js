

const listas = {
	list_jefes : [],
	list_marcas : [],
} ;
const vApp = new Vue({
	delimiters : ['${','}'],
	el : '#vContainer',
	data: {
		jefe : null,
		marca : null,
		mes : null,
		anio : null,
		list : listas,
		details : [],
		item_meta : [],
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods : {
		getParameters(){
			return {
				marca : this.marca,
				jefe : this.jefe,
				mes : this.mes ? moment().format("MM"):null,
				anio : this.anio ? moment(this.anio).format("YYYY"):null,
			} ;
		},
		setValues(){
			if(this.item_meta){
				vModal.id = this.item_meta.id ;
				vModal.jefe = this.item_meta.jefe.id ;
				vModal.marca = this.item_meta.marca.id ;
				vModal.mes = this.item_meta.mes.toString() ;
				vModal.anio = this.item_meta.anio.toString() ;
				vModal.meta = this.item_meta.meta ;
			} ;
		},
		showDialog() {
			vModal.dialog_status = true;
		},
		getJefes(){
			axios({
				method : 'get',
				url: URI_JEFES,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_jefes = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de Jefes.',
					icon:'warning',
				});
			}) ;
		},
		getMarcas(){
			axios({
				method : 'get',
				url: URI_MARCAS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_marcas = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de Marcas.',
					icon:'warning',
				});
			}) ;
		},
		getMetas(){
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_metas_jefe',
			})
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result;
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo lista de Metas Jefe.',
					icon:'warning',
				});
			}) ;
		},
		getSearch(){
			this.getMetas();
		},
		modifyMeta(item){
			this.item_meta = item;
			this.setValues();
			vModal.dialog_status = true ;
		},
		removeMeta(item){
			this.item_meta = item;
			let wmessage  = `Se procederá con el eliminado de meta Nro. ${this.item_meta.id}, ¿Desea Continuar?`
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.removeProcess();
						break;
				} ;
			}) ;
		},
		removeProcess(){
			let wparameters = {
				wtype : 'remove_meta_jefe',
				item : this.item_meta,
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					this.item_meta = [],
					swal('Aviso',response.result.message,"success");
					vApp.getMetas();
				}
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, eliminando meta por Jefe.',
						icon:'warning',
					});
				}
			}) ;
		},
	},
	computed : {
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
		},
	},
	mounted(){
		this.getJefes();
		this.getMarcas();
		this.getMetas();
	},
}) ;

const vModal = new Vue({
	delimiters : ['${','}'],
	el: '#vModal',
	data : {
		dialog_status : false,
		id : null,
		jefe : null,
		marca : null,
		mes : null,
		anio : null,
		meta : null,
		list : listas,
	},
	methods : {
		getParameters(){
			return {
				id : vApp.item_meta.id ? vApp.item_meta.id:null,
				marca : this.marca,
				jefe : this.jefe,
				mes : this.mes ? this.mes:null,
				anio : this.anio ? this.anio:null,
				meta : this.meta,
				wtype : vApp.item_meta.id ?'update_meta_jefe':'save_information',
			} ;
		},
		validatingFields(){
			let wparameters = this.getParameters();
			if(!wparameters.jefe) {
				swal("Error",'Seleccione Jefe','error');
				return false;
			} ;
			if(!wparameters.marca) {
				swal("Error",'Seleccione Marca','error');
				return false;
			} ;
			if(!wparameters.anio) {
				swal("Error",'Seleccione año','error');
				return false;
			} ;
			if(!wparameters.mes) {
				swal("Error",'Seleccione mes','error');
				return false;
			} ;
			if(!wparameters.meta) {
				swal("Error",'Ingrese una Meta','error');
				return false;
			} else if (wparameters.meta==0){
				swal("Error",'Meta no puede ser igual a 0','error');
				return false;
			} ;

			return true;
		},
		cleanFields(){
			let wfields = [
				'marca','jefe','mes','anio','meta',
			] ;
			let wvalues = [
				null,null,null,null,null,
			] ;
			for(item in wfields){
				this[wfields[item]] = wvalues[item];
			};
			if(vApp.item_meta){
				vApp.item_meta= [];
			} ;
		},
		closeDialog() {
			this.cleanFields();
			this.dialog_status = false ;
		},
		cancelDialog() {
			this.closeDialog() ;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					vApp.getMetas();
					this.closeDialog()
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, registrando meta por jefe.',
					icon:'warning',
				});
			}) ;
		},
		confirmDialog() {
			if(!this.validatingFields()) return false;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation() ;
						break;
				} ;
			}) ;
		},
	}
}) ;