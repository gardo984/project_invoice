

const listas = {
	list_estados : [],
	list_categorias: [],
} ;

const vApp = new Vue({
	delimiters : ['${','}'],
	el:"#vContainer",
	data : {
		articulo:null,
		categoria :null,
		estado:null,
		details : [],
		list : listas,
		item_articulo: [],
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods:{
		removeProcess(){
			let wparameters = {
				wtype : 'remove_products_article',
				item : this.item_articulo,
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					this.item_articulo = [],
					swal('Aviso',response.result.message,"success");
					vApp.getArticles();
				}
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, eliminando producto.',
						icon:'warning',
					});
				}
			}) ;
		},
		handleModifyArticle(item){
			vModal.dialog_status = true;
			this.item_articulo = item;
			this.setValues()
		},
		handleRemoveArticle(item){
			this.item_articulo = item;
			let wmessage  = `Se procederá con el eliminado de articulo: ${this.item_articulo.descripcion}, ¿Desea Continuar?`
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.removeProcess();
						break;
				} ;
			}) ;
		},
		handleSearch(){
			this.getArticles();
		},
		handleAddCategory(){
			vModal.dialog_status = true;
			vModal.estado = 1;
		},
		getParameters(){
			return {
				articulo : this.articulo,
				categoria : this.categoria,
				estado : this.estado,
			} ;
		},
		getArticles(){
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_products_articles',
			})
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result;
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo lista de articulos.',
					icon:'warning',
				});
			}) ;
		},
		getCategorias(){
			axios({
				method : 'get',
				url: URI_CATEGORIAS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_categorias = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de categorias.',
					icon:'warning',
				});
			}) ;
		},
		getEstados(){
			axios({
				method : 'get',
				url: URI_ESTADO,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_estados = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de estados.',
					icon:'warning',
				});
			}) ;
		},
		setValues(){
			if(this.item_articulo){
				vModal.id = this.item_articulo.id ;
				vModal.articulo = this.item_articulo.descripcion ;
				vModal.categoria = this.item_articulo.categoria.id ;
				vModal.estado = this.item_articulo.estado.id;
			} ;
		},
	},
	computed:{
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true ;
				} catch (ex) {
					return false;
				}
			})
		},
		totales(){
			return {
				filas : this.lst_items.length,
			} ;
		},
	},
	mounted(){
		this.getEstados();
		this.getCategorias()
		this.getArticles();
	},
}) ;

const vModal = new Vue({
	el:"#vModal",
	data : {
		id:null,
		dialog_status:false,
		articulo:null,
		estado :null,
		categoria :null,
		list : listas,
	},
	methods : {
		validatingFields(){
			let wparameters = this.getParameters();
			if(!wparameters.categoria) {
				swal("Error",'Ingrese Categoria','error');
				return false;
			} ;
			if(!wparameters.estado) {
				swal("Error",'Seleccione Estado','error');
				return false;
			} ;
			return true;
		},
		getParameters(){
			return {
				id : vApp.item_articulo.id ? vApp.item_articulo.id:null,
				articulo : this.articulo,
				categoria:this.categoria,
				estado: this.estado,
				wtype : vApp.item_articulo.id ?'update_products_article':'add_products_article',
			} ;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					vApp.getArticles();
					this.handleCloseDialog()
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, registrando articulo.',
					icon:'warning',
				});
			}) ;
		},
		cleanFields(){
			let wfields = [
				'id','articulo','estado','categoria',
			] ;
			let wvalues = [
				null,null,null,null,
			] ;
			for(item in wfields){
				this[wfields[item]] = wvalues[item];
			};
			if(vApp.item_articulo){
				vApp.item_articulo= [];
			} ;
		},
		handleCloseDialog(){
			this.dialog_status = false;
			this.cleanFields();
		},
		handleSaveArticle(){
			if(!this.validatingFields()) return false;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation() ;
						break;
				} ;
			}) ;
		},
	},
	mounted(){

	}
}) ;
