

const listas = {
	list_estados : [],
	list_salas : [],
	list_plantas : [],
	list_empleados : [],
} ;

const modalSchedule = {
	status :false,
	data : {
		sala : null,
		fecha_busqueda : null,
		button_status : false,
		details : [],
	},
} ;

const wrequests = { 
	delimiters : ['${','}'],
	name :"wrequests",
	template : '#container-requests',
	data(){
		return {
			empleado : null,
			fecha_desde : null,
			fecha_hasta : null,
			list : listas,
			details : [],
			item_reserva : [],
			errors : [],
		}
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods: {
		handleCleanFields(){
			let wdate_to = new Date();
			let wdate_from = wdate_to.addDays(wdate_to,-7) ;
			wdate_from = moment(wdate_from).format("Y-MM-D") 
			wdate_to = moment(wdate_to).format("Y-MM-D") 
			let witems = ['empleado','fecha_desde','fecha_hasta',] ;
			let wvalues = [null,wdate_from,wdate_to,] ;
			for(item in witems) {
				this[witems[item]] = wvalues[item] ;
			} ;
		},
		getParameters(){
			return {
				empleado : this.empleado,
				fecha_desde : this.fecha_desde,
				fecha_hasta : this.fecha_hasta,
			} ;
		},
		handleNew(){
			this.$emit('change-page','/reserva') ;
		},
		handleSearch(){
			if(this.areThereErrors) {
				swal('Aviso','Hay campos por completar.','error')
				return false ;
			} ;
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_reservas',
			})
			axios({
				method : 'post',
				url:URI_HOME,
				data : wparameters,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de reservas.',
					icon:'warning',
				});
			}) ;
		},
		modifyReserva(item){
			this.$emit('change-page','/reserva',item) ;
		},
		removeReserva(item){
			this.item_reserva = item;
			let wfecha = moment(this.item_reserva.fecha_evento).format('DD/MM/YYYY')
			let wmessage  = `Se procederá con el eliminado ` +
				 `de la reserva : '${wfecha}', ¿Desea Continuar?` ;
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.removeProcess();
						break;
				} ;
			}) ;
		},
		removeProcess(){
			let wparameters = {
				wtype : 'remove_reserva',
				item : this.item_reserva,
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					this.item_feriado = [],
					swal('Aviso',response.result.message,"success");
					this.handleSearch();
				}
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, eliminando reservas.',
						icon:'warning',
					});
				}
			}) ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
	},
	computed: {
		areThereErrors(){
			return this.errors.length>0?true:false;
		},
		totales(){
			return {
				filas : this.lst_items.length,
			} ;
		},
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
		},
	},
	watch : {
		'fecha_desde': function(wnew, wold){
			this.validatingValue('fecha_desde',wnew) ;
		},
		'fecha_hasta': function(wnew, wold){
			this.validatingValue('fecha_hasta',wnew) ;
		},
	},
	mounted(){
		this.handleCleanFields() ;
		this.handleSearch();
	},
}
const wreserva ={ 
	delimiters : ['${','}'],
	name :"wreserva",
	template : '#container-form',
	data(){
		return {
			sala: null,
			solicitante: null,
			planta:null,
			fecha:null,
			time_from :null,
			time_to:null,
			estado:null,
			participante :{
				id: null,
				value :null,
			},
			list : listas,
			details : [],
			range_time: {
				start: '08:30',
				step: '00:15',
				end: '18:30'
			},
			remote_empleado: {
				loading:false,
				list : [],
			},
			remote_participante: {
				loading:false,
				list : [],
			},
			item_reserva : [],
			errors : [],
			modal : modalSchedule,
		}
	},
	methods : {
		getSchedulesByDate(){
			let wparameters = {
				wtype : 'get_schedules',
				fecha : this.modal.data.fecha_busqueda,
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.modal.data.details = response.result.schedules ;
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo disponibilidad por sala.',
						icon:'warning',
					});
				}
			}) ;
		},
		handleModalSelectItem(){
			console.log("select item") ;
		},
		handleModalSearch(){
			console.log("click event to search");
		},
		handleModalSubmit(){
			console.log("submit modal");
		},
		handleModalClose(){
			this.modal.status = false ;
		},
		handleAvailability(date_value){
			if(date_value){
				this.modal.status = true ;
				this.modal.data.fecha_busqueda = this.fecha;
				this.getSchedulesByDate();
			} ;
		},
		handleAddParticipante(){
			if (!this.participante.id){
				swal('Error','Ingrese participante a agregar.','error') ;
				return false;
			} ;
			if(this.participante.id){
				for(item in this.lst_items){
					let wobj = this.lst_items[item] ;
					if (!wobj.visible) continue ;
					if(wobj.empleado.id == this.participante.id){
						swal('Error','Participante ya se encuentra agregado.','error') ;
						return false ;
					} ;
				} ;
				let witem = this.participante.id ;
				let wapplicant = this.list.list_empleados.filter(row=> {
					return row.id ==  witem ;
				}) ;
				if (wapplicant.length>0) {
					this.details.push({
						check : false,
						empleado : {
							id : wapplicant[0].id,
							fullname : wapplicant[0].value,
						},
						visible : true,
					}) ;
					this.participante.id = null ;
					this.participante.value = null ;
				}
			} ;
		},
		handleRemoveParticipante(item){
			if(item){
				let wmessage = `Se procederá con la eliminación de ${item.empleado.fullname}, ¿Desea continuar?` ;
				swal({
					title : 'Confirmación',
					text : wmessage,
					buttons : {
						yes: 'Si',
						no: 'No',
					},
					icon : 'info',
				}).then(rsp=>{
					switch(rsp){
						case 'yes' :
								this.lst_items.forEach((wobj,windex)=> {
									if(wobj.empleado.id == item.empleado.id){
										wobj.visible = false ;
										return ;
									} ;
								}) ;
							break;
					}
				}) ;
			} ;
		},
		selectParticipante(item){
			this.participante.id = item.id ;
			this.participante.value = item.value ;
		},
		searchParticipante(query,cb){
			if (query !== '') {
	            this.remote_participante.list = this.list.list_empleados.filter(item => {
	              return item.value.toLowerCase()
	                	.indexOf(query.toLowerCase()) > -1;
	            }) ;
	        } else {
	         	this.remote_participante.list = [];
	        };
	        cb(this.remote_participante.list);
		},
		searchParticipante2(query){
			 if (query !== '') {
		          this.remote_participante.loading = true;
		          setTimeout(() => {
		            this.remote_participante.loading = false;
		            this.remote_participante.list = this.list.list_empleados.filter(item => {
		              return item.value.toLowerCase()
		                	.indexOf(query.toLowerCase()) > -1;
		            });
		          }, 200);
	        } else {
	         	 this.remote_participante.list = [];
	        }
		},
		searchEmpleado(query){
			 if (query !== '') {
		          this.remote_empleado.loading = true;
		          setTimeout(() => {
		            this.remote_empleado.loading = false;
		            this.remote_empleado.list = this.list.list_empleados.filter(item => {
		              return item.value.toLowerCase()
		                	.indexOf(query.toLowerCase()) > -1;
		            });
		          }, 200);
	        } else {
	         	 this.remote_empleado.list = [];
	        }
		},
		getParameters(){
			return  {
				 solicitante : this.solicitante?this.solicitante:null,
				 sala : this.sala?this.sala:null,
				 planta : this.planta?this.planta:null,
				 fecha : this.fecha?moment(this.fecha).format('Y-MM-D'):null,
				 time_from : this.time_from?this.time_from:null,
				 time_to : this.time_to?this.time_to:null,
				 estado : this.estado,
				 id: this.item_reserva.id ?this.item_reserva.id:null,
				 participantes : this.lst_items.length>0 ?this.lst_items:[],
				 wtype : this.item_reserva.id ?'update_reserva':'save_information',
			} ;
		},
		validatingFields(){
			let wparameters = this.getParameters();
			if(!wparameters.solicitante) {
				swal("Error",'Ingrese solicitante','error');
				return false;
			} ;
			if(!wparameters.sala) {
				swal("Error",'Ingrese Sala','error');
				return false;
			} ;
			if(!wparameters.planta) {
				swal("Error",'Ingrese Planta','error');
				return false;
			} ;
			if(!wparameters.fecha) {
				swal("Error",'Ingrese Fecha','error');
				return false;
			} ;
			if(!wparameters.time_from) {
				swal("Error",'Ingrese horario desde.','error');
				return false;
			} ;
			if(!wparameters.time_to) {
				swal("Error",'Ingrese horario hasta.','error');
				return false;
			} ;
			if(!wparameters.estado) {
				swal("Error",'Seleccione Estado','error');
				return false;
			} ;
			return true;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					this.$emit('close-dialog') ;
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, procesando guardado de datos en reservas.',
					icon:'warning',
				});
			}) ;
		},
		handleConfirmDialog(){
			if(this.areThereErrors){
				swal('Aviso','Hay campos por completar.','error') ;
				return false;
			} ;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
				icon : 'info'
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation() ;
						break;
				} ;
			}) ;
		},
		handleCancelOperation(){
			let wmessage  = `Se cancelaran los cambios ingresados, ¿Desea Continuar?` ;
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
				icon:'warning',
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.$emit('close-dialog') ;
						break;
				} ;
			}) ;
		},
		setDefaultValues(){
			this.estado = 1;
			this.fecha = new Date();
		},
		setValues(){
			if(this.item_reserva){
				this.estado = this.item_reserva.estado.id;
				this.sala = this.item_reserva.sala.id;
				this.planta = this.item_reserva.planta.id;
				let wfrom =` ${this.item_reserva.fecha_evento} ${this.item_reserva.hora_inicio}`;
				this.time_from = moment(wfrom,"Y-MM-D H:mm:ss").format("H:mm");
				let wto =` ${this.item_reserva.fecha_evento} ${this.item_reserva.hora_final}`;
				this.time_to = moment(wto,"Y-MM-D H:mm:ss").format("H:mm");
				this.fecha = this.item_reserva.fecha_evento;
				this.remote_empleado.list = this.list.list_empleados.filter(item => {
	              	return item.id == this.item_reserva.empleado.id;
	            });
	            this.solicitante = this.item_reserva.empleado.id;
			} ;
		},
		getReserva(wid){
			let wparameters = {
				id:wid, 
				wtype : 'get_reserva_item',
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers : axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.item_reserva = response.result[0];
				this.details = this.item_reserva.participantes ;
				this.setValues();
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo reserva.',
					icon:'warning',
				});
			}) ;	
		},
		validatingStatus(){
			let wparams = this.$route.params ;
			if(Object.keys(wparams).length>0){
				this.getReserva(wparams.uid);
			}else{
				this.setDefaultValues();
			} ;
			// this.$refs.sala.focus() ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
		handleBlur(e){
			if(e.target) {
				this.validatingValue(e.target.name,e.target.value) ;
				return ;
			} ;
			if(e.name) {
				this.validatingValue(e.name,e.value) ;
				return ;
			} ;
		}
	},
	watch : {
		'solicitante' : function(wnew,wold){
			this.validatingValue('solicitante',wnew) ;
		},
		'sala':function(wnew,wold){
			this.validatingValue('sala',wnew) ;
			let item = this.list.list_salas.filter(row=>{
				return row.id == wnew ;
			}) ;
			if(item.length>0) {
				this.planta = item[0].planta.id ;
				return ; 
			}
			this.planta = null ;
		},
		'planta' : function(wnew,wold){
			this.validatingValue('planta',wnew) ;
		},
		'fecha' : function(wnew,wold){
			this.validatingValue('fecha',wnew) ;
		},
		'time_from' : function(wnew,wold){
			this.validatingValue('time_from',wnew) ;
		},
		'time_to' : function(wnew,wold){
			this.validatingValue('time_to',wnew) ;
		},
		'estado' : function(wnew,wold){
			this.validatingValue('estado',wnew) ;
		},
	},
	computed : {
		areThereErrors(){
			let wfields = [
				'solicitante','sala','planta',
				'fecha','time_from','time_to',
				'estado',
			]
			for(item in wfields){
				this.validatingValue(wfields[item],this[wfields[item]])
			}
			return this.errors.length>0?true:false;
		},
		lst_items(){
			return this.details.filter(row=>{
				try {
					return row.visible==true ;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
		},
	},
	created(){
				
	},
	mounted(){
		this.validatingStatus();
	},
}

const routes =[
	{path:'/',component: wrequests,},
	{ path:'/reserva',component: wreserva,},
	{ 
		path:'/reserva/:uid',
		component: wreserva,
		props : true,
	},
]  ;
const router = new VueRouter({
	routes: routes
})
const vcontainer = new Vue({
	delimiters : ['${','}'],
	el : "#vTabs",
	data : {
		list : listas,
	},
	router,
	methods : {
		getEmpleados(){
			axios({
				method : 'get',
				url: URI_EMPLEADOS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_empleados = response.result;
			}).catch( err=> {
				if (err.response) {
					console.log(err.response.data) ;
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de Empleados.',
						icon:'warning',
					});
				}
			}) ;
		},
		getPlantas(){
			axios({
				method : 'get',
				url: URI_PLANTAS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_plantas = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de plantas.',
					icon:'warning',
				});
			}) ;
		},
		getEstados(){
			axios({
				method : 'get',
				url: URI_ESTADO,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_estados = response.result;
			}).catch( err=> {
				if (err.response) {
					console.log(err.response.data) ;
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de estados.',
						icon:'warning',
					});
				}
			}) ;
		},
		getSalas(){
			axios({
				method : 'get',
				url: URI_SALAS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_salas = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de salas.',
					icon:'warning',
				});
			}) ;
		},
		onChangePage(wpage,wparams){
			let wurl = wpage ;
			if (wparams){
				wurl = `${wurl}/${wparams.id}`
			};
			this.$router.push({
				path:wurl,
				params: wparams,
			});
		},
		onCloseDialog(){
			this.$router.push({path:'/'});	
		},
	},
	mounted(){
		this.getPlantas();
		this.getEstados();
		this.getSalas();
		this.getEmpleados();
	},
}) ;
